﻿using Base2.Storage;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Base2.Web.Controllers
{
    public class UploadController : UploadControllerBase
    {
        public UploadController(IBinaryObjectManager binaryObjectManager) :
            base(binaryObjectManager)
        {
        }

        public async Task<ActionResult> GetUploadedObject(Guid fileId, string contentType)
        {
            var fileObject = await BinaryObjectManager.GetOrNullAsync(fileId);
            if (fileObject == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            return File(fileObject.Bytes, contentType);
        }

        public async Task DeleteUploadedObject(Guid fileId)
        {
            await BinaryObjectManager.DeleteAsync(fileId).ConfigureAwait(false);
        }
    }
}


