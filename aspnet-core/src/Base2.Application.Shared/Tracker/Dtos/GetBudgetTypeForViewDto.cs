namespace Base2.Tracker.Dtos
{
    public class GetBudgetTypeForViewDto
    {
        public BudgetTypeDto BudgetType { get; set; }
    }
}