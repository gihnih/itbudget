import {Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {finalize} from 'rxjs/operators';
import {
    BudgetUsesServiceProxy,
    CreateOrEditBudgetUseDto,
    CreateOrEditBudgetDto,
    BudgetApproverDto,
    GetBudgetForViewDto,
    BudgetDto,
    BudgetRequestDto, GetBudgetUseForEditOutput, UserServiceProxy, BinaryObjectDto,
} from '@shared/service-proxies/service-proxies';
import {AppComponentBase} from '@shared/common/app-component-base';
import {BudgetLookupTableModalComponent} from './budget-lookup-table-modal.component';
import {NgForm} from '@angular/forms';
import {AppSessionService} from '@shared/common/session/app-session.service';
import {AppConsts} from '@shared/AppConsts';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Http} from '@angular/http';

@Component({
    selector: 'createOrEditBudgetUseModal',
    templateUrl: './create-or-edit-budgetUse-modal.component.html'
})
export class CreateOrEditBudgetUseModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('budgetLookupTableModal') budgetLookupTableModal: BudgetLookupTableModalComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>(true);

    active = false;
    saving = false;
    edit = false;
    userOrgUnitId: number;
    isUpdateActualAmount = false;
    budgetUse = new GetBudgetUseForEditOutput();

    budget: CreateOrEditBudgetDto = new CreateOrEditBudgetDto();
    budgetApprover: BudgetApproverDto;

    budgetDescription = '';
    requestAmount:number = 0;
    totalBudgeted:number = 0;
    amountBudgetRequired: number = 0;
    isBudgetEnough:boolean = false

    bb: any[];
    paperStatus;
    userDepartment = [];

    // Upload fileg
    uploadUrl: string;
    getUrl: string;
    deleteUrl: string;
    isFileSelected = false;
    public progress = 0;
    @Output() onProgress: EventEmitter<any> = new EventEmitter();
    attachment: Attachment[] = [];
    budgetUseId: number;
    create = false;

    canSubmit:boolean = false;


    constructor(
        injector: Injector,
        private _budgetUsesServiceProxy: BudgetUsesServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _appSessionService: AppSessionService,
        private _httpClient: HttpClient,
        private http: HttpClient
    ) {
        super(injector);
        this._budgetUsesServiceProxy.getPaperStatus().subscribe(result => {
            this.paperStatus = result;
        });
        this.budgetUse.budgetUse = new CreateOrEditBudgetUseDto();
        this.budgetUse.budgetUse.budgetRequests = [];
        this.budgetUse.budgetUse.binaryObjects = [];
        this._budgetUsesServiceProxy.getAllOrganizationUnit().subscribe(result => {
            this.userDepartment = result;
            console.log(result);
            if (this.userDepartment.length == 1) {
                this.userOrgUnitId = result[0].id;
            }
        });
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Upload/UploadFile';
        this.getUrl = AppConsts.remoteServiceBaseUrl + '/Upload/GetUploadedObject';
        this.deleteUrl = AppConsts.remoteServiceBaseUrl + '/Upload/DeleteUploadedObject';
    }


    show(budgetUseId?: number) {
        this.budgetUse = new GetBudgetUseForEditOutput();
        this.budgetUse.budgetUse = new CreateOrEditBudgetUseDto();
        this.budgetUse.budgetUse.budgetRequests = [];
        this.budgetUse.budgetUse.binaryObjects = [];

        // if user single OrgUnit auto assign
        if(this.userDepartment.length < 2) {
            this.budgetUse.budgetUse.organizationUnitId = this.userOrgUnitId;
        }


        if (!budgetUseId) { //create new budget utilized
            this.create = true;
            this.active = true;
            this.budgetUse.isActualAmountUpdate = false;
            this.isUpdateActualAmount = false;
            this.modal.show();
        } else {
            this.edit = true;
            this.isBudgetEnough = true;
            this._budgetUsesServiceProxy.getBudgetUseForEdit(budgetUseId).subscribe(result => {
                this.budgetUse = new GetBudgetUseForEditOutput();
                console.log(result);
                this.budgetUse = result;
                this.active = true;
                this.modal.show();
            });
        }
    }

    UpdateAM(budgetUseId?: number) {
        this._budgetUsesServiceProxy.getBudgetUseForEdit(budgetUseId).subscribe(result => {
            this.budgetUse = result;
            this.budgetUse.isActualAmountUpdate = true;
            this.edit = true;
            this.isUpdateActualAmount = true;
            this.active = true;
            this.modal.show();
            console.log(result);
        });
    }

    save(): void {
        this.saving = true;
        let requestForUtilize: number = this.budgetUse.budgetUse.requestAmount;
        let totalRequestFromBudget: number;
        totalRequestFromBudget = 0;

        // get Total Budget Request
        for (let item of this.budgetUse.budgetUse.budgetRequests) {
            totalRequestFromBudget += +item.requestAmount;
            console.log(totalRequestFromBudget);
        }
        this.budgetUse.budgetUse.requestAmount = totalRequestFromBudget;

        console.log(this.budgetUse);
        this._budgetUsesServiceProxy.createOrEdit(this.budgetUse)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });

        // if (requestForUtilize - totalRequestFromBudget === 0) {
        //     this.saving = true;
        //     console.log(this.budgetUse);
        //     this._budgetUsesServiceProxy.createOrEdit(this.budgetUse)
        //         .pipe(finalize(() => {
        //             this.saving = false;
        //         }))
        //         .subscribe(result => {
        //             this.notify.info(this.l('SavedSuccessfully'));
        //             this.close();
        //             this.modalSave.emit(null);
        //         });
        // } else {
        //     let value = requestForUtilize - totalRequestFromBudget;
        //     this.message.warn(
        //         'message',
        //         'Need more or less get from budget : RM ' + value);
        // }

    }

    addBudgetSelect(item: GetBudgetForViewDto, budgetOwn: string) {
        if (item !== null) {
            let budgetRequest: BudgetRequestDto = new BudgetRequestDto();
            budgetRequest.budgetId = item.id;
            budgetRequest.ownBudget = budgetOwn === 'own';
            budgetRequest.budgetUseId = this.budgetUse.budgetUse.id;

            budgetRequest.budget = Object.assign(new BudgetDto(), item);

            this.budgetUse.budgetUse.description = item.description;

            if (!this.budgetUse.budgetUse.budgetRequests.some(b => b.budgetId === item.id)) {
                this.budgetUse.budgetUse.budgetRequests.push(budgetRequest);
            }
        }
    }

    removeBudgetSelect(data: any) {
        console.log('remove');
        console.log(data);
        const indexNumber = this.budgetUse.budgetUse.budgetRequests.indexOf(data);
        if (indexNumber !== -1) {
            this.budgetUse.budgetUse.budgetRequests.splice(indexNumber, 1);
        }
        this.amountBudgetRequiredCalc();
    }

    openSelectBudgetModal(type: string = 'own') {
        this.budgetLookupTableModal.show(type);
    }


    close(): void {
        if (this.create === false && this.edit === false) {
            for (let item of this.budgetUse.budgetUse.binaryObjects) {
                this._budgetUsesServiceProxy.deleteAttachment(item.id).subscribe(result => {
                    console.log('deleteAttachment');
                });
            }
        }

        this.active = false;
        this.edit = false;
        this.create = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }

    amountBudgetRequiredCalc() {
        let totalRequestFromBudget: number = 0;
        totalRequestFromBudget = 0;
        this.isBudgetEnough = false;

        // get Total Budget Request
        for (let item of this.budgetUse.budgetUse.budgetRequests) {
            totalRequestFromBudget += +item.requestAmount;
        }

        // get total Budgeted
        this.totalBudgeted = 0;
        for(let budget of this.budgetUse.budgetUse.budgetRequests){
            this.totalBudgeted += budget.budget.estimatedBalance;
        }
        if(this.totalBudgeted > 0){
            this.isBudgetEnough = this.totalBudgeted - totalRequestFromBudget >= 0 ? true : false;
        }
        
        console.log(this.isBudgetEnough);
    }

    // upload file

    onBeforeSend(event): void {
        console.log(event);
        console.log(this.uploadUrl);
        this.isFileSelected = true;
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());

        event.xhr.upload.addEventListener('progress', (e: ProgressEvent) => {
            if (e.lengthComputable) {
                this.progress = Math.round((e.loaded * 100) / e.total);
            }

            this.onProgress.emit({originalEvent: e, progress: this.progress});
        }, false);

    }

    onUploadFile(event): void {
        const jsonResult = JSON.parse(event.xhr.response);
        console.log(jsonResult);
        let attachment = new BinaryObjectDto();
        attachment.id = jsonResult.result.id;
        attachment.contentType = jsonResult.result.contentType;
        attachment.name = jsonResult.result.name;

        console.log(this.budgetUse.budgetUse.binaryObjects);
        this.budgetUse.budgetUse.binaryObjects.push(attachment);
        console.log(this.budgetUse.budgetUse.binaryObjects);
        this.isFileSelected = false;
        this.progress = 0;
    }

    uploadFile(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);
        console.log(formData);
        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .subscribe(response => {
                this.isFileSelected = false;
                this.progress = 0;
            });
    }

    onDeleteAttachment(id: string) {
        let attIndex = this.budgetUse.budgetUse.binaryObjects.findIndex(e => e.id == id);
        
        if(attIndex > -1){
            this._budgetUsesServiceProxy.deleteAttachment(id).subscribe(result => {
                this.budgetUse.budgetUse.binaryObjects.splice(attIndex, 1);
            });
        }
    }

    test(){
        console.log('test masuk');
    }

}

export interface Attachment {
    contentType: string;
    id: string;
    name: string;
    remark: string;
}
