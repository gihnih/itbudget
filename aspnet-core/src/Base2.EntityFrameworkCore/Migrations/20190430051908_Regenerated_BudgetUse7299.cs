﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class Regenerated_BudgetUse7299 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentAmount",
                table: "BudgetUses");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CurrentAmount",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0m);
        }
    }
}