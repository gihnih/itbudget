import {Component, ViewChild, Injector, Output, EventEmitter, ViewEncapsulation} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {
    BudgetRequestsServiceProxy,
    BudgetRequestBudgetUseLookupTableDto
} from '@shared/service-proxies/service-proxies';
import {AppComponentBase} from '@shared/common/app-component-base';
import {DataTable} from 'primeng/components/datatable/datatable';
import {Paginator} from 'primeng/components/paginator/paginator';
import {LazyLoadEvent} from 'primeng/components/common/lazyloadevent';

@Component({
    selector: 'budgetUseLookupTableModal',
    styleUrls: ['./budgetUse-lookup-table-modal.component.less'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './budgetUse-lookup-table-modal.component.html'
})
export class BudgetUseLookupTableModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    filterText = '';
    id: number;
    displayName: string;
    category = '';
    organizationUnit: any[];

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    constructor(
        injector: Injector,
        private _budgetRequestsServiceProxy: BudgetRequestsServiceProxy
    ) {
        super(injector);
    }

    show(): void {
        this.active = true;
        this.paginator.rows = 5;
        this.getAll();
        this.modal.show();
    }

    getAll(event?: LazyLoadEvent) {
        if (!this.active) {
            return;
        }

        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        this._budgetRequestsServiceProxy.getAllBudgetUseForLookupTable(
            this.filterText,
            this.category,
            this.organizationUnit,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    setAndSave(budgetUse: BudgetRequestBudgetUseLookupTableDto) {
        this.id = budgetUse.id;
        this.displayName = budgetUse.displayName;
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }
}
