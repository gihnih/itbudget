<#
.PARAMETER env
Options are "local" and "staging" "prod"
.PARAMETER deploy
Options are $true and $false. The default value is $false
.PARAMETER updatedb
Options are $true and $false. The default value is $false
#>
param (
    [string]$env = "local",
    [bool]$deploy = $false,
    [bool]$updatedb = $false
)

# COMMON PATHS

$buildFolder = (Get-Item -Path "./" -Verbose).FullName
$slnFolder = Join-Path $buildFolder "../"
$outputFolder = Join-Path $buildFolder "outputs"
$efFolder = Join-Path $slnFolder "src/Base2.EntityFrameworkCore"
$migratorPath = Join-Path $slnFolder "src\Base2.Migrator\bin\Debug\net461"
$migrator = ".\Base2.Migrator.exe"
$webHostFolder = Join-Path $slnFolder "src/Base2.Web.Host"
$ngFolder = Join-Path $buildFolder "../../angular"
$outputHostFolder = Join-Path $outputFolder "Host/*"
$outputNgFolder = Join-Path $outputFolder "ng/*"
$remoteHostFolder = "\\10.1.100.85\c$\inetpub\wwwroot\budgettracker"
$remoteWwwrootFolder = "\\10.1.100.85\c$\inetpub\wwwroot\budgettracker\wwwroot"
$localHostFolder = "C:\inetpub\wwwroot\BudgetTracker"
$localWwwrootFolder = "C:\inetpub\wwwroot\BudgetTracker\wwwroot"

# CLEAR ######################################################################

Remove-Item $outputFolder -Force -Recurse -ErrorAction Ignore
New-Item -Path $outputFolder -ItemType Directory

# RESTORE NUGET PACKAGES #####################################################

Set-Location $slnFolder
dotnet restore

# UPDATE DATABASE ###################################################

Set-Location $efFolder
if ($updatedb) {
    if($env -eq "staging") {
        [Environment]::SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT","Staging")
    } else {
        [Environment]::SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT","Development")
    }

    dotnet ef database update

    Write-Host "Successfully updated the database."

    ## EXECUTE MIGRATOR ###################################################
    Set-Location $migratorPath

    & $migrator "-s"

    Write-Host "Successfully executed the migrator."

    [Environment]::SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT","")
}

## PUBLISH WEB HOST PROJECT ###################################################

Set-Location $webHostFolder
if($env -eq "staging") {
    dotnet publish /p:EnvironmentName=Staging --output (Join-Path $outputFolder "Host")
} elseif($env -eq "prod") {
	dotnet publish -c Release /p:EnvironmentName=Production --output (Join-Path $outputFolder "Host")
}else {
    dotnet publish --output (Join-Path $outputFolder "Host")
}

# PUBLISH ANGULAR UI PROJECT #################################################

Set-Location $ngFolder
& yarn
npm run build-prod
Copy-Item (Join-Path $ngFolder "dist") (Join-Path $outputFolder "ng/") -Recurse

# Change UI configuration
$ngConfigPath = Join-Path $outputFolder "ng/assets/appconfig.json"
if ($env -eq "local") {
    (Get-Content $ngConfigPath) -replace "22742", "8081" | Set-Content $ngConfigPath
    (Get-Content $ngConfigPath) -replace "4200", "8081" | Set-Content $ngConfigPath
} elseif ($env -eq "staging") {
    (Get-Content $ngConfigPath) -replace "localhost:22742", "10.1.100.85:93" | Set-Content $ngConfigPath
    (Get-Content $ngConfigPath) -replace "localhost:4200", "10.1.100.85:93" | Set-Content $ngConfigPath
} elseif($env -eq "prod"){
    (Get-Content $ngConfigPath) -replace "localhost:22742", "10.2.2.101:82" | Set-Content $ngConfigPath
    (Get-Content $ngConfigPath) -replace "localhost:4200", "10.2.2.101:82" | Set-Content $ngConfigPath
}

Set-Location $outputFolder

if($deploy -and $env -eq "staging") {
    Invoke-Command -ComputerName BRUATAMMS01 { Set-Location C:\Windows\System32; ./cmd.exe /c "iisreset /stop"} | Out-Null
    Copy-Item -Force -Recurse -Verbose $outputHostFolder -Destination $remoteHostFolder  | Out-Null
    Copy-Item -Force -Recurse -Verbose $outputNgFolder -Destination $remoteWwwrootFolder  | Out-Null
    Invoke-Command -ComputerName BRUATAMMS01 { Set-Location C:\Windows\System32; ./cmd.exe /c "iisreset /start"}  | Out-Null
} elseif ($deploy -and $env -eq "local") {
    Invoke-Command -ComputerName BRUATAMMS01 { Set-Location C:\Windows\System32; ./cmd.exe /c "iisreset /stop"} | Out-Null
    Copy-Item -Force -Recurse -Verbose $outputHostFolder -Destination $localHostFolder  | Out-Null
    Copy-Item -Force -Recurse -Verbose $outputNgFolder -Destination $localWwwrootFolder  | Out-Null
    Invoke-Command -ComputerName BRUATAMMS01 { Set-Location C:\Windows\System32; ./cmd.exe /c "iisreset /start"}  | Out-Null
}

Write-Host "Successfully deployed to $env."
