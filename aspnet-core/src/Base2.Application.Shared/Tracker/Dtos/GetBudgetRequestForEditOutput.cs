namespace Base2.Tracker.Dtos
{
    public class GetBudgetRequestForEditOutput
    {
        public CreateOrEditBudgetRequestDto BudgetRequest { get; set; }

        public string BudgetDescription { get; set; }

        public string BudgetUseDescription { get; set; }
    }
}