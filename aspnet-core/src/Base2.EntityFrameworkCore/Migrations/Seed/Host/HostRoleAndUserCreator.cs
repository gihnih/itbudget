using Abp;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Abp.Notifications;
using Base2.Authorization;
using Base2.Authorization.Roles;
using Base2.Authorization.Users;
using Base2.EntityFrameworkCore;
using Base2.Notifications;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Base2.Migrations.Seed.Host
{
    public class HostRoleAndUserCreator
    {
        private readonly Base2DbContext _context;

        public HostRoleAndUserCreator(Base2DbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateHostRoleAndUsers();
        }

        private void CreateHostRoleAndUsers()
        {
            //Admin role for host

            var adminRoleForHost = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == null && r.Name == StaticRoleNames.Host.Admin);
            if (adminRoleForHost == null)
            {
                adminRoleForHost = _context.Roles.Add(new Role(null, StaticRoleNames.Host.Admin, StaticRoleNames.Host.Admin) { IsStatic = true, IsDefault = true }).Entity;
                _context.SaveChanges();
            }

            //admin user for host

            var adminUserForHost = _context.Users.IgnoreQueryFilters().FirstOrDefault(u => u.TenantId == null && u.UserName == AbpUserBase.AdminUserName);
            if (adminUserForHost == null)
            {
                var user = new User
                {
                    TenantId = null,
                    UserName = AbpUserBase.AdminUserName,
                    Name = "admin",
                    Surname = "admin",
                    EmailAddress = "admin@aspnetzero.com",
                    IsEmailConfirmed = true,
                    ShouldChangePasswordOnNextLogin = true,
                    IsActive = true,
                    Password = "AM4OLBpptxBYmM79lGOX9egzZk3vIQU3d/gFCJzaBjAPXzYIK3tQ2N7X4fcrHtElTw==" //123qwe
                };

                user.SetNormalizedNames();

                adminUserForHost = _context.Users.Add(user).Entity;
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(null, adminUserForHost.Id, adminRoleForHost.Id));
                _context.SaveChanges();

                //Grant all permissions
                var permissions = PermissionFinder
                    .GetAllPermissions(new AppAuthorizationProvider(true))
                    .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Host))
                    .ToList();

                foreach (var permission in permissions)
                {
                    _context.Permissions.Add(
                        new RolePermissionSetting
                        {
                            TenantId = null,
                            Name = permission.Name,
                            IsGranted = true,
                            RoleId = adminRoleForHost.Id
                        });
                }

                _context.SaveChanges();

                //User account of admin user
                _context.UserAccounts.Add(new UserAccount
                {
                    TenantId = null,
                    UserId = adminUserForHost.Id,
                    UserName = AbpUserBase.AdminUserName,
                    EmailAddress = adminUserForHost.EmailAddress
                });

                _context.SaveChanges();

                //Notification subscriptions
                _context.NotificationSubscriptions.Add(new NotificationSubscriptionInfo(SequentialGuidGenerator.Instance.Create(), null, adminUserForHost.Id, AppNotificationNames.NewTenantRegistered));
                _context.NotificationSubscriptions.Add(new NotificationSubscriptionInfo(SequentialGuidGenerator.Instance.Create(), null, adminUserForHost.Id, AppNotificationNames.NewUserRegistered));

                _context.SaveChanges();
            }

            ////Custodian

            ////Custodian role for host

            //var custodianRoleForHost = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == null && r.Name == StaticRoleNames.Host.Custodian);
            //if (custodianRoleForHost == null)
            //{
            //    custodianRoleForHost = _context.Roles.Add(new Role(null, StaticRoleNames.Host.Custodian, StaticRoleNames.Host.Custodian) { IsStatic = true, IsDefault = false }).Entity;
            //    _context.SaveChanges();
            //}

            ////PermissionGrantInfo permission to custodian role
            //string[] permissionRole = {
            //        "Pages_Dashboards",
            //        "Pages_Dashboards_ItDashboard",
            //        "Pages_PendingActivity",
            //        "Pages_PendingActivity_ItCustodian",
            //        "Pages_Budgets",
            //        "Pages_Budgets_Create",
            //        "Pages_Budgets_Edit",
            //        "Pages_Budgets_Delete",

            //    };

            //foreach (string value in permissionRole)
            //{
            //    _context.Permissions.Add(
            //         new RolePermissionSetting
            //         {
            //             TenantId = null,
            //             Name = value,
            //             IsGranted = true,
            //             RoleId = adminRoleForHost.Id
            //         });
            //}

            //_context.SaveChanges();
        }

        //private void CreateCustodianRoleAndPermission()
        //{
        //    //Custodian role for host

        //    var custodianRoleForHost = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == null && r.Name == StaticRoleNames.Host);
        //    if (adminRoleForHost == null)
        //    {
        //        adminRoleForHost = _context.Roles.Add(new Role(null, StaticRoleNames.Host.Admin, StaticRoleNames.Host.Admin) { IsStatic = true, IsDefault = true }).Entity;
        //        _context.SaveChanges();
        //    }
        //}
    }
}