using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public class UseStatusesExcelExporter : EpPlusExcelExporterBase, IUseStatusesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public UseStatusesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) :
    base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetUseStatusForViewDto> useStatuses)
        {
            return CreateExcelPackage(
                "UseStatuses.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("UseStatuses"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, useStatuses,
                        _ => _.UseStatus.Name
                        );
                });
        }
    }
}