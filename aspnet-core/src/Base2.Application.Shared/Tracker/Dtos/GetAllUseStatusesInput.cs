using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class GetAllUseStatusesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }
    }
}