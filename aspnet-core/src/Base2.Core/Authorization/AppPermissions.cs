﻿namespace Base2.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_SampleEntities = "Pages.SampleEntities";
        public const string Pages_SampleEntities_Create = "Pages.SampleEntities.Create";
        public const string Pages_SampleEntities_Edit = "Pages.SampleEntities.Edit";
        public const string Pages_SampleEntities_Delete = "Pages.SampleEntities.Delete";

        //public const string Pages_BudgetApprovers = "Pages.BudgetApprovers";
        //public const string Pages_BudgetApprovers_Create = "Pages.BudgetApprovers.Create";
        //public const string Pages_BudgetApprovers_Edit = "Pages.BudgetApprovers.Edit";
        //public const string Pages_BudgetApprovers_Delete = "Pages.BudgetApprovers.Delete";

        public const string Pages_Dashboards = "Pages.Dashboards";
        public const string Pages_Dashboards_ItDashboard = "Pages.Dashboard.It";
        public const string Pages_Dashboards_MyDepartment = "Pages.Dashboard.MyDepartment";

        public const string Pages_PendingActivity = "Pages.PendingActivity";
        public const string Pages_PendingActivity_ItCustodian = "Pages.MyTask.ItCustodian";
        public const string Pages_PendingActivity_MyDepartment = "Pages.MyTask.MyTask";

        public const string Pages_Budgets = "Pages.Budgets";
        public const string Pages_Budgets_Create = "Pages.Budgets.Create";
        public const string Pages_Budgets_Edit = "Pages.Budgets.Edit";
        public const string Pages_Budgets_Delete = "Pages.Budgets.Delete";

        public const string Pages_MyBudgets = "Pages.MyBudgets";
        public const string Pages_MyBudgets_Cancel = "Pages.MyBudgets.Cancel";
        public const string Pages_MyBudgets_View = "Pages.MyBudgets.View";
        public const string Pages_MyBudgets_Unbudgeted = "Pages.Mybudgets.Unbudgeted";

        public const string Pages_BudgetUses = "Pages.BudgetUses";
        public const string Pages_BudgetUses_Create = "Pages.BudgetUses.Create";
        public const string Pages_BudgetUses_Edit = "Pages.BudgetUses.Edit";
        public const string Pages_BudgetUses_Delete = "Pages.BudgetUses.Delete";

        public const string Pages_DepartmentDashboard = "Pages.DepartmentDashboard";

        public const string Pages_ItDashboard = "Pages.ItDashboard";

        public const string Pages_MasterData = "Pages.MasterData";

        //public const string Pages_ItDashboard = "Pages.ItDashboard";
        //public const string Pages_DepartmentDashboard = "Pages.DepartmentDashboard";
        //public const string Pages_PendingActivity = "Pages.PendingActivity";

        //public const string Pages_BudgetRequests = "Pages.BudgetRequests";
        //public const string Pages_BudgetRequests_Create = "Pages.BudgetRequests.Create";
        //public const string Pages_BudgetRequests_Edit = "Pages.BudgetRequests.Edit";
        //public const string Pages_BudgetRequests_Delete = "Pages.BudgetRequests.Delete";

        //public const string Pages_UseStatuses = "Pages.UseStatuses";
        //public const string Pages_UseStatuses_Create = "Pages.UseStatuses.Create";
        //public const string Pages_UseStatuses_Edit = "Pages.UseStatuses.Edit";
        //public const string Pages_UseStatuses_Delete = "Pages.UseStatuses.Delete";

        //public const string Pages_BudgetYears = "Pages.BudgetYears";
        //public const string Pages_BudgetYears_Create = "Pages.BudgetYears.Create";
        //public const string Pages_BudgetYears_Edit = "Pages.BudgetYears.Edit";
        //public const string Pages_BudgetYears_Delete = "Pages.BudgetYears.Delete";

        //public const string Pages_DescriptionTypes = "Pages.DescriptionTypes";
        //public const string Pages_DescriptionTypes_Create = "Pages.DescriptionTypes.Create";
        //public const string Pages_DescriptionTypes_Edit = "Pages.DescriptionTypes.Edit";
        //public const string Pages_DescriptionTypes_Delete = "Pages.DescriptionTypes.Delete";

        public const string Pages_Administration_GLCodes = "Pages.Administration.GLCodes";
        public const string Pages_Administration_GLCodes_Create = "Pages.Administration.GLCodes.Create";
        public const string Pages_Administration_GLCodes_Edit = "Pages.Administration.GLCodes.Edit";
        public const string Pages_Administration_GLCodes_Delete = "Pages.Administration.GLCodes.Delete";

        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";
    }
}