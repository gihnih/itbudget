﻿using Abp.Authorization;
using Base2.Authorization.Roles;
using Base2.Authorization.Users;

namespace Base2.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}