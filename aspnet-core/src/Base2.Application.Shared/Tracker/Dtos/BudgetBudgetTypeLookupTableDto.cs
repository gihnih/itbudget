namespace Base2.Tracker.Dtos
{
    public class BudgetBudgetTypeLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}