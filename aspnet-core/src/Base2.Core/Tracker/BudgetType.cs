namespace Base2.Tracker
{
    public enum BudgetType
    {
        OPEX = 1,
        CAPEX = 2
    }
}