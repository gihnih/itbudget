import {Component, Injector, OnInit, ViewChild} from '@angular/core';
import {appModuleAnimation} from '@shared/animations/routerTransition';
import {AppComponentBase} from '@shared/common/app-component-base';
import {
    BudgetsServiceProxy, DashboardItDto,
    HostDashboardServiceProxy,
    UserServiceProxy
} from '@shared/service-proxies/service-proxies';
import {LazyLoadEvent} from '@node_modules/primeng/components/common/lazyloadevent';
import {DataTable} from '@node_modules/primeng/components/datatable/datatable';
import {Paginator} from '@node_modules/primeng/components/paginator/paginator';

@Component({
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less'],
    animations: [appModuleAnimation()]
})
export class DashboardComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    dashboardIt: DashboardItDto = new DashboardItDto();
    years = [];
    currentYear;


    budgetData: any[];
    view: any[];
    colorScheme: any;
    colorSets: any;
    selectedColorScheme: string;
    opexData: any[] = [];
    capexData: any[] = [];

    opexPie: any[] = [];
    capexPie: any[] = [];
    ttlOpex = 0;
    utiOpex = 0;
    ttlCapex = 0;
    utiCapex = 0;

    // option
    gradient = false;
    tooltipDisabled = false;
    animations = true;
    showLabels = true;
    doughnut = false;
    budget: any[] = [];


    //filter Budget
    filterText = '';
    descriptionFilter = '';
    budgetCategory = '';
    maxBudgetAmountFilter: number;
    maxBudgetAmountFilterEmpty: number;
    minBudgetAmountFilter: number;
    minBudgetAmountFilterEmpty: number;
    maxUtilizedBudgetFilter: number;
    maxUtilizedBudgetFilterEmpty: number;
    minUtilizedBudgetFilter: number;
    minUtilizedBudgetFilterEmpty: number;
    maxCurrentAmountFilter: number;
    maxCurrentAmountFilterEmpty: number;
    minCurrentAmountFilter: number;
    minCurrentAmountFilterEmpty: number;
    remarkFilter = '';
    categoryFilter = '';
    budgetTypeNameFilter = '';
    departmentNameFilter = '';
    glCodeCodeFilter = '';
    descriptionTypeNameFilter = '';
    budgetYearYearFilter = '';
    codeOrganizationUnit: any[] = [];
    sorting = '';
    skipCount = 0;
    max = 1000000;

    OpexBudgetSummary: BudgetSummary[] = [
        {unit: 'ITIR', totalBudget: 10000000, totalUtilize: 200000},
        {unit: 'ITEA', totalBudget: 30000000, totalUtilize: 10000},
        {unit: 'ITDI', totalBudget: 40000000, totalUtilize: 5000000},
        {unit: 'IDEA', totalBudget: 50000000, totalUtilize: 890000},
    ];

    CapexBudgetSummary: BudgetSummary[] = [
        {unit: 'ITIR', totalBudget: 11200000, totalUtilize: 200000},
        {unit: 'ITEA', totalBudget: 4300000, totalUtilize: 23000},
        {unit: 'ITDI', totalBudget: 980000, totalUtilize: 780000},
        {unit: 'IDEA', totalBudget: 1200000, totalUtilize: 20030},
    ];


    displayedColumns = ['description', 'budgetAmount', 'utilizedBudget', 'year'];
    dataSource = budgetSampleData;
    private organizationUnitDisplayNameFilter: string;

    constructor(
        injector: Injector,
        private _dashboardProxy: HostDashboardServiceProxy,
        private _budgetService: BudgetsServiceProxy
    ) {
        super(injector);
        this.initialDate();
        this.getDashboardData();
        this.colorSets = colorSets;
        this.setColorScheme('vivid');
        // this.getBudgetData();
    }

    initialDate() {
        this.currentYear = new Date().getFullYear();
        this.years.push(new Date().getFullYear() - 1);
        this.years.push(new Date().getFullYear());
        this.years.push(new Date().getFullYear() + 1);
    }

    getNewData() {
        this.getDashboardData();
    }

    // getBudgetData() {
    //     // console.log(this.codeOrganizationUnit);
    //     // console.log(this.appSession.organizationUnit);
    //     // this.codeOrganizationUnit = this.appSession.organizationUnit;
    //     // console.log(this.codeOrganizationUnit );
    //     this._budgetService.getAll(
    //         this.filterText,
    //         this.descriptionFilter,
    //         this.budgetCategory,
    //         this.categoryFilter,
    //         this.budgetTypeNameFilter,
    //         this.departmentNameFilter,
    //         this.glCodeCodeFilter,
    //         this.descriptionTypeNameFilter,
    //         this.budgetYearYearFilter,
    //         this.codeOrganizationUnit,
    //         this.sorting,
    //         this.skipCount,
    //         this.max
    //     ).subscribe(result => {
    //         for (let item of result.items) {
    //             this.budget.push(item);
    //         }
    //         for (let b of this.budget) {
    //             console.log(b);
    //
    //             if (b.budgetType === 'OPEX' && b.budgetCategory === 'Budgeted') { //Opex
    //                 this.opexData.push({
    //                     description: b.description,
    //                     budgetAmount: b.budgetAmount,
    //                     utilizedBudget: b.utilizedBudget,
    //                     year: b.budgetYear
    //                 });
    //
    //                 this.ttlOpex += b.budgetAmount;
    //                 this.utiOpex += b.utilizedBudget;
    //             } else if (b.budgetType === 'CAPEX' && b.budgetCategory === 'Budgeted') { //Capex
    //                 this.capexData.push({
    //                     description: b.description,
    //                     budgetAmount: b.budgetAmount,
    //                     utilizedBudget: b.utilizedBudget,
    //                     year: b.budgetYear
    //                 });
    //                 this.ttlCapex += b.budgetAmount;
    //                 this.utiCapex += b.utilizedBudget;
    //             }
    //         }
    //         console.log(this.budget);
    //
    //         this.opexPie = [
    //             {name: 'Utilized', value: this.utiOpex},
    //             {name: 'Not Utilized', value: this.ttlOpex - this.utiOpex}
    //         ];
    //         this.capexPie = [
    //             {name: 'Utilized', value: this.utiCapex},
    //             {name: 'Not Utilized', value: this.ttlCapex - this.utiCapex}
    //         ];
    //
    //     });
    //
    // }

    getDashboardData() {
        this.opexData = [];
        this.capexData = [];
        this._dashboardProxy.getDashboardIt(
            this.currentYear
        ).subscribe(result => {
            this.dashboardIt = result;
            this.opexPie = [
                {name: 'Utilized', value: result.totalOpexUtilized},
                {name: 'Not Utilized', value: result.totalOpexBudgeted - result.totalOpexUtilized}
            ];
            this.capexPie = [
                {name: 'Utilized', value: result.totalCapexUtilized},
                {name: 'Not Utilized', value: result.totalCapexBudgeted - result.totalCapexUtilized}
            ];
        });
    }

    ngOnInit() {

        Object.assign(this, {
            budgetData,
            colorSets,
        });

    }

    getOpexBudgetSummary($event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging($event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        this.primengDatatableHelper.totalRecordsCount = this.OpexBudgetSummary.length;
        this.primengDatatableHelper.records = this.OpexBudgetSummary;
        this.primengDatatableHelper.hideLoadingIndicator();
    }

    getCapexBudgetSummary(event?: LazyLoadEvent) {
        // get Budgets
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengDatatableHelper.showLoadingIndicator();

        this._budgetService.getAll(
            this.filterText,
            this.budgetCategory,
            'CAPEX',
            this.glCodeCodeFilter,
            this.descriptionTypeNameFilter,
            this.budgetYearYearFilter,
            this.organizationUnitDisplayNameFilter,
            this.codeOrganizationUnit,
            this.remarkFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.organizationUnitDisplayNameFilter = '';
            console.log(result.items);

        });

        this.primengDatatableHelper.totalRecordsCount = this.CapexBudgetSummary.length;
        this.primengDatatableHelper.records = this.CapexBudgetSummary;
        this.primengDatatableHelper.hideLoadingIndicator();
    }

    setColorScheme(name) {
        this.selectedColorScheme = name;
        this.colorScheme = this.colorSets.find(s => s.name === name);
    }

    selectPie() {
        console.log('hai');
    }
}

const budgetData: any[] = this.opexPie;


export let colorSets = [
    {
        name: 'vivid',
        selectable: true,
        group: 'Ordinal',
        domain: [
            '#068a43', '#edcf77', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514'
        ]
    },
    {
        name: 'natural',
        selectable: true,
        group: 'Ordinal',
        domain: [
            '#bf9d76', '#e99450', '#d89f59', '#f2dfa7', '#a5d7c6', '#7794b1', '#afafaf', '#707160', '#ba9383', '#d9d5c3'
        ]
    },
    {
        name: 'cool',
        selectable: true,
        group: 'Ordinal',
        domain: [
            '#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886'
        ]
    },
    {
        name: 'fire',
        selectable: true,
        group: 'Ordinal',
        domain: [
            '#ff3d00', '#bf360c', '#ff8f00', '#ff6f00', '#ff5722', '#e65100', '#ffca28', '#ffab00'
        ]
    },
    {
        name: 'solar',
        selectable: true,
        group: 'Continuous',
        domain: [
            '#fff8e1', '#ffecb3', '#ffe082', '#ffd54f', '#ffca28', '#ffc107', '#ffb300', '#ffa000', '#ff8f00', '#ff6f00'
        ]
    },
    {
        name: 'air',
        selectable: true,
        group: 'Continuous',
        domain: [
            '#e1f5fe', '#b3e5fc', '#81d4fa', '#4fc3f7', '#29b6f6', '#03a9f4', '#039be5', '#0288d1', '#0277bd', '#01579b'
        ]
    },
    {
        name: 'aqua',
        selectable: true,
        group: 'Continuous',
        domain: [
            '#e0f7fa', '#b2ebf2', '#80deea', '#4dd0e1', '#26c6da', '#00bcd4', '#00acc1', '#0097a7', '#00838f', '#006064'
        ]
    },
    {
        name: 'flame',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#A10A28', '#D3342D', '#EF6D49', '#FAAD67', '#FDDE90', '#DBED91', '#A9D770', '#6CBA67', '#2C9653', '#146738'
        ]
    },
    {
        name: 'ocean',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#1D68FB', '#33C0FC', '#4AFFFE', '#AFFFFF', '#FFFC63', '#FDBD2D', '#FC8A25', '#FA4F1E', '#FA141B', '#BA38D1'
        ]
    },
    {
        name: 'forest',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#55C22D', '#C1F33D', '#3CC099', '#AFFFFF', '#8CFC9D', '#76CFFA', '#BA60FB', '#EE6490', '#C42A1C', '#FC9F32'
        ]
    },
    {
        name: 'horizon',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#2597FB', '#65EBFD', '#99FDD0', '#FCEE4B', '#FEFCFA', '#FDD6E3', '#FCB1A8', '#EF6F7B', '#CB96E8', '#EFDEE0'
        ]
    },
    {
        name: 'neons',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#FF3333', '#FF33FF', '#CC33FF', '#0000FF', '#33CCFF', '#33FFFF', '#33FF66', '#CCFF33', '#FFCC00', '#FF6600'
        ]
    },
    {
        name: 'picnic',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#FAC51D', '#66BD6D', '#FAA026', '#29BB9C', '#E96B56', '#55ACD2', '#B7332F', '#2C83C9', '#9166B8', '#92E7E8'
        ]
    },
    {
        name: 'night',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#2B1B5A', '#501356', '#183356', '#28203F', '#391B3C', '#1E2B3C', '#120634',
            '#2D0432', '#051932', '#453080', '#75267D', '#2C507D', '#4B3880', '#752F7D', '#35547D'
        ]
    },
    {
        name: 'nightLights',
        selectable: false,
        group: 'Ordinal',
        domain: [
            '#4e31a5', '#9c25a7', '#3065ab', '#57468b', '#904497', '#46648b',
            '#32118d', '#a00fb3', '#1052a2', '#6e51bd', '#b63cc3', '#6c97cb', '#8671c1', '#b455be', '#7496c3'
        ]
    }
];

export interface BudgetElement {
    description: string;
    budgetAmount: number;
    utilizedBudget: number;
    year: number;
}

const budgetSampleData: BudgetElement[] = [
    {description: 'EDW (Core Banking Retrofit)', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
    {description: 'EDW (Core Banking Retrofit)2', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
    {description: 'EDW (Core Banking Retrofit)3', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
    {description: 'EDW (Core Banking Retrofit)4', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
    {description: 'EDW (Core Banking Retrofit)5', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
    {description: 'EDW (Core Banking Retrofit)6', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
    {description: 'EDW (Core Banking Retrofit)7', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
    {description: 'EDW (Core Banking Retrofit)8', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
    {description: 'EDW (Core Banking Retrofit)9', budgetAmount: 5000000, utilizedBudget: 1000000, year: 2019},
];


export interface Element {
    name: string;
    position: number;
    weight: number;
    symbol: string;
}

const ELEMENT_DATA: Element[] = [
    {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
    {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
    {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
    {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
    {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
    {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
    {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
    {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
    {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
    {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
    {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
    {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
    {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
    {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
    {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
    {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
    {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
    {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
];


export interface Year {
    value: number;
    viewValue: string;
}

export interface BudgetSummary {
    unit: string;
    totalBudget: number;
    totalUtilize: number;
}

