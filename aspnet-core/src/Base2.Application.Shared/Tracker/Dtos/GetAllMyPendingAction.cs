﻿using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class GetAllMyPendingAction : PagedAndSortedResultRequestDto
    {
        public int? BudgetId { get; set; }
        public BudgetDto Budget { get; set; }

        public int? BudgetUseId { get; set; }
        public BudgetUse2Dto BudgetUse { get; set; }

        public decimal RequestAmount { get; set; }
        public decimal ActualAmount { get; set; }

        public bool OwnBudget { get; set; }

        public string RequestStatus { get; set; }

        public long CreatorUserId { get; set; }
    }
}