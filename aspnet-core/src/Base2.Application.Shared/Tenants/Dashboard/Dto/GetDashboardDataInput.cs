namespace Base2.Tenants.Dashboard.Dto
{
    public class GetDashboardDataInput
    {
        public SalesSummaryDatePeriod SalesSummaryDatePeriod { get; set; }
    }
}