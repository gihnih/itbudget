﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Sample.Dtos;
using Base2.Dto;

namespace Base2.Sample
{
    public interface ISampleEntitiesAppService : IApplicationService 
    {
        Task<PagedResultDto<GetSampleEntityForViewDto>> GetAll(GetAllSampleEntitiesInput input);

        Task<GetSampleEntityForViewDto> GetSampleEntityForView(int id);

		Task<GetSampleEntityForEditOutput> GetSampleEntityForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditSampleEntityDto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetSampleEntitiesToExcel(GetAllSampleEntitiesForExcelInput input);

		
    }
}