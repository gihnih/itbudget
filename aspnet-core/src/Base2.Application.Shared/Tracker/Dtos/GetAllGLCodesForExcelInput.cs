namespace Base2.Tracker.Dtos
{
    public class GetAllGLCodesForExcelInput
    {
        public string Filter { get; set; }

        public string CodeFilter { get; set; }

        public string DescriptionFilter { get; set; }
    }
}