using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Base2.Authorization;
using Base2.Dto;
using Base2.Tracker.Dtos;
using Base2.Tracker.Exporting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    [AbpAuthorize(AppPermissions.Pages_Administration_GLCodes)]
    public class GLCodesAppService : Base2AppServiceBase, IGLCodesAppService
    {
        private readonly IRepository<GLCode> _glCodeRepository;
        private readonly IGLCodesExcelExporter _glCodesExcelExporter;

        public GLCodesAppService(IRepository<GLCode> glCodeRepository, IGLCodesExcelExporter glCodesExcelExporter)
        {
            _glCodeRepository = glCodeRepository;
            _glCodesExcelExporter = glCodesExcelExporter;
        }

        public async Task<PagedResultDto<GetGLCodeForViewDto>> GetAll(GetAllGLCodesInput input)
        {
            var filteredGLCodes = _glCodeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code.ToLower() == input.CodeFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim());

            var query = (from o in filteredGLCodes
                         select new GetGLCodeForViewDto()
                         {
                             GLCode = ObjectMapper.Map<GLCodeDto>(o)
                         });

            var totalCount = await query.CountAsync();

            var glCodes = await query
                .OrderBy(input.Sorting ?? "glCode.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetGLCodeForViewDto>(
                totalCount,
                glCodes
            );
        }

        public async Task<GetGLCodeForViewDto> GetGLCodeForView(int id)
        {
            var glCode = await _glCodeRepository.GetAsync(id);

            var output = new GetGLCodeForViewDto { GLCode = ObjectMapper.Map<GLCodeDto>(glCode) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_GLCodes_Edit)]
        public async Task<GetGLCodeForEditOutput> GetGLCodeForEdit(EntityDto input)
        {
            var glCode = await _glCodeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetGLCodeForEditOutput { GLCode = ObjectMapper.Map<CreateOrEditGLCodeDto>(glCode) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditGLCodeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_GLCodes_Create)]
        private async Task Create(CreateOrEditGLCodeDto input)
        {
            var glCode = ObjectMapper.Map<GLCode>(input);

            await _glCodeRepository.InsertAsync(glCode);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_GLCodes_Edit)]
        private async Task Update(CreateOrEditGLCodeDto input)
        {
            var glCode = await _glCodeRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, glCode);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_GLCodes_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _glCodeRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetGLCodesToExcel(GetAllGLCodesForExcelInput input)
        {
            var filteredGLCodes = _glCodeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code.ToLower() == input.CodeFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim());

            var query = (from o in filteredGLCodes
                         select new GetGLCodeForViewDto()
                         {
                             GLCode = ObjectMapper.Map<GLCodeDto>(o)
                         });

            var glCodeListDtos = await query.ToListAsync();

            return _glCodesExcelExporter.ExportToFile(glCodeListDtos);
        }


        public async Task<GetGLCodeForViewDto> GetByCode(string code)
        {
            var query = _glCodeRepository.GetAll()
                .Where(e => e.Code.ToLower() == code.ToLower().Trim());

            var glCode = await query.FirstOrDefaultAsync();

            var output = new GetGLCodeForViewDto { GLCode = ObjectMapper.Map<GLCodeDto>(glCode) };

            return output;
        }

    }
}