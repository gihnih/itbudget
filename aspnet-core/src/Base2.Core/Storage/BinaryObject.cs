﻿using Abp;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Base2.Tracker;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base2.Storage
{
    [Table("AppBinaryObjects")]
    public class BinaryObject : FullAuditedEntity<Guid>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }

        [Required]
        public virtual byte[] Bytes { get; set; }

        public virtual int? BudgetUseId { get; set; }
        public virtual BudgetUse BudgetUse { get; set; }

        public virtual string Name { get; set; }
        public virtual string ContentType { get; set; }

        public virtual string Remark { get; set; }

        public BinaryObject()
        {
            Id = SequentialGuidGenerator.Instance.Create();
        }

        public BinaryObject(int? tenantId, byte[] bytes)
            : this()
        {
            TenantId = tenantId;
            Bytes = bytes;
        }
    }
}