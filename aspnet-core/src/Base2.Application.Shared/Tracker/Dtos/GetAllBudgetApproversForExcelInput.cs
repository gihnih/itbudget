namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetApproversForExcelInput
    {
        public string Filter { get; set; }

        public long? MaxUserIdFilter { get; set; }
        public long? MinUserIdFilter { get; set; }

        public int? MaxBudgetUseIdFilter { get; set; }
        public int? MinBudgetUseIdFilter { get; set; }

        public int? MaxBudgetIdFilter { get; set; }
        public int? MinBudgetIdFilter { get; set; }

        public int? MaxUseStatusIdFilter { get; set; }
        public int? MinUseStatusIdFilter { get; set; }

        public decimal? MaxRequestAmountFilter { get; set; }
        public decimal? MinRequestAmountFilter { get; set; }

        public decimal? MaxActualAmountFilter { get; set; }
        public decimal? MinActualAmountFilter { get; set; }
    }
}