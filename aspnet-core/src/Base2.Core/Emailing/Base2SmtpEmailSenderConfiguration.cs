﻿using Abp.Configuration;
using Abp.Net.Mail;
using Abp.Net.Mail.Smtp;
using Abp.Runtime.Security;

namespace Base2.Emailing
{
    public class Base2SmtpEmailSenderConfiguration : SmtpEmailSenderConfiguration
    {
        public Base2SmtpEmailSenderConfiguration(ISettingManager settingManager) : base(settingManager)
        {
        }

        public override string Password => SimpleStringCipher.Instance.Decrypt(GetNotEmptySettingValue(EmailSettingNames.Smtp.Password));
    }
}