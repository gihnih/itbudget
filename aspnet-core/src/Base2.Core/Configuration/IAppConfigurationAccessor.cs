﻿using Microsoft.Extensions.Configuration;

namespace Base2.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}