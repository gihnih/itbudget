﻿namespace Base2.Tracker.Dtos
{
    public class EditBudgetApproveDto
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public long UserId { get; set; }
    }
}