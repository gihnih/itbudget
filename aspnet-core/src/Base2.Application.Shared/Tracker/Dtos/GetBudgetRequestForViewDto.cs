namespace Base2.Tracker.Dtos
{
    public class GetBudgetRequestForViewDto
    {
        public BudgetRequestDto BudgetRequest { get; set; }

        public string BudgetDescription { get; set; }

        public string BudgetUseDescription { get; set; }
    }
}