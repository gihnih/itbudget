﻿using Abp.Localization;
using System.ComponentModel.DataAnnotations;

namespace Base2.Localization.Dto
{
    public class SetDefaultLanguageInput
    {
        [Required]
        [StringLength(ApplicationLanguage.MaxNameLength)]
        public virtual string Name { get; set; }
    }
}