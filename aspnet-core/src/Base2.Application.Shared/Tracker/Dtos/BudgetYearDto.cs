using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class BudgetYearDto : EntityDto
    {
        public int Year { get; set; }
    }
}