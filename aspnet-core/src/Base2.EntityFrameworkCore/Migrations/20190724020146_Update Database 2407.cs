﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class UpdateDatabase2407 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BudgetId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "CustodianStatus",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "UserApprovalId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "UserRequestId",
                table: "BudgetUses");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BudgetId",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustodianStatus",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserApprovalId",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "UserRequestId",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0L);
        }
    }
}