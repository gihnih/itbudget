using Abp;

namespace Base2
{
    /// <summary>
    /// This class can be used as a base class for services in this application.
    /// It has some useful objects property-injected and has some basic methods most of services may need to.
    /// It's suitable for non domain nor application service classes.
    /// For domain services inherit <see cref="Base2DomainServiceBase"/>.
    /// For application services inherit Base2AppServiceBase.
    /// </summary>
    public abstract class Base2ServiceBase : AbpServiceBase
    {
        protected Base2ServiceBase()
        {
            LocalizationSourceName = Base2Consts.LocalizationSourceName;
        }
    }
}