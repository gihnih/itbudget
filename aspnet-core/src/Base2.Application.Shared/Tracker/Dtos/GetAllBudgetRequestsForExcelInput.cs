namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetRequestsForExcelInput
    {
        public string Filter { get; set; }

        public string BudgetDescriptionFilter { get; set; }

        public string BudgetUseDescriptionFilter { get; set; }
    }
}