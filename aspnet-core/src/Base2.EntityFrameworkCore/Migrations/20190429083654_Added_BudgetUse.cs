﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class Added_BudgetUse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BudgetUses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActualAmount = table.Column<decimal>(nullable: false),
                    BudgetAmount = table.Column<decimal>(nullable: false),
                    CurrentAmount = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    RequestAmount = table.Column<decimal>(nullable: false),
                    UseStatusId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetUses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BudgetUses_UseStatuses_UseStatusId",
                        column: x => x.UseStatusId,
                        principalTable: "UseStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BudgetUses_UseStatusId",
                table: "BudgetUses",
                column: "UseStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BudgetUses");
        }
    }
}