import {Component, Injector, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
    BudgetsServiceProxy,
    BudgetDto,
    UserServiceProxy,
    GetGLCodeForViewDto, BudgetGLCodeLookupTableDto, BudgetOrganizationUnitLookupTableDto, BudgetUsesServiceProxy
} from '@shared/service-proxies/service-proxies';
import {NotifyService} from '@abp/notify/notify.service';
import {AppComponentBase} from '@shared/common/app-component-base';
import {TokenAuthServiceProxy} from '@shared/service-proxies/service-proxies';
import {DataTable} from 'primeng/components/datatable/datatable';
import {Paginator} from 'primeng/components/paginator/paginator';
import {LazyLoadEvent} from 'primeng/components/common/lazyloadevent';
import {FileDownloadService} from '@shared/utils/file-download.service';
import {CreateOrEditBudgetModalComponent} from '@app/main/tracker/budgets/create-or-edit-budget-modal.component';
import {ViewBudgetModalComponent} from '@app/main/tracker/budgets/view-budget-modal.component';
import {appModuleAnimation} from '@shared/animations/routerTransition';

@Component({
    selector: 'app-unbudgets',
    templateUrl: './unbudgets.component.html',
    styleUrls: ['./unbudgets.component.css'],
    animations: [appModuleAnimation()]
})
export class UnbudgetsComponent extends AppComponentBase {

    @ViewChild('createOrEditBudgetModal') createOrEditBudgetModal: CreateOrEditBudgetModalComponent;
    @ViewChild('viewBudgetModalComponent') viewBudgetModal: ViewBudgetModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    // data init
    budgetTypes = [];
    departments: BudgetOrganizationUnitLookupTableDto[] = [];

    // filter
    filter = '';
    budgetTypeFilter = '';
    budgetCategory = 'Unbudgeted';
    descriptionTypeFilter = '';
    glCodeFilter = '';
    budgetYearFilter = '';
    organizationUnitFilter = '';
    remarkFilter = '';
    descriptionFilter = '';
    categoryFilter = '';

    advancedFiltersAreShown = false;
    codeOrganizationUnit: any[] = [];

    shownLoginName = '';
    tenancyName = '';
    userName = '';
    budget = [];

    type;

    userId = this.appSession.userId;
    private userRole: string = null;

    constructor(
        injector: Injector,
        private _budgetsServiceProxy: BudgetsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _budgetUseServiceProxy: BudgetUsesServiceProxy
    ) {
        super(injector);
        this.initialData();
        this._budgetUseServiceProxy.getListOrganizationUnit().subscribe(result => {
            this.departments = result;
        });
    }

    initialData() {
        this._budgetsServiceProxy.getBudgetType().subscribe(result => {
            this.budgetTypes = result;
        });
    }

    getBudgets(event?: LazyLoadEvent) {

        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengDatatableHelper.showLoadingIndicator();

        this._budgetsServiceProxy.getAll(
            this.filter,
            this.budgetCategory,
            this.budgetTypeFilter,
            this.glCodeFilter,
            this.descriptionTypeFilter,
            this.budgetYearFilter,
            this.organizationUnitFilter,
            this.codeOrganizationUnit,
            this.remarkFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
            console.log(result);
        });
    }


    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createBudget(): void {
        this.createOrEditBudgetModal.show();
    }

    deleteBudget(budget: BudgetDto): void {
        console.log(budget);
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._budgetsServiceProxy.delete(budget.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._budgetsServiceProxy.getBudgetsToExcel(
            this.filter,
            this.descriptionFilter,
            this.remarkFilter,
            this.categoryFilter,
            this.budgetTypeFilter,
            this.glCodeFilter,
            this.descriptionTypeFilter,
            this.budgetYearFilter,
            this.organizationUnitFilter,
            this.budgetCategory
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    getCurrentLoginInformations(): void {
        console.log('s');
        this.shownLoginName = this.appSession.getShownLoginName();
        this.tenancyName = this.appSession.tenancyName;
        this.userName = this.appSession.user.userName;
    }
}
