﻿using Abp.Application.Services.Dto;
using Base2.MultiTenancy.Accounting.Dto;
using System.Threading.Tasks;

namespace Base2.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}