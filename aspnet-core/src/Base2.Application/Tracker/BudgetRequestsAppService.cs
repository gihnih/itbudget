using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Abp.UI;
using Base2.Authorization;
using Base2.Authorization.Users;
using Base2.Dto;
using Base2.Tracker.Dtos;
using Base2.Tracker.Exporting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    [Audited]
    [AbpAuthorize(AppPermissions.Pages_BudgetUses)]
    public class BudgetRequestsAppService : Base2AppServiceBase, IBudgetRequestsAppService
    {
        private readonly IRepository<BudgetRequest> _budgetRequestRepository;
        private readonly IBudgetRequestsExcelExporter _budgetRequestsExcelExporter;
        private readonly IRepository<Budget, int> _budgetRepository;
        private readonly IRepository<BudgetUse, int> _budgetUseRepository;
        private readonly UserManager _userManager;
        private readonly IUserEmailer _userEmailer;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;

        public BudgetRequestsAppService(IRepository<BudgetRequest> budgetRequestRepository, IBudgetRequestsExcelExporter budgetRequestsExcelExporter,
            IRepository<Budget, int> budgetRepository, IRepository<BudgetUse, int> budgetUseRepository, UserManager userManager, IUserEmailer userEmailer,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository, IRepository<OrganizationUnit, long> organizationUnitRepository)
        {
            _budgetRequestRepository = budgetRequestRepository;
            _budgetRequestsExcelExporter = budgetRequestsExcelExporter;
            _budgetRepository = budgetRepository;
            _budgetUseRepository = budgetUseRepository;
            _userManager = userManager;
            _userEmailer = userEmailer;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRepository = organizationUnitRepository;
        }

        public async Task<PagedResultDto<GetBudgetRequestForViewDto>> GetAll(GetAllBudgetRequestsInput input)
        {
            var filteredBudgetRequests = _budgetRequestRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false);

            var query = (from o in filteredBudgetRequests
                         join o1 in _budgetRepository.GetAll() on o.BudgetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _budgetUseRepository.GetAll() on o.BudgetUseId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         select new GetBudgetRequestForViewDto()
                         {
                             BudgetRequest = ObjectMapper.Map<BudgetRequestDto>(o),
                             BudgetDescription = s1 == null ? "" : s1.Description.ToString(),
                             BudgetUseDescription = s2 == null ? "" : s2.Description.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetDescriptionFilter), e => e.BudgetDescription.ToLower() == input.BudgetDescriptionFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetUseDescriptionFilter), e => e.BudgetUseDescription.ToLower() == input.BudgetUseDescriptionFilter.ToLower().Trim());

            var totalCount = await query.CountAsync();

            var budgetRequests = await query
                .OrderBy(input.Sorting ?? "budgetRequest.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetBudgetRequestForViewDto>(
                totalCount,
                budgetRequests
            );
        }

        public async Task<PagedResultDto<GetAllMyPendingAction>> GetMyPendingAction(GetAllMyPendingActionInput input)
        {
            try
            {
                var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
                //var user = await _userManager.GetUserByIdAsync(4);
                var organizationUnits = await _userManager.GetOrganizationUnitsAsync(user);
                var organizationUnitCodes = organizationUnits.Select(ou => ou.Code);

                var query = _budgetRequestRepository
                    .GetAll()
                    .Include(b => b.Budget)
                    .Include(bu => bu.BudgetUse)
                        .ThenInclude(ou => ou.OrganizationUnit)
                    .Include(bu => bu.BudgetUse)
                        .ThenInclude(e => e.BinaryObjects)
                    .Include(b => b.Budget)
                        .ThenInclude(gl => gl.GLCode)
                    .Where(ou => organizationUnitCodes.Contains(ou.Budget.OrganizationUnit.Code))
                    .Where(ob => ob.OwnBudget == false)
                    .Where(rs => rs.RequestStatus == RequestStatus.Submitted)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.BudgetUse.Description.ToLower().Trim().Contains(input.Filter.ToLower().Trim()));

                var total = query.Count();
                var pendingAction = await query
                    .Select(item => new GetAllMyPendingAction
                    {
                        BudgetId = item.BudgetId,
                        Budget = new BudgetDto()
                        {
                            Id = item.Budget.Id,
                            Description = item.Budget.Description,
                            BudgetAmount = item.Budget.BudgetAmount,
                            UtilizedBudget = item.Budget.UtilizedBudget,
                            EstimatedBalance = item.Budget.EstimatedBalance,
                            Remark = item.Budget.Remark,
                            BudgetYear = item.Budget.BudgetYear,
                            OrganizationUnitId = item.Budget.OrganizationUnitId,
                            IsBudgeted = item.Budget.IsBudgeted,
                            BudgetCategory = item.Budget.BudgetCategory.ToString(),
                            BudgetType = item.Budget.BudgetType.ToString(),
                            DescriptionType = item.Budget.DescriptionType.ToString(),
                            GLCodeId = item.Budget.GLCodeId,
                            GLCode = item.Budget.GLCode.MapTo<GLCodeDto>(),
                            OrganizationUnit = item.Budget.OrganizationUnit.MapTo<BudgetOrganizationUnitLookupTableDto>(),
                        },
                        ActualAmount = item.ActualAmount,
                        BudgetUse = new BudgetUse2Dto()
                        {
                            Id = item.BudgetUse.Id,
                            Description = item.BudgetUse.Description,
                            RequestAmount = item.BudgetUse.RequestAmount,
                            ActualAmount = item.BudgetUse.ActualAmount,
                            PaperStatus = item.BudgetUse.PaperStatus.ToString(),
                            RequestStatus = item.BudgetUse.RequestStatus.ToString(),
                            OwnBudget = item.OwnBudget,
                            OrganizationUnitId = item.BudgetUse.OrganizationUnitId.Value,
                            OrganizationUnit = item.BudgetUse.OrganizationUnit.MapTo<BudgetOrganizationUnitLookupTableDto>(),
                            BinaryObjects = item.BudgetUse.BinaryObjects.MapTo<ICollection<BinaryObjectDto>>()
                        },
                        BudgetUseId = item.BudgetUseId,
                        RequestAmount = item.RequestAmount,
                        OwnBudget = item.OwnBudget,
                        RequestStatus = item.RequestStatus.ToString(),
                        CreatorUserId = item.CreatorUserId.Value
                    })
                    .OrderBy(input.Sorting ?? "budgetId desc")
                    .PageBy(input)
                    .ToListAsync();

                return new PagedResultDto<GetAllMyPendingAction>(
                        total,
                        pendingAction
                    );
            }
            catch (Exception e)
            {
                var err = e;
                return new PagedResultDto<GetAllMyPendingAction>();
            }
        }

        public async Task<PagedResultDto<GetAllMyPendingAction>> GetCustodianPendingAction(GetAllCustodianPendingActionInput input)
        {
            try
            {
                var query = _budgetRequestRepository
                    .GetAll()
                    .Include(b => b.Budget)
                        .ThenInclude(ou => ou.OrganizationUnit)
                    .Include(bu => bu.BudgetUse)
                        .ThenInclude(ou => ou.OrganizationUnit)
                   .Include(bu => bu.BudgetUse)
                        .ThenInclude(e => e.BinaryObjects)
                    .Include(b => b.Budget)
                        .ThenInclude(gl => gl.GLCode)
                    .Include(bu => bu.BudgetUse)
                    .Where(s => (s.RequestStatus == RequestStatus.Submitted && s.OwnBudget == true) || (s.RequestStatus == RequestStatus.Approved && s.OwnBudget == false))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                    e.BudgetUse.Description.ToLower().Trim().Contains(input.Filter.ToLower().Trim()) ||
                    e.BudgetUse.RequestStatus.ToString().ToLower().Contains(input.Filter.ToLower())
                    )
                    .Select(item => new GetAllMyPendingAction
                    {
                        BudgetId = item.BudgetId,
                        Budget = new BudgetDto()
                        {
                            Id = item.Budget.Id,
                            Description = item.Budget.Description,
                            BudgetAmount = item.Budget.BudgetAmount,
                            UtilizedBudget = item.Budget.UtilizedBudget,
                            EstimatedBalance = item.Budget.EstimatedBalance,
                            Remark = item.Budget.Remark,
                            BudgetYear = item.Budget.BudgetYear,
                            OrganizationUnitId = item.Budget.OrganizationUnitId,
                            IsBudgeted = item.Budget.IsBudgeted,
                            BudgetCategory = item.Budget.BudgetCategory.ToString(),
                            BudgetType = item.Budget.BudgetType.ToString(),
                            DescriptionType = item.Budget.DescriptionType.ToString(),
                            GLCodeId = item.Budget.GLCodeId,
                            GLCode = item.Budget.GLCode.MapTo<GLCodeDto>(),
                            OrganizationUnit = item.Budget.OrganizationUnit.MapTo<BudgetOrganizationUnitLookupTableDto>(),
                        },
                        ActualAmount = item.ActualAmount,
                        BudgetUse = new BudgetUse2Dto()
                        {
                            Id = item.BudgetUse.Id,
                            Description = item.BudgetUse.Description,
                            RequestAmount = item.BudgetUse.RequestAmount,
                            ActualAmount = item.BudgetUse.ActualAmount,
                            PaperStatus = item.BudgetUse.PaperStatus.ToString(),
                            RequestStatus = item.BudgetUse.RequestStatus.ToString(),
                            OwnBudget = item.OwnBudget,
                            OrganizationUnitId = item.BudgetUse.OrganizationUnitId.Value,
                            OrganizationUnit = item.BudgetUse.OrganizationUnit.MapTo<BudgetOrganizationUnitLookupTableDto>(),
                            BinaryObjects = item.BudgetUse.BinaryObjects.MapTo<ICollection<BinaryObjectDto>>()
                        },
                        BudgetUseId = item.BudgetUseId,
                        RequestAmount = item.RequestAmount,
                        OwnBudget = item.OwnBudget,
                        RequestStatus = item.RequestStatus.ToString(),
                        CreatorUserId = item.CreatorUserId.Value
                    });

                var total = query.Count();
                var pendingAction = await query
                    .OrderBy(input.Sorting ?? "budgetId desc")
                    .PageBy(input)
                    .ToListAsync();

                return new PagedResultDto<GetAllMyPendingAction>(
                        total,
                        pendingAction
                    );
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<GetBudgetRequestForViewDto> GetBudgetRequestForView(int id)
        {
            var budgetRequest = await _budgetRequestRepository.GetAsync(id);

            var output = new GetBudgetRequestForViewDto { BudgetRequest = ObjectMapper.Map<BudgetRequestDto>(budgetRequest) };

            if (output.BudgetRequest.BudgetId != null)
            {
                var budget = await _budgetRepository.FirstOrDefaultAsync((int)output.BudgetRequest.BudgetId);
                output.BudgetDescription = budget.Description.ToString();
            }

            if (output.BudgetRequest.BudgetUseId != null)
            {
                var budgetUse = await _budgetUseRepository.FirstOrDefaultAsync((int)output.BudgetRequest.BudgetUseId);
                output.BudgetUseDescription = budgetUse.Description.ToString();
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses_Edit)]
        public async Task<GetBudgetRequestForEditOutput> GetBudgetRequestForEdit(EntityDto input)
        {
            var budgetRequest = await _budgetRequestRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetBudgetRequestForEditOutput { BudgetRequest = ObjectMapper.Map<CreateOrEditBudgetRequestDto>(budgetRequest) };

            if (output.BudgetRequest.BudgetId != null)
            {
                var budget = await _budgetRepository.FirstOrDefaultAsync((int)output.BudgetRequest.BudgetId);
                output.BudgetDescription = budget.Description.ToString();
            }

            if (output.BudgetRequest.BudgetUseId != null)
            {
                var budgetUse = await _budgetUseRepository.FirstOrDefaultAsync((int)output.BudgetRequest.BudgetUseId);
                output.BudgetUseDescription = budgetUse.Description.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditBudgetRequestDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses_Create)]
        private async Task Create(CreateOrEditBudgetRequestDto input)
        {
            var budgetRequest = ObjectMapper.Map<BudgetRequest>(input);
            await _budgetRequestRepository.InsertAsync(budgetRequest);
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses_Edit)]
        public async Task Update(CreateOrEditBudgetRequestDto input)
        {
            try
            {
                // Get current user
                var userId = AbpSession.UserId;

                // Get Budget Request Detail BudgetReqeuest2Dto
                var budgetRequest = await _budgetRequestRepository
                    .GetAll()
                    .Include(b => b.Budget)
                    .Include(bu => bu.BudgetUse)
                    .Where(w => w.BudgetId == input.BudgetId && w.BudgetUseId == input.BudgetUseId)
                    .FirstOrDefaultAsync();

                var requestStatus = RequestStatus.PendingApproval;

                // if reject or decline : Custodian
                if (input.RequestStatus == RequestStatus.Rejected.ToString() || input.RequestStatus == RequestStatus.Declined.ToString())
                {
                    //Update Budget Amount
                    budgetRequest.Budget.EstimatedBalance += budgetRequest.RequestAmount;
                    budgetRequest.Budget.UtilizedBudget -= budgetRequest.RequestAmount;
                }

                //capture status and datetime
                if (budgetRequest.RequestStatus == RequestStatus.Submitted && !budgetRequest.OwnBudget)
                {
                    budgetRequest.BudgetOwnerRequestStatus = (RequestStatus)Enum.Parse(typeof(RequestStatus), input.RequestStatus);
                    budgetRequest.BudgetOwnerApproval = DateTime.Now;
                    budgetRequest.OwnerApprovalId = (int)userId;
                }
                else
                {
                    budgetRequest.BudgetCustodianRequestStatus = (RequestStatus)Enum.Parse(typeof(RequestStatus), input.RequestStatus);
                    budgetRequest.BudgetCustodianApproval = DateTime.Now;
                    budgetRequest.CustodianApprovalId = (int)userId;
                }
                // Update Status
                budgetRequest.RequestStatus = (RequestStatus)Enum.Parse(typeof(RequestStatus), input.RequestStatus);

                //Update BudgetRequest
                //budgetRequest.BudgetUse.RequestStatus = (RequestStatus)Enum.Parse(typeof(RequestStatus), input.RequestStatus);

                // Update Budget Use Status
                var budgetRequestAll = await _budgetRequestRepository
                    .GetAll()
                    .Include(b => b.Budget)
                    .Include(bu => bu.BudgetUse)
                    .Where(w => w.BudgetUseId == input.BudgetUseId)
                    .ToListAsync();

                var budgetUse = await _budgetUseRepository
                    .GetAll()
                    .Where(w => w.Id == input.BudgetUseId)
                    .FirstOrDefaultAsync();

                var currentStatus = "";
                var cancelSubmmition = false;

                foreach (var request in budgetRequestAll)
                {
                    if (budgetRequestAll.First() == request)
                    {
                        currentStatus = request.RequestStatus.ToString();
                    }
                    if (request.RequestStatus == RequestStatus.Declined || request.RequestStatus == RequestStatus.Rejected)
                    {
                        request.BudgetUse.RequestStatus = request.RequestStatus;
                        cancelSubmmition = true;
                        break;
                    }

                    if (!request.RequestStatus.ToString().Equals(currentStatus) || request.RequestStatus == RequestStatus.Approved)
                    {
                        budgetUse.RequestStatus = RequestStatus.PendingApproval;
                        break;
                    }

                    budgetUse.RequestStatus = request.RequestStatus;
                }

                if (cancelSubmmition)
                {
                    foreach (var request in budgetRequestAll)
                    {
                        if (request.RequestStatus == RequestStatus.Approved || request.RequestStatus == RequestStatus.Submitted || request.RequestStatus == RequestStatus.VerifiedAndReserved)
                        {
                            request.Budget.EstimatedBalance += request.RequestAmount;
                            request.Budget.UtilizedBudget -= request.RequestAmount;
                            request.RequestStatus = RequestStatus.Cancel;
                        }
                    }
                }

                // Email notification
                var users = from uou in _userOrganizationUnitRepository.GetAll()
                            join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
                            join user in UserManager.Users on uou.UserId equals user.Id
                            where uou.OrganizationUnitId == budgetRequest.BudgetUse.OrganizationUnitId
                            select new { user };

                var orgUnit = await _budgetRepository.GetAsync(input.BudgetId.Value);
                var budgetOwner = from uou in _userOrganizationUnitRepository.GetAll()
                            join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
                            join user in UserManager.Users on uou.UserId equals user.Id
                            where uou.OrganizationUnitId == orgUnit.OrganizationUnitId
                            select new { user };

                var custodians = await _userManager.GetUsersInRoleAsync("Custodian");

                // if RequestStatus == VerifiedAndReserved , then email to Requester
                if (input.RequestStatus == RequestStatus.VerifiedAndReserved.ToString() || input.RequestStatus == RequestStatus.Declined.ToString()) {
                    // email to requester
                    foreach (var user in users)
                    {
                        await _userEmailer.SendRespondApprovalToRequesterCustodian(ObjectMapper.Map<User>(user.user), input.RequestStatus);
                    }
                } else if(input.RequestStatus == RequestStatus.Approved.ToString() || input.RequestStatus == RequestStatus.Rejected.ToString()){
                    
                    foreach (var user in users)
                    {
                        await _userEmailer.SendRespondApprovalToRequesterBudgetOwner(ObjectMapper.Map<User>(user.user), input.RequestStatus);
                    }
                    if(input.RequestStatus == RequestStatus.Approved.ToString())
                    {
                        foreach (var user in custodians)
                        {
                            await _userEmailer.SendRespondApprovalToRequesterCustodian(ObjectMapper.Map<User>(user), input.RequestStatus);
                        }
                    }

                }


                // else if RequestStatus == Reject, Then email to Requester

                // else if RequestStatus == Verified & Reserved, then email to Requester

                // else if RequestStatus == Decline, then email to Requester


                //// if budget custodian action
                //if (input.RequestStatus == RequestStatus.VerifiedAndReserved.ToString() || input.RequestStatus == RequestStatus.Declined.ToString())
                //{
                //    // send email to custodian
                //    // get email budgetcustodian
                //    var users = await _userManager.GetUsersInRoleAsync("Custodian");

                //    foreach (var user in users)
                //    {
                //        await _userEmailer.SendRespondApprovalToRequesterCustodian(ObjectMapper.Map<User>(user), input.RequestStatus);
                //    }
                //}
                //else
                //{
                //    // send email to budget owner first
                //    // get email budget owner
                //    var orgUnit = await _budgetRepository.GetAsync(input.BudgetId.Value);
                //    var users = from uou in _userOrganizationUnitRepository.GetAll()
                //                join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
                //                join user in UserManager.Users on uou.UserId equals user.Id
                //                where uou.OrganizationUnitId == orgUnit.OrganizationUnitId
                //                select new { user };

                //    foreach (var user in users)
                //    {
                //        await _userEmailer.SendRespondApprovalToRequesterBudgetOwner(ObjectMapper.Map<User>(user.user), input.RequestStatus);
                //    }

                //    // Second Approval (Custodian)
                //    if (input.RequestStatus == RequestStatus.Approved.ToString())
                //    {
                //        var custodianUsers = await _userManager.GetUsersInRoleAsync("Custodian");
                //        foreach (var user in custodianUsers)
                //        {
                //            await _userEmailer.SendTaskBudgetedUseCustodian(ObjectMapper.Map<User>(user), "");
                //        }
                //    }
                //}
            }
            catch (Exception e)
            {
                var err = e;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _budgetRequestRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetBudgetRequestsToExcel(GetAllBudgetRequestsForExcelInput input)
        {
            var filteredBudgetRequests = _budgetRequestRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false);

            var query = (from o in filteredBudgetRequests
                         join o1 in _budgetRepository.GetAll() on o.BudgetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _budgetUseRepository.GetAll() on o.BudgetUseId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         select new GetBudgetRequestForViewDto()
                         {
                             BudgetRequest = ObjectMapper.Map<BudgetRequestDto>(o),
                             BudgetDescription = s1 == null ? "" : s1.Description.ToString(),
                             BudgetUseDescription = s2 == null ? "" : s2.Description.ToString()
                         })
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetDescriptionFilter), e => e.BudgetDescription.ToLower() == input.BudgetDescriptionFilter.ToLower().Trim())
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetUseDescriptionFilter), e => e.BudgetUseDescription.ToLower() == input.BudgetUseDescriptionFilter.ToLower().Trim());

            var budgetRequestListDtos = await query.ToListAsync();

            return _budgetRequestsExcelExporter.ExportToFile(budgetRequestListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses)]
        public async Task<PagedResultDto<BudgetRequestBudgetLookupTableDto>> GetAllBudgetForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _budgetRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Description.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var budgetList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<BudgetRequestBudgetLookupTableDto>();
            foreach (var budget in budgetList)
            {
                lookupTableDtoList.Add(new BudgetRequestBudgetLookupTableDto
                {
                    Id = budget.Id,
                    DisplayName = budget.Description?.ToString()
                });
            }

            return new PagedResultDto<BudgetRequestBudgetLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses)]
        public async Task<PagedResultDto<BudgetRequestBudgetUseLookupTableDto>> GetAllBudgetUseForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _budgetUseRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Description.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var budgetUseList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<BudgetRequestBudgetUseLookupTableDto>();
            foreach (var budgetUse in budgetUseList)
            {
                lookupTableDtoList.Add(new BudgetRequestBudgetUseLookupTableDto
                {
                    Id = budgetUse.Id,
                    DisplayName = budgetUse.Description?.ToString()
                });
            }

            return new PagedResultDto<BudgetRequestBudgetUseLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }
    }
}