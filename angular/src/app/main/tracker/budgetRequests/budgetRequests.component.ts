import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { BudgetRequestsServiceProxy, BudgetRequestDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditBudgetRequestModalComponent } from './create-or-edit-budgetRequest-modal.component';
import { ViewBudgetRequestModalComponent } from './view-budgetRequest-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './budgetRequests.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class BudgetRequestsComponent extends AppComponentBase {

    @ViewChild('createOrEditBudgetRequestModal') createOrEditBudgetRequestModal: CreateOrEditBudgetRequestModalComponent;
    @ViewChild('viewBudgetRequestModalComponent') viewBudgetRequestModal: ViewBudgetRequestModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
        budgetDescriptionFilter = '';
        budgetUseDescriptionFilter = '';




    constructor(
        injector: Injector,
        private _budgetRequestsServiceProxy: BudgetRequestsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getBudgetRequests(event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        this._budgetRequestsServiceProxy.getAll(
            this.filterText,
            this.budgetDescriptionFilter,
            this.budgetUseDescriptionFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createBudgetRequest(): void {
        this.createOrEditBudgetRequestModal.show();
    }

    deleteBudgetRequest(budgetRequest: BudgetRequestDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._budgetRequestsServiceProxy.delete(budgetRequest.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._budgetRequestsServiceProxy.getBudgetRequestsToExcel(
        this.filterText,
            this.budgetDescriptionFilter,
            this.budgetUseDescriptionFilter,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
