﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Authorization.Users.Dto;
using System.Threading.Tasks;

namespace Base2.Authorization.Users
{
    public interface IUserLoginAppService : IApplicationService
    {
        Task<ListResultDto<UserLoginAttemptDto>> GetRecentUserLoginAttempts();
    }
}