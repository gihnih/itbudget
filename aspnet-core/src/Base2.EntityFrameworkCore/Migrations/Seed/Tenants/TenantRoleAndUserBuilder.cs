﻿using Abp;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Abp.Notifications;
using Base2.Authorization;
using Base2.Authorization.Roles;
using Base2.Authorization.Users;
using Base2.EntityFrameworkCore;
using Base2.Notifications;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Linq;

namespace Base2.Migrations.Seed.Tenants
{
    public class TenantRoleAndUserBuilder
    {
        private readonly Base2DbContext _context;
        private readonly int _tenantId;

        public TenantRoleAndUserBuilder(Base2DbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            //Admin role

            var adminRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Admin);
            if (adminRole == null)
            {
                adminRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Admin, StaticRoleNames.Tenants.Admin) { IsStatic = true }).Entity;
                _context.SaveChanges();

                //Grant all permissions to admin role
                var permissions = PermissionFinder
                    .GetAllPermissions(new AppAuthorizationProvider(false))
                    .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Tenant))
                    .ToList();

                foreach (var permission in permissions)
                {
                    _context.Permissions.Add(
                        new RolePermissionSetting
                        {
                            TenantId = _tenantId,
                            Name = permission.Name,
                            IsGranted = true,
                            RoleId = adminRole.Id
                        });
                }

                _context.SaveChanges();
            }

            //User role

            var userRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.User);
            if (userRole == null)
            {
                userRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.User, StaticRoleNames.Tenants.User) { IsStatic = true, IsDefault = true }).Entity;
                _context.SaveChanges();

                //PermissionGrantInfo permission to user role
                string[] permissionRole =
                {
                    "Pages.Dashboards",
                    "Pages.Dashboard.MyDepartment",
                    "Pages.PendingActivity",
                    "Pages.MyTask.MyTask",
                    "Pages.MyBudgets",
                    "Pages.MyBudgets.Cancel",
                    "Pages.MyBudgets.View",
                    "Pages.Mybudgets.Unbudgeted",
                    "Pages.BudgetUses",
                    "Pages.BudgetUses.Create",
                    "Pages.BudgetUses.Edit",
                    "Pages.BudgetUses.Delete"
                };
                foreach (string value in permissionRole)
                {
                    _context.Permissions.Add(
                       new RolePermissionSetting
                       {
                           TenantId = _tenantId,
                           Name = value,
                           IsGranted = true,
                           RoleId = userRole.Id
                       });
                }
                _context.SaveChanges();
            }

            // Custodian role
            var custodianRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Custodian);
            if (custodianRole == null)
            {
                custodianRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Custodian, StaticRoleNames.Tenants.Custodian) { IsStatic = true }).Entity;
                _context.SaveChanges();

                //PermissionGrantInfo permission to custodian role
                string[] permissionRole = {
                    "Pages",
                    "Pages.Dashboards",
                    "Pages.Dashboard.It",
                    "Pages.PendingActivity",
                    "Pages.MyTask.ItCustodian",
                    "Pages.Budgets",
                    "Pages.Budgets.Create",
                    "Pages.Budgets.Edit",
                    "Pages.Budgets.Delete",
                    "Pages.Administration.GLCodes",
                    "Pages.Administration.GLCodes.Create",
                    "Pages.Administration.GLCodes.Edit",
                    "Pages.Administration.GLCodes.Delete",
                    "Pages.Administration.Roles",
                    "Pages.Administration.Roles.Create",
                    "Pages.Administration.Roles.Edit",
                    "Pages.Administration.Roles.Delete",
                    "Pages.Administration.Users",
                    "Pages.Administration.Users.Create",
                    "Pages.Administration.Users.Edit",
                    "Pages.Administration.Users.Delete",
                    "Pages.Administration.Users.ChangePermissions",
                    "Pages.Administration.Users.Impersonation",
                    "Pages.Administration.Languages",
                    "Pages.Administration.Languages.Create",
                    "Pages.Administration.Languages.Edit",
                    "Pages.Administration.Languages.Delete",
                    "Pages.Administration.Languages.ChangeTexts",
                    "Pages.Administration.OrganizationUnits",
                    "Pages.Administration.OrganizationUnits.ManageOrganizationTree",
                    "Pages.Administration.OrganizationUnits.ManageMembers",
                };

                foreach (string value in permissionRole)
                {
                    _context.Permissions.Add(
                       new RolePermissionSetting
                       {
                           TenantId = _tenantId,
                           Name = value,
                           IsGranted = true,
                           RoleId = custodianRole.Id
                       });
                }
                _context.SaveChanges();
            }

            //admin user

            var adminUser = _context.Users.IgnoreQueryFilters().FirstOrDefault(u => u.TenantId == _tenantId && u.UserName == AbpUserBase.AdminUserName);
            if (adminUser == null)
            {
                adminUser = User.CreateTenantAdminUser(_tenantId, "admin@defaulttenant.com");
                adminUser.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>(new PasswordHasherOptions())).HashPassword(adminUser, "123qwe");
                adminUser.IsEmailConfirmed = true;
                adminUser.ShouldChangePasswordOnNextLogin = true;
                adminUser.IsActive = true;

                _context.Users.Add(adminUser);
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(_tenantId, adminUser.Id, adminRole.Id));
                _context.SaveChanges();

                //User account of admin user
                if (_tenantId == 1)
                {
                    _context.UserAccounts.Add(new UserAccount
                    {
                        TenantId = _tenantId,
                        UserId = adminUser.Id,
                        UserName = AbpUserBase.AdminUserName,
                        EmailAddress = adminUser.EmailAddress
                    });
                    _context.SaveChanges();
                }

                //Notification subscription
                _context.NotificationSubscriptions.Add(new NotificationSubscriptionInfo(SequentialGuidGenerator.Instance.Create(), _tenantId, adminUser.Id, AppNotificationNames.NewUserRegistered));
                _context.SaveChanges();
            }
        }
    }
}