namespace Base2.Tracker.Dtos
{
    public class GetBudgetYearForViewDto
    {
        public BudgetYearDto BudgetYear { get; set; }
    }
}