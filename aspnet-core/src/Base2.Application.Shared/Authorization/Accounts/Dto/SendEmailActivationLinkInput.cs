﻿using System.ComponentModel.DataAnnotations;

namespace Base2.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}