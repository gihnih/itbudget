namespace Base2.Tracker.Dtos
{
    public class GetBudgetApproverForViewDto
    {
        public BudgetApproverDto BudgetApprover { get; set; }

        public BudgetDto Budget { get; set; }

        public BudgetUseDto BudgetUse { get; set; }

        public string requesterName { get; set; }
        public long requesterId { get; set; }
        public string approverName { get; set; }
        public long approverId { get; set; }
    }
}