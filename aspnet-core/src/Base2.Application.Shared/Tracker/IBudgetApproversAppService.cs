using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IBudgetApproversAppService : IApplicationService
    {
        Task<PagedResultDto<GetBudgetApproverForViewDto>> GetAll(GetAllBudgetApproversInput input);

        Task<GetBudgetApproverForViewDto> GetBudgetApproverForView(int id);

        Task<GetBudgetApproverForEditOutput> GetBudgetApproverForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBudgetApproverDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetBudgetApproversToExcel(GetAllBudgetApproversForExcelInput input);
    }
}