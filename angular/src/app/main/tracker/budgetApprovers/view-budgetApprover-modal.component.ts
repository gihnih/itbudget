import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetBudgetApproverForViewDto, BudgetApproverDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewBudgetApproverModal',
    templateUrl: './view-budgetApprover-modal.component.html'
})
export class ViewBudgetApproverModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetBudgetApproverForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetBudgetApproverForViewDto();
        this.item.budgetApprover = new BudgetApproverDto();
    }

    show(item: GetBudgetApproverForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
