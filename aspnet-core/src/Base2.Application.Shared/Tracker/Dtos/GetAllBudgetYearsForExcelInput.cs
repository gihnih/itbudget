namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetYearsForExcelInput
    {
        public string Filter { get; set; }

        public int? MaxYearFilter { get; set; }
        public int? MinYearFilter { get; set; }
    }
}