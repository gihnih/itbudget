using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public class BudgetUsesExcelExporter : EpPlusExcelExporterBase, IBudgetUsesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BudgetUsesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) :
    base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetBudgetUseForViewDto> budgetUses)
        {
            return CreateExcelPackage(
                "BudgetUses.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("BudgetUses"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Description"),
                        L("RequestAmount"),
                        L("PaperStatus"),
                        L("RequestStatus"),
                        L("ActualAmount"));

                    AddObjects(
                        sheet, 2, budgetUses,
                        _ => _.Description,
                        _ => _.RequestAmount,
                        _ => _.PaperStatus,
                        _ => _.RequestStatus,
                        _ => _.ActualAmount
                        );
                });
        }
    }
}