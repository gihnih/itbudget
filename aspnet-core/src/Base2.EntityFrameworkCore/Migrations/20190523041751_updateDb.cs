﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class updateDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Budgets_Departments_DepartmentId",
                table: "Budgets");

            migrationBuilder.DropIndex(
                name: "IX_Budgets_DepartmentId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Budgets");

            migrationBuilder.AddColumn<long>(
                name: "OrganizationUnitId",
                table: "Budgets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_OrganizationUnitId",
                table: "Budgets",
                column: "OrganizationUnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Budgets_AbpOrganizationUnits_OrganizationUnitId",
                table: "Budgets",
                column: "OrganizationUnitId",
                principalTable: "AbpOrganizationUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Budgets_AbpOrganizationUnits_OrganizationUnitId",
                table: "Budgets");

            migrationBuilder.DropIndex(
                name: "IX_Budgets_OrganizationUnitId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "OrganizationUnitId",
                table: "Budgets");

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Budgets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_DepartmentId",
                table: "Budgets",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Budgets_Departments_DepartmentId",
                table: "Budgets",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}