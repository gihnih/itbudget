namespace Base2.Tracker.Dtos
{
    public class BudgetRequestBudgetUseLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}