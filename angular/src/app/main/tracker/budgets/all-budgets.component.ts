import {Component, Injector, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
    BudgetsServiceProxy,
    BudgetDto,
    OrganizationUnitServiceProxy,
    UserServiceProxy
} from '@shared/service-proxies/service-proxies';
import {NotifyService} from '@abp/notify/notify.service';
import {AppComponentBase} from '@shared/common/app-component-base';
import {TokenAuthServiceProxy} from '@shared/service-proxies/service-proxies';
import {CreateOrEditBudgetModalComponent} from './create-or-edit-budget-modal.component';
import {ViewBudgetModalComponent} from './view-budget-modal.component';
import {appModuleAnimation} from '@shared/animations/routerTransition';
import {DataTable} from 'primeng/components/datatable/datatable';
import {Paginator} from 'primeng/components/paginator/paginator';
import {LazyLoadEvent} from 'primeng/components/common/lazyloadevent';
import {FileDownloadService} from '@shared/utils/file-download.service';

@Component({
    templateUrl: './all-budgets.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class AllBudgetsComponent extends AppComponentBase {
    @ViewChild('createOrEditBudgetModal') createOrEditBudgetModal: CreateOrEditBudgetModalComponent;
    @ViewChild('viewBudgetModalComponent') viewBudgetModal: ViewBudgetModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    descriptionFilter = '';
    budgetCategory = '';
    maxBudgetAmountFilter: number;
    maxBudgetAmountFilterEmpty: number;
    minBudgetAmountFilter: number;
    minBudgetAmountFilterEmpty: number;
    maxUtilizedBudgetFilter: number;
    maxUtilizedBudgetFilterEmpty: number;
    minUtilizedBudgetFilter: number;
    minUtilizedBudgetFilterEmpty: number;
    maxCurrentAmountFilter: number;
    maxCurrentAmountFilterEmpty: number;
    minCurrentAmountFilter: number;
    minCurrentAmountFilterEmpty: number;
    remarkFilter = '';
    categoryFilter = '';
    budgetTypeNameFilter = '';
    glCodeCodeFilter = '';
    descriptionTypeNameFilter = '';
    budgetYearYearFilter = '';
    organizationUnitDisplayNameFilter = '';
    codeOrganizationUnit: any[] = [];
    maxResult;

    shownLoginName = '';
    tenancyName = '';
    userName = '';
    budget = [];

    userId = this.appSession.userId;
    userRole: string = null;

    constructor(
        injector: Injector,
        private _budgetsServiceProxy: BudgetsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userService: UserServiceProxy
    ) {
        super(injector);
    }

    getBudgets(event?: LazyLoadEvent) {
        this._userService.getUserForEdit(this.appSession.userId).subscribe(result => {
            // check user admin or custodian or staff
            for (let item of result.roles) {
                if (item.roleDisplayName.toLowerCase() === 'admin' || item.roleDisplayName.toLowerCase() === 'custodian') {
                    if (item.isAssigned) {
                        this.userRole = item.roleDisplayName;
                    }
                } else if (item.roleDisplayName.toLowerCase() === 'staff' && this.userRole === null) {
                    console.log('staff');

                    // get user organization unit
                    for (let item of result.allOrganizationUnits) {
                        for (let d of result.memberedOrganizationUnits) {
                            if (item.code.indexOf(d) === 0) {
                                this.codeOrganizationUnit.push(item.code);
                            }
                        }
                    }
                    console.log('current user code Organization unit');
                    console.log(this.codeOrganizationUnit);
                } else {
                    if (this.userRole === null) {
                        this.codeOrganizationUnit.push('00000.00000');
                    }
                }
            }

            console.log('org unit:');
            console.log(this.codeOrganizationUnit);


            if (this.primengDatatableHelper.shouldResetPaging(event)) {
                this.paginator.changePage(0);
                return;
            }
            this.primengDatatableHelper.showLoadingIndicator();

            // Get Budgets start
            this._budgetsServiceProxy.getAll(
                this.filterText,
                this.budgetCategory,
                this.budgetTypeNameFilter,
                this.glCodeCodeFilter,
                this.descriptionTypeNameFilter,
                this.budgetYearYearFilter,
                this.organizationUnitDisplayNameFilter,
                this.codeOrganizationUnit,
                this.remarkFilter,
                this.primengDatatableHelper.getSorting(this.dataTable),
                this.primengDatatableHelper.getSkipCount(this.paginator, event),
                this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
            ).subscribe(result => {
                this.primengDatatableHelper.totalRecordsCount = result.totalCount;
                this.primengDatatableHelper.records = result.items;
                this.primengDatatableHelper.hideLoadingIndicator();
                this.organizationUnitDisplayNameFilter = '';
            });
        });


    }


    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createBudget(): void {
        this.createOrEditBudgetModal.show();
    }

    deleteBudget(budget: BudgetDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._budgetsServiceProxy.delete(budget.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._budgetsServiceProxy.getBudgetsToExcel(
            this.filterText,
            this.descriptionFilter,
            this.remarkFilter,
            this.categoryFilter,
            this.budgetTypeNameFilter,
            this.glCodeCodeFilter,
            this.descriptionTypeNameFilter,
            this.budgetYearYearFilter,
            this.organizationUnitDisplayNameFilter,
            this.budgetCategory
        )
            .subscribe(result => {
                console.log(result);
                // this._fileDownloadService.downloadTempFile(result);
            });
    }

    getCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        this.tenancyName = this.appSession.tenancyName;
        this.userName = this.appSession.user.userName;
    }
}
