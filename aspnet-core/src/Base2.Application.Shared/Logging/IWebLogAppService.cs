﻿using Abp.Application.Services;
using Base2.Dto;
using Base2.Logging.Dto;

namespace Base2.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}