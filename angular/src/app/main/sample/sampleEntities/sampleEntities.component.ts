﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SampleEntitiesServiceProxy, SampleEntityDto  } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditSampleEntityModalComponent } from './create-or-edit-sampleEntity-modal.component';
import { ViewSampleEntityModalComponent } from './view-sampleEntity-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './sampleEntities.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class SampleEntitiesComponent extends AppComponentBase {

    @ViewChild('createOrEditSampleEntityModal') createOrEditSampleEntityModal: CreateOrEditSampleEntityModalComponent;
    @ViewChild('viewSampleEntityModalComponent') viewSampleEntityModal: ViewSampleEntityModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    titleFilter = '';
    descriptionFilter = '';
    fieldRequiredFilter = '';
    fieldFilter = '';
    maxFieldDecimalNullFilter : number;
		maxFieldDecimalNullFilterEmpty : number;
		minFieldDecimalNullFilter : number;
		minFieldDecimalNullFilterEmpty : number;
    maxFieldDoubleNullFilter : number;
		maxFieldDoubleNullFilterEmpty : number;
		minFieldDoubleNullFilter : number;
		minFieldDoubleNullFilterEmpty : number;




    constructor(
        injector: Injector,
        private _sampleEntitiesServiceProxy: SampleEntitiesServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getSampleEntities(event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        this._sampleEntitiesServiceProxy.getAll(
            this.filterText,
            this.titleFilter,
            this.descriptionFilter,
            this.fieldRequiredFilter,
            this.fieldFilter,
            this.maxFieldDecimalNullFilter == null ? this.maxFieldDecimalNullFilterEmpty: this.maxFieldDecimalNullFilter,
            this.minFieldDecimalNullFilter == null ? this.minFieldDecimalNullFilterEmpty: this.minFieldDecimalNullFilter,
            this.maxFieldDoubleNullFilter == null ? this.maxFieldDoubleNullFilterEmpty: this.maxFieldDoubleNullFilter,
            this.minFieldDoubleNullFilter == null ? this.minFieldDoubleNullFilterEmpty: this.minFieldDoubleNullFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createSampleEntity(): void {
        this.createOrEditSampleEntityModal.show();
    }

    deleteSampleEntity(sampleEntity: SampleEntityDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._sampleEntitiesServiceProxy.delete(sampleEntity.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._sampleEntitiesServiceProxy.getSampleEntitiesToExcel(
        this.filterText,
            this.titleFilter,
            this.descriptionFilter,
            this.fieldRequiredFilter,
            this.fieldFilter,
            this.maxFieldDecimalNullFilter == null ? this.maxFieldDecimalNullFilterEmpty: this.maxFieldDecimalNullFilter,
            this.minFieldDecimalNullFilter == null ? this.minFieldDecimalNullFilterEmpty: this.minFieldDecimalNullFilter,
            this.maxFieldDoubleNullFilter == null ? this.maxFieldDoubleNullFilterEmpty: this.maxFieldDoubleNullFilter,
            this.minFieldDoubleNullFilter == null ? this.minFieldDoubleNullFilterEmpty: this.minFieldDoubleNullFilter,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         });
    }
}
