﻿namespace Base2.Chat
{
    public enum ChatSide
    {
        Sender = 1,

        Receiver = 2
    }
}