﻿using Abp.Application.Services;
using Base2.Install.Dto;
using System.Threading.Tasks;

namespace Base2.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}