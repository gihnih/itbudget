import {Component, Injector, OnInit, ViewChild} from '@angular/core';
import {appModuleAnimation} from '@shared/animations/routerTransition';
import {CreateOrEditBudgetModalComponent} from '@app/main/tracker/budgets/create-or-edit-budget-modal.component';
import {ViewBudgetModalComponent} from '@app/main/tracker/budgets/view-budget-modal.component';
import {DataTable} from '@node_modules/primeng/components/datatable/datatable';
import {Paginator} from '@node_modules/primeng/components/paginator/paginator';
import {AppComponentBase} from '@shared/common/app-component-base';
import {LazyLoadEvent} from '@node_modules/primeng/components/common/lazyloadevent';
import {
    BudgetDto, BudgetGLCodeLookupTableDto,
    BudgetsServiceProxy, GetGLCodeForViewDto, GLCodesServiceProxy,
    OrganizationUnitServiceProxy,
    TokenAuthServiceProxy, UserServiceProxy
} from '@shared/service-proxies/service-proxies';
import {NotifyService} from '@abp/notify/notify.service';
import {ActivatedRoute} from '@angular/router';
import {FileDownloadService} from '@shared/utils/file-download.service';
import {Globals} from '@app/global';

@Component({
    selector: 'app-my-budgets',
    templateUrl: './my-budgets.component.html',
    animations: [appModuleAnimation()]
})
export class MyBudgetsComponent extends AppComponentBase {
    @ViewChild('createOrEditBudgetModal') createOrEditBudgetModal: CreateOrEditBudgetModalComponent;
    @ViewChild('viewBudgetModalComponent') viewBudgetModal: ViewBudgetModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    // filter
    filterText = '';
    budgetCategory = 'Budgeted';
    budgetTypeFilter = '';
    descriptionTypeNameFilter = '';
    glCodeFilter = '';
    descriptionTypeFilter = '';
    organizationUnitFilter = '';


    advancedFiltersAreShown = false;
    descriptionFilter = '';
    remarkFilter = '';
    categoryFilter = 'Budgeted';
    budgetYearFilter = '';
    organizationUnitDisplayNameFilter = '';
    organizationUnitId: any[];
    codeOrganizationUnit: any[] = [];

    budgetTypes = [];
    glCode: BudgetGLCodeLookupTableDto[];
    descriptionType = [];

    shownLoginName = '';
    tenancyName = '';
    userName = '';
    budget = [];

    userId = this.appSession.userId;

    budgetType = this._global.budgetType;


    constructor(
        injector: Injector,
        private _budgetsServiceProxy: BudgetsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _userOrganizationUnit: OrganizationUnitServiceProxy,
        private _userService: UserServiceProxy,
        private _global: Globals,
    ) {
        super(injector);
        this.initialData();
    }

    initialData() {
        this._budgetsServiceProxy.getBudgetType().subscribe(result => {
            this.budgetTypes = result;
            console.log(this.budgetTypes);
        });

        this._budgetsServiceProxy.getAllGLCode()
            .subscribe(result => {
                this.glCode = result.items;
            });

        this._budgetsServiceProxy.getDescriptionType().subscribe(result => this.descriptionType = result);
    }
    budgetCategoryButton(event) {
        this.budgetCategory = event.target.innerText;
        this.getBudgets();
    }


    getBudgets(event?: LazyLoadEvent) {
        // get Budgets
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengDatatableHelper.showLoadingIndicator();


        this._budgetsServiceProxy.getAllMyBudgets(
            this.filterText,
            this.budgetCategory,
            this.budgetTypeFilter,
            this.glCodeFilter,
            this.descriptionTypeNameFilter,
            this.budgetYearFilter,
            this.organizationUnitDisplayNameFilter,
            this.codeOrganizationUnit,
            this.remarkFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.organizationUnitDisplayNameFilter = '';
            console.log(result);
            console.log(this.budgetCategory);
        });
    }

    //
    // getOrganizationUnit() {
    //     this._userOrganizationUnit.getOrganizationUnitByUserId(this.appSession.userId).subscribe(result => {
    //         this.organizationUnitId = result.items;
    //     });
    //
    // }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createUnbudget(): void {
        this.createOrEditBudgetModal.budgetCategory = this.budgetCategory;
        this.createOrEditBudgetModal.show();
    }

    deleteBudget(budgetId: number): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._budgetsServiceProxy.deleteUnbudget(budgetId)
                        .subscribe(() => {

                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }


    exportToExcel(): void {
        this._budgetsServiceProxy.getMyBudgetsToExcel(
            this.filterText,
            this.descriptionFilter,
            this.remarkFilter,
            this.categoryFilter,
            this.budgetTypeFilter,
            this.glCodeFilter,
            this.descriptionTypeNameFilter,
            this.budgetYearFilter,
            this.organizationUnitDisplayNameFilter,
            this.budgetCategory
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    getCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        this.tenancyName = this.appSession.tenancyName;
        this.userName = this.appSession.user.userName;
    }


}
