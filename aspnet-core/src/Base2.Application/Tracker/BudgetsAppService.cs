using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Organizations;
using Base2.Authorization;
using Base2.Authorization.Users;
using Base2.Authorization.Users.Dto;
using Base2.Dto;
using Base2.Organizations.Dto;
using Base2.Tracker.Dtos;
using Base2.Tracker.Exporting;
using Base2.Url;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public class BudgetsAppService : Base2AppServiceBase, IBudgetsAppService
    {
        public IAppUrlService AppUrlService { get; set; }

        private readonly IRepository<Budget> _budgetRepository;
        private readonly IBudgetsExcelExporter _budgetsExcelExporter;
        private readonly IRepository<GLCode, int> _glCodeRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly UserManager _userManager;

        private readonly IUserEmailer _userEmailer;
        private readonly IEmailSender _emailSender;

        public BudgetsAppService(IRepository<Budget> budgetRepository, IBudgetsExcelExporter budgetsExcelExporter, IRepository<GLCode, int> glCodeRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository, IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<UserRole, long> userRoleRepository, UserManager userManager,
            IEmailSender emailSender, IUserEmailer userEmailer)
        {
            _budgetRepository = budgetRepository;
            _budgetsExcelExporter = budgetsExcelExporter;
            _glCodeRepository = glCodeRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userRoleRepository = userRoleRepository;
            _userManager = userManager;
            _userEmailer = userEmailer;
        }

        [AbpAuthorize(AppPermissions.Pages_Budgets)]
        public async Task<PagedResultDto<GetBudgetForViewDto>> GetAll(GetAllBudgetsInput input)
        {
            var queryBudgets = _budgetRepository
                 .GetAll()
                 .Include(ou => ou.OrganizationUnit)
                 .Include(gl => gl.GLCode)
                 .Include(bu => bu.BudgetRequests)
                     .ThenInclude(bu => bu.BudgetUse)
                         .OrderBy("id desc")
                 .Where(b => b.BudgetCategory == (BudgetCategory)Enum.Parse(typeof(BudgetCategory), input.BudgetCategoryFilter))
                 .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Description.ToLower().Contains(input.Filter.ToLower()))
                 .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetTypeFilter), e => e.BudgetType.ToString() == input.BudgetTypeFilter)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionTypeFilter), e => e.DescriptionType.ToString() == input.DescriptionTypeFilter)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.GLCodeFilter), e => e.GLCodeId.ToString() == input.GLCodeFilter)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetYearFilter), e => e.BudgetYear.ToString() == input.BudgetYearFilter)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.RemarkFilter), e => e.Remark.ToString().ToLower().Contains(input.RemarkFilter.ToLower()))
                 .WhereIf(!string.IsNullOrWhiteSpace(input.OrganizationUnitFilter), e => e.OrganizationUnit.DisplayName.ToLower().Contains(input.OrganizationUnitFilter.ToLower()));

            var totalCount = queryBudgets.Count();
            var budgets = await queryBudgets
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .ToListAsync();
            var budgetsDto = ObjectMapper.Map<List<GetBudgetForViewDto>>(budgets);

            return new PagedResultDto<GetBudgetForViewDto>(
                totalCount,
                budgetsDto
            );
        }

        [AbpAuthorize(AppPermissions.Pages_MyBudgets)]
        public async Task<PagedResultDto<GetBudgetForViewDto>> GetAllMyBudgets(GetAllBudgetsInput input)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
            var organizationUnits = await _userManager.GetOrganizationUnitsAsync(user);
            var organizationUnitCodes = organizationUnits.Select(ou => ou.Code);
            if (input.BudgetCategoryFilter == null)
            {
                input.BudgetCategoryFilter = BudgetCategory.Budgeted.ToString();
            }
            var query = _budgetRepository
                .GetAll()
                .Include(gl => gl.GLCode)
                .Include(bu => bu.BudgetRequests)
                    .ThenInclude(bu => bu.BudgetUse)
                .Include(ou => ou.OrganizationUnit)
                .Where(b => organizationUnitCodes.Contains(b.OrganizationUnit.Code))
                .Where(b => b.BudgetCategory == (BudgetCategory)Enum.Parse(typeof(BudgetCategory), input.BudgetCategoryFilter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Description.ToLower().Contains(input.Filter.ToLower()))
                .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetTypeFilter), e => e.BudgetType.ToString() == input.BudgetTypeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionTypeFilter), e => e.DescriptionType.ToString() == input.DescriptionTypeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.GLCodeFilter), e => e.GLCodeId.ToString() == input.GLCodeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.OrganizationUnitFilter), e => e.OrganizationUnit.DisplayName.ToLower().Contains(input.OrganizationUnitFilter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetYearFilter), e => e.BudgetYear.ToString() == input.BudgetYearFilter);

            var total = query.Count();
            var budget = await query
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .ToListAsync();

            var budgetDto = budget.MapTo<List<GetBudgetForViewDto>>();
            return new PagedResultDto<GetBudgetForViewDto>(
                total,
                budgetDto
            );
        }

        public async Task<PagedResultDto<GetAllBudgetForLookup>> GetAllOtherBudgets(GetAllBudgetsInput input)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
            var organizationUnits = await _userManager.GetOrganizationUnitsAsync(user);
            var organizationUnitCodes = organizationUnits.Select(ou => ou.Code);

            var query = _budgetRepository
                .GetAll()
                .Include(gl => gl.GLCode)
                .Include(bu => bu.BudgetRequests)
                    .ThenInclude(bu => bu.BudgetUse)
                .Include(ou => ou.OrganizationUnit)
                .Where(b => !organizationUnitCodes.Contains(b.OrganizationUnit.Code))
                .Where(b => b.BudgetCategory == BudgetCategory.Budgeted)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter) || e.Remark.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Description.ToLower() == input.Filter.ToLower().Trim())
                .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetTypeFilter), e => e.BudgetType.ToString() == input.BudgetTypeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionTypeFilter), e => e.DescriptionType.ToString() == input.DescriptionTypeFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.OrganizationUnitFilter), e => e.OrganizationUnitId.ToString() == input.OrganizationUnitFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetYearFilter), e => e.BudgetYear.ToString() == input.BudgetYearFilter);

            var total = query.Count();
            var budget = await query
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .ToListAsync();

            var budgetDto = budget.MapTo<List<GetAllBudgetForLookup>>();

            return new PagedResultDto<GetAllBudgetForLookup>(
                total,
                budgetDto
            );
        }

        public async Task<GetBudgetForViewDto> GetBudgetForView(int id)
        {
            var budget = await _budgetRepository
                .GetAll()
                .Include(gl => gl.GLCode)
                .Include(ou => ou.OrganizationUnit)
                .Include(br => br.BudgetRequests)
                    .ThenInclude(bu => bu.BudgetUse)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();

            var getBudgetForViewDto = budget.MapTo<GetBudgetForViewDto>();
            foreach(var budgetRequest in getBudgetForViewDto.BudgetRequests)
            {
                var userdetail = _userManager.GetUserByIdAsync(budgetRequest.CreatorUserId).Result;
                budgetRequest.User = userdetail.MapTo<UserListDto>();


                if (budgetRequest.CustodianApprovalId != null)
                {
                    var custodianDetail = _userManager.GetUserByIdAsync(budgetRequest.CustodianApprovalId.Value).Result;
                    budgetRequest.CustodianApproval = custodianDetail.MapTo<UserListDto>();
                }
                    
                if(budgetRequest.OwnerApprovalId != null) { 
                    var ownerDetail = _userManager.GetUserByIdAsync(budgetRequest.OwnerApprovalId.Value).Result;
                    budgetRequest.OwnerApproval = ownerDetail.MapTo<UserListDto>();
                }
            }

            return getBudgetForViewDto;
        }

        [AbpAuthorize(AppPermissions.Pages_Budgets_Edit)]
        public async Task<GetBudgetForEditOutput> GetBudgetForEdit(EntityDto input)
        {
            var budget = await _budgetRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetBudgetForEditOutput { Budget = ObjectMapper.Map<CreateOrEditBudgetDto>(budget) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Budgets_Edit)]
        public async Task CreateOrEdit(CreateOrEditBudgetDto input)
        {
            if (input.Id == null)
            {
                try
                {
                    await Create(input);
                }
                catch (Exception e)
                {
                    var error = e;
                }
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Budgets_Create)]
        private async Task Create(CreateOrEditBudgetDto input)
        {
            input.EstimatedBalance = input.BudgetAmount;
            if (input.BudgetCategory == BudgetCategory.Unbudgeted.ToString())
            {
                input.GLCodeId = 1;
            }
            var budget = ObjectMapper.Map<Budget>(input);
            await _budgetRepository.InsertAsync(budget);

            //EmailNotification
            //GetAllUserInOrganizationUnit
            var userIdsInOrganizationUnit = _userOrganizationUnitRepository.GetAll()
               .Where(uou => uou.OrganizationUnitId == input.OrganizationUnitId)
               .Select(uou => uou.UserId);

            var query = (from uou in _userOrganizationUnitRepository.GetAll()
                         join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
                         join user in UserManager.Users on uou.UserId equals user.Id
                         where uou.OrganizationUnitId == input.OrganizationUnitId
                         select new { user }).ToList();

            var orgUnit = await _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationUnitId).FirstOrDefaultAsync();
            budget.OrganizationUnit = orgUnit;
        }

        [AbpAuthorize(AppPermissions.Pages_Budgets_Edit)]
        private async Task Update(CreateOrEditBudgetDto input)
        {
            var budget = await _budgetRepository.FirstOrDefaultAsync((int)input.Id);
            var oldEB = input.BudgetAmount - budget.BudgetAmount;
            input.EstimatedBalance = budget.EstimatedBalance + oldEB;
            ObjectMapper.Map(input, budget);


        }

        [AbpAuthorize(AppPermissions.Pages_Budgets_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _budgetRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetBudgetsToExcel(GetAllBudgetsForExcelInput input)
        {
            var query = await _budgetRepository.GetAll()
                .Include(gl => gl.GLCode)
                .Include(e => e.BudgetRequests)
                     .ThenInclude(bu => bu.BudgetUse)
                .Include(e => e.BudgetRequests)
                .Include(ou => ou.OrganizationUnit)
                .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetCategory), e => e.BudgetCategory.ToString() == input.BudgetCategory)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter) || e.Remark.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
                .WhereIf(!string.IsNullOrWhiteSpace(input.RemarkFilter), e => e.Remark.ToLower() == input.RemarkFilter.ToLower().Trim())
                .ToListAsync();

            var budgetsDto = query.MapTo<List<GetBudgetForViewDto>>();

            if (input.BudgetCategory == BudgetCategory.Budgeted.ToString())
            {
                return _budgetsExcelExporter.ExportToFile(budgetsDto);
            }
            else
                return _budgetsExcelExporter.ExportUnbudgetedToFile(budgetsDto);
        }

        public async Task<PagedResultDto<BudgetGLCodeLookupTableDto>> GetAllGLCodeForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _glCodeRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.Code.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var glCodeList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<BudgetGLCodeLookupTableDto>();
            foreach (var glCode in glCodeList)
            {
                lookupTableDtoList.Add(new BudgetGLCodeLookupTableDto
                {
                    Id = glCode.Id,
                    DisplayName = glCode.Code?.ToString()
                });
            }

            return new PagedResultDto<BudgetGLCodeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        public async Task<PagedResultDto<BudgetGLCodeLookupTableDto>> GetAllGLCode()
        {
            var query = _glCodeRepository.GetAll();

            var totalCount = await query.CountAsync();

            var glCodeList = await query
                .ToListAsync();

            var lookupTableDtoList = new List<BudgetGLCodeLookupTableDto>();
            foreach (var glCode in glCodeList)
            {
                lookupTableDtoList.Add(new BudgetGLCodeLookupTableDto
                {
                    Id = glCode.Id,
                    DisplayName = glCode.Code?.ToString()
                });
            }

            return new PagedResultDto<BudgetGLCodeLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Budgets)]
        public async Task<PagedResultDto<BudgetOrganizationUnitLookupTableDto>> GetAllOrganizationUnitForLookupTable(GetAllForLookupTableInput input)
        {
            var query = _organizationUnitRepository.GetAll().WhereIf(
                   !string.IsNullOrWhiteSpace(input.Filter),
                  e => e.DisplayName.ToString().Contains(input.Filter)
               );

            var totalCount = await query.CountAsync();

            var organizationUnitList = await query
                .PageBy(input)
                .ToListAsync();

            var lookupTableDtoList = new List<BudgetOrganizationUnitLookupTableDto>();
            foreach (var organizationUnit in organizationUnitList)
            {
                lookupTableDtoList.Add(new BudgetOrganizationUnitLookupTableDto
                {
                    Id = organizationUnit.Id,
                    DisplayName = organizationUnit.DisplayName?.ToString()
                });
            }

            return new PagedResultDto<BudgetOrganizationUnitLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Budgets)]
        public async Task<PagedResultDto<BudgetOrganizationUnitLookupTableDto>> GetAllOrganizationUnit()
        {
            var query = _organizationUnitRepository.GetAll();

            var totalCount = await query.CountAsync();

            var organizationUnitList = await query
                .ToListAsync();

            var lookupTableDtoList = new List<BudgetOrganizationUnitLookupTableDto>();
            foreach (var organizationUnit in organizationUnitList)
            {
                lookupTableDtoList.Add(new BudgetOrganizationUnitLookupTableDto
                {
                    Id = organizationUnit.Id,
                    DisplayName = organizationUnit.DisplayName?.ToString()
                });
            }

            return new PagedResultDto<BudgetOrganizationUnitLookupTableDto>(
                totalCount,
                lookupTableDtoList
            );
        }

        [AbpAuthorize(AppPermissions.Pages_MyBudgets_Unbudgeted)]
        public async Task<PagedResultDto<GetBudgetForViewDto>> GetAllMyUnbudgeted(GetAllBudgetsInput input)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
            var organizationUnits = await _userManager.GetOrganizationUnitsAsync(user);
            var organizationUnitCodes = organizationUnits.Select(ou => ou.Code);

            var query = _budgetRepository
                .GetAll()
                .Include(ou => ou.OrganizationUnit)
                .Include(gl => gl.GLCode)
                .Include(bu => bu.BudgetRequests)
                    .ThenInclude(bu => bu.BudgetUse)
                .Where(b => organizationUnitCodes.Contains(b.OrganizationUnit.Code))
                .Where(b => b.BudgetCategory == (BudgetCategory)Enum.Parse(typeof(BudgetCategory), input.BudgetCategoryFilter));

            var ttlCount = query.Count();
            var unbudgets = await query
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .ToListAsync();

            var unbudgetsDto = unbudgets.MapTo<List<GetBudgetForViewDto>>();

            return new PagedResultDto<GetBudgetForViewDto>(
                ttlCount,
                unbudgetsDto);
        }

        [AbpAuthorize(AppPermissions.Pages_MyBudgets_Unbudgeted)]
        public async Task<GetBudgetForEditOutput> GetMyUnbudgetForEdit(EntityDto input)
        {
            var unbudget = await _budgetRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetBudgetForEditOutput
            {
                Budget = ObjectMapper.Map<CreateOrEditBudgetDto>(unbudget)
            };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_MyBudgets_Unbudgeted)]
        public async Task<GetBudgetForEditOutput> GetBudgetForEditUnBudget(EntityDto input)
        {
            var budget = await _budgetRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetBudgetForEditOutput { Budget = ObjectMapper.Map<CreateOrEditBudgetDto>(budget) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_MyBudgets_Unbudgeted)]
        public async Task CreateOrEditUnBudget(CreateOrEditBudgetDto input)
        {
            if (input.Id == null)
            {
                try
                {
                    await CreateUnbudget(input);
                }
                catch (Exception e)
                {
                    var error = e;
                }
            }
            else
            {
                await UpdateUnbudget(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_MyBudgets_Unbudgeted)]
        private async Task CreateUnbudget(CreateOrEditBudgetDto input)
        {
            input.EstimatedBalance = input.BudgetAmount;
            input.GLCodeId = 1;
            var unbudget = ObjectMapper.Map<Budget>(input);
            await _budgetRepository.InsertAsync(unbudget);

            ////EmailNotification
            ////GetAllUserInOrganizationUnit
            //var userIdsInOrganizationUnit = _userOrganizationUnitRepository.GetAll()
            //   .Where(uou => uou.OrganizationUnitId == input.OrganizationUnitId)
            //   .Select(uou => uou.UserId);

            //var query = (from uou in _userOrganizationUnitRepository.GetAll()
            //             join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
            //             join user in UserManager.Users on uou.UserId equals user.Id
            //             where uou.OrganizationUnitId == input.OrganizationUnitId
            //             select new { user }).ToList();

            //var orgUnit = await _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationUnitId).FirstOrDefaultAsync();
            //budget.OrganizationUnit = orgUnit;

            //foreach (var item in query)
            //{
            //    var user = ObjectMapper.Map<User>(item.user);
            //    await _userEmailer.SendBudgetedAddedAsync(
            //        user,
            //        budget);
            //}
        }

        [AbpAuthorize(AppPermissions.Pages_MyBudgets_Unbudgeted)]
        private async Task UpdateUnbudget(CreateOrEditBudgetDto input)
        {
            var budget = await _budgetRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, budget);
        }

        [AbpAuthorize(AppPermissions.Pages_MyBudgets_Unbudgeted)]
        public async Task DeleteUnbudget(EntityDto input)
        {
            await _budgetRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetBudgetsToExcelUnbudget(GetAllBudgetsForExcelInput input)
        {
            var query = await _budgetRepository.GetAll()
                .Include(ou => ou.OrganizationUnit)
                .Include(gl => gl.GLCode)
                .Include(bu => bu.BudgetRequests)
                    .ThenInclude(bu => bu.BudgetUse)
                .Include(e => e.BudgetRequests)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter) || e.Remark.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
                .WhereIf(!string.IsNullOrWhiteSpace(input.RemarkFilter), e => e.Remark.ToLower() == input.RemarkFilter.ToLower().Trim())
                .ToListAsync();

            var budgetsDto = query.MapTo<List<GetBudgetForViewDto>>();

            return _budgetsExcelExporter.ExportToFile(budgetsDto);
        }

        public async Task<FileDto> GetMyBudgetsToExcel(GetAllBudgetsForExcelInput input)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
            var organizationUnits = await _userManager.GetOrganizationUnitsAsync(user);
            var organizationUnitCodes = organizationUnits.Select(ou => ou.Code);

            var query = await _budgetRepository
                .GetAll()
                .Include(gl => gl.GLCode)
                .Include(bu => bu.BudgetRequests)
                    .ThenInclude(bu => bu.BudgetUse)
                .Include(ou => ou.OrganizationUnit)
                .Where(b => organizationUnitCodes.Contains(b.OrganizationUnit.Code))
                .Where(b => b.BudgetCategory == (BudgetCategory)Enum.Parse(typeof(BudgetCategory), input.BudgetCategory))
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter) || e.Remark.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower() == input.DescriptionFilter.ToLower().Trim())
                .WhereIf(!string.IsNullOrWhiteSpace(input.BudgetCategory), e => e.BudgetCategory.ToString() == input.BudgetCategory)
                .ToListAsync();

            var budgetsDto = query.MapTo<List<GetBudgetForViewDto>>();

            if (input.BudgetCategory == BudgetCategory.Budgeted.ToString())
            {
                return _budgetsExcelExporter.ExportToFile(budgetsDto);
            }
            else
                return _budgetsExcelExporter.ExportUnbudgetedToFile(budgetsDto);
        }

        public List<string> GetPaperStatus()
        {
            return Enum.GetNames(typeof(UseStatus)).ToList();
        }

        public List<string> GetBudgetType()
        {
            return Enum.GetNames(typeof(BudgetType)).ToList();
        }

        public List<string> GetDescriptionType()
        {
            return Enum.GetNames(typeof(DescriptionType)).ToList();
        }

        public List<string> GetRequestStatus()
        {
            return Enum.GetNames(typeof(RequestStatus)).ToList();
        }

        public async Task<ListResultDto<OrganizationUnitDto>> GetOrganizationUnitByUserId(int id)
        {
            var query =
                from ou in _organizationUnitRepository.GetAll()
                join uou in _userOrganizationUnitRepository.GetAll() on ou.Id equals uou.OrganizationUnitId
                where uou.UserId == id
                select new { ou };

            var items = await query.ToListAsync();

            return new ListResultDto<OrganizationUnitDto>(
                items.Select(item =>
                {
                    var dto = ObjectMapper.Map<OrganizationUnitDto>(item.ou);
                    return dto;
                }).ToList());
        }

    }
}