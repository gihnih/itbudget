﻿namespace Base2.Authorization.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}