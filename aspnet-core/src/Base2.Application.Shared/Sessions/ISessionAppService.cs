﻿using Abp.Application.Services;
using Base2.Sessions.Dto;
using System.Threading.Tasks;

namespace Base2.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}