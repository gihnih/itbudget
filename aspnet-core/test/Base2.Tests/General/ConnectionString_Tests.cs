﻿using Shouldly;
using System.Data.SqlClient;
using Xunit;

namespace Base2.Tests.General
{
    public class ConnectionString_Tests
    {
        [Fact]
        public void SqlConnectionStringBuilder_Test()
        {
            var csb = new SqlConnectionStringBuilder("Server=localhost; Database=Base2; Trusted_Connection=True;");
            csb["Database"].ShouldBe("Base2");
        }
    }
}