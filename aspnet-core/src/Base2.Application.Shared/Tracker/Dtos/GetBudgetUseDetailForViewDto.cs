using Abp.Application.Services.Dto;
using Base2.Authorization.Users.Dto;
using Humanizer;
using System.Collections.Generic;

namespace Base2.Tracker.Dtos
{
    public class GetBudgetUseDetailForViewDto
    {
        public GetBudgetUseForViewDto BudgetUse { get; set; }

        public UserListDto CreatorUser { get; set; }
    }
}