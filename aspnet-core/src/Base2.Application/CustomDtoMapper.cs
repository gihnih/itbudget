﻿using Base2.Sample.Dtos;
using Base2.Sample;
using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using AutoMapper;
using Base2.Auditing.Dto;
using Base2.Authorization.Accounts.Dto;
using Base2.Authorization.Permissions.Dto;
using Base2.Authorization.Roles;
using Base2.Authorization.Roles.Dto;
using Base2.Authorization.Users;
using Base2.Authorization.Users.Dto;
using Base2.Authorization.Users.Profile.Dto;
using Base2.Editions;
using Base2.Editions.Dto;
using Base2.Localization.Dto;
using Base2.MultiTenancy;
using Base2.MultiTenancy.Dto;
using Base2.MultiTenancy.HostDashboard.Dto;
using Base2.MultiTenancy.Payments;
using Base2.MultiTenancy.Payments.Dto;
using Base2.Notifications.Dto;
using Base2.Organizations.Dto;
using Base2.Sessions.Dto;
using Base2.Tracker;
using Base2.Tracker.Dtos;

namespace Base2
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateOrEditSampleEntityDto, SampleEntity>().ReverseMap();
            configuration.CreateMap<SampleEntityDto, SampleEntity>().ReverseMap();
            configuration.CreateMap<CreateOrEditBudgetApproverDto, BudgetApprover>();
            configuration.CreateMap<BudgetApprover, BudgetApproverDto>();
            configuration.CreateMap<CreateOrEditBudgetRequestDto, BudgetRequest>();
            configuration.CreateMap<BudgetRequest, BudgetRequestDto>();
            configuration.CreateMap<CreateOrEditBudgetUseDto, BudgetUse>();
            configuration.CreateMap<BudgetUse, BudgetUseDto>();
            configuration.CreateMap<CreateOrEditUseStatusDto, UseStatus>();
            configuration.CreateMap<UseStatus, UseStatusDto>();
            configuration.CreateMap<CreateOrEditBudgetDto, Budget>();
            configuration.CreateMap<Budget, BudgetDto>();
            configuration.CreateMap<CreateOrEditDescriptionTypeDto, DescriptionType>();
            configuration.CreateMap<DescriptionType, DescriptionTypeDto>();
            configuration.CreateMap<CreateOrEditGLCodeDto, GLCode>();
            configuration.CreateMap<GLCode, GLCodeDto>();
            configuration.CreateMap<CreateOrEditBudgetTypeDto, BudgetType>();
            configuration.CreateMap<BudgetType, BudgetTypeDto>();
            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();

            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
        }
    }
}