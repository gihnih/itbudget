import {Component, ViewChild, Injector, Output, EventEmitter, OnInit} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {finalize} from 'rxjs/operators';
import {
    BudgetGLCodeLookupTableDto, BudgetOrganizationUnitLookupTableDto,
    BudgetsServiceProxy,
    CreateOrEditBudgetDto, OrganizationUnitServiceProxy
} from '@shared/service-proxies/service-proxies';
import {AppComponentBase} from '@shared/common/app-component-base';
import {GLCodeLookupTableModalComponent} from './glCode-lookup-table-modal.component';
import {OrganizationUnitLookupTableModalComponent} from './organizationUnit-lookup-table-modal.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '@app/global';
import {AppSessionService} from '@shared/common/session/app-session.service';


@Component({
    selector: 'createOrEditBudgetModal',
    templateUrl: './create-or-edit-budget-modal.component.html'
})
export class CreateOrEditBudgetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('glCodeLookupTableModal') glCodeLookupTableModal: GLCodeLookupTableModalComponent;
    @ViewChild('organizationUnitLookupTableModal') organizationUnitLookupTableModal: OrganizationUnitLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;


    budgetTypeName = '';
    glCodeCode = '';
    budgetYearYear = '';
    organizationUnitDisplayName = '';

    selectBudgetType = '';

    budgetCategory = 'Budgeted';


    budget: CreateOrEditBudgetDto = new CreateOrEditBudgetDto();
    // Form select
    budgetTypes = this._globals.budgetType;
    orgUnits: BudgetOrganizationUnitLookupTableDto[];
    glCodes: BudgetGLCodeLookupTableDto[];
    descTypes = this._globals.budgetDescriptionType;

    constructor(
        injector: Injector,
        private _budgetsServiceProxy: BudgetsServiceProxy,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private _globals: Globals,
        // private _organizationUnitProxy: OrganizationUnitServiceProxy,
        private _appSession: AppSessionService
    ) {
        super(injector);
        this.budget = new CreateOrEditBudgetDto();
    }


    show(budgetId?: number): void {
        console.log(budgetId);
        this.selectBudgetType = this.activatedRoute.snapshot.url[this.activatedRoute.snapshot.url.length - 1].path;
        this.getGLCode();
        this.getOrgUnit();
        if (!budgetId) {
            this.budget = new CreateOrEditBudgetDto();
            this.budget.budgetCategory = this.budgetCategory;
            this.budget.id = budgetId;
            this.active = true;
            this.modal.show();
        } else {
            if (this.budgetCategory === 'Budgeted') {
                this._budgetsServiceProxy.getBudgetForEdit(budgetId).subscribe(result => {
                    this.budget = result.budget;

                    this.budgetTypeName = result.budgetTypeName;
                    this.glCodeCode = result.glCodeCode;
                    this.budgetYearYear = result.budgetYearYear;
                    this.organizationUnitDisplayName = result.organizationUnitDisplayName;

                    this.active = true;
                    this.modal.show();
                });
            } else {
                this._budgetsServiceProxy.getBudgetForEditUnBudget(budgetId).subscribe(result => {
                    this.budget = result.budget;

                    this.budgetTypeName = result.budgetTypeName;
                    this.glCodeCode = result.glCodeCode;
                    this.budgetYearYear = result.budgetYearYear;
                    this.organizationUnitDisplayName = result.organizationUnitDisplayName;

                    this.active = true;
                    this.modal.show();
                });
            }

        }
    }

    save(): void {
        this.saving = true;
        this.budget.isBudgeted = true;
        console.log(this.budget);

        if (this.budgetCategory === 'Budgeted') {
            this._budgetsServiceProxy.createOrEdit(this.budget)
                .pipe(finalize(() => {
                    this.saving = false;
                }))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                });
        } else {
            this._budgetsServiceProxy.createOrEditUnBudget(this.budget)
                .pipe(finalize(() => {
                    this.saving = false;
                }))
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                });
        }

    }

    getNewGLCodeId() {
        this.glCodeCode = this.glCodeLookupTableModal.displayName;
    }

    close(): void {
        // this.budget = new CreateOrEditBudgetDto();
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }

    getGLCode() {
        this._budgetsServiceProxy.getAllGLCode().subscribe(result => {
            this.glCodes = result.items;
        });
    }

    getOrgUnit() {
        if (this.budgetCategory === 'Budgeted') {
            this._budgetsServiceProxy.getAllOrganizationUnit().subscribe(result => {
                this.orgUnits = result.items;
            });
        } else {
            this._budgetsServiceProxy.getOrganizationUnitByUserId(this._appSession.userId).subscribe(result => {
                this.orgUnits = result.items;
            });
        }

    }
}
