﻿using Abp.Notifications;
using Base2.Dto;

namespace Base2.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}