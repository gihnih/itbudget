namespace Base2.Tracker.Dtos
{
    public class GetUseStatusForViewDto
    {
        public UseStatusDto UseStatus { get; set; }
    }
}