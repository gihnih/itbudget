namespace Base2.Tracker.Dtos
{
    public class BudgetBudgetYearLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}