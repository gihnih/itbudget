namespace Base2.Tracker.Dtos
{
    public class BudgetUseBudgetLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }

        public decimal CurrentAmount { get; set; }

        public long? OrganizationUnitId { get; set; }
    }
}