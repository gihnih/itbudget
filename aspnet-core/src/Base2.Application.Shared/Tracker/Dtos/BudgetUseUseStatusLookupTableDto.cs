namespace Base2.Tracker.Dtos
{
    public class BudgetUseUseStatusLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}