using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class CreateOrEditBudgetApproverDto : EntityDto<int?>
    {
        public long UserId { get; set; }

        public int BudgetUseId { get; set; }

        public int BudgetId { get; set; }

        public int UseStatusId { get; set; }

        public decimal RequestAmount { get; set; }

        public decimal ActualAmount { get; set; }
    }
}