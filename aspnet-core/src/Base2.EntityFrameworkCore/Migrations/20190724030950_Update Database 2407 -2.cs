﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class UpdateDatabase24072 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnBudget",
                table: "BudgetUses");

            migrationBuilder.AddColumn<bool>(
                name: "OwnBudget",
                table: "BudgetRequests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "RequestStatus",
                table: "BudgetRequests",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnBudget",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "RequestStatus",
                table: "BudgetRequests");

            migrationBuilder.AddColumn<bool>(
                name: "OwnBudget",
                table: "BudgetUses",
                nullable: false,
                defaultValue: false);
        }
    }
}