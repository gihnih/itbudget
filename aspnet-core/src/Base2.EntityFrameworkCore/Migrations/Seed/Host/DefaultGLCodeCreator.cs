﻿using Base2.EntityFrameworkCore;
using Base2.Tracker;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Base2.Migrations.Seed.Host
{
    public class DefaultGLCodeCreator
    {
        public static List<GLCode> InitialGLCodes => GetInitialGLCodes();

        private readonly Base2DbContext _context;

        private static List<GLCode> GetInitialGLCodes()
        {
            return new List<GLCode>
            {
                new GLCode{Code = "000000000" , Description = "No GL Code"},
            };
        }

        public DefaultGLCodeCreator(Base2DbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateGLCodes();
        }

        private void CreateGLCodes()
        {
            foreach (var code in InitialGLCodes)
            {
                AddGLCodeIfNotExists(code);
            }
        }

        private void AddGLCodeIfNotExists(GLCode code)
        {
            if (_context.GLCodes.IgnoreQueryFilters().Any(l => l.Code == code.Code))
            {
                return;
            }

            _context.GLCodes.Add(code);

            _context.SaveChanges();
        }
    }
}