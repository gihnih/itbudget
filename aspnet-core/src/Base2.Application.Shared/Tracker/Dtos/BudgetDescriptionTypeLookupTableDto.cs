namespace Base2.Tracker.Dtos
{
    public class BudgetDescriptionTypeLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}