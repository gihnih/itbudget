﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Base2.Tracker.Dtos
{
    public class BinaryObjectInput
    {
        public Guid Id { get; set; }
        public int BudgetUseId { get; set; }

        public string Name { get; set; }

        public string ContentType { get; set; }

        public string Remark { get; set; }
    }
}
