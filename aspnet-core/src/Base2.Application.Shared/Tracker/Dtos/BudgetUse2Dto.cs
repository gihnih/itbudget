using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Base2.Tracker.Dtos
{
    public class BudgetUse2Dto : EntityDto
    {
        public string Description { get; set; }

        public decimal RequestAmount { get; set; }

        public decimal ActualAmount { get; set; }

        public string PaperStatus { get; set; }

        public string RequestStatus { get; set; }

        public bool OwnBudget { get; set; }

        public long OrganizationUnitId { get; set; }

        public BudgetOrganizationUnitLookupTableDto OrganizationUnit { get; set; }

        public ICollection<BinaryObjectDto> BinaryObjects { get; set; }
    }
}