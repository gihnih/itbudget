namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetTypesForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }
    }
}