using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Organizations;
using Abp.UI;
using Abp.Zero.Configuration;
using Base2.Authorization;
using Base2.Authorization.Roles;
using Base2.Authorization.Users;
using Base2.Authorization.Users.Dto;
using Base2.Dto;
using Base2.Organizations.Dto;
using Base2.Storage;
using Base2.Tracker.Dtos;
using Base2.Tracker.Exporting;
using Base2.Url;
using Humanizer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    [AbpAuthorize(AppPermissions.Pages_BudgetUses)]
    public class BudgetUsesAppService : Base2AppServiceBase, IBudgetUsesAppService
    {
        public IAppUrlService AppUrlService { get; set; }
        private readonly IRepository<BudgetUse> _budgetUseRepository;
        private readonly IBudgetUsesExcelExporter _budgetUsesExcelExporter;
        private readonly IRepository<Budget, int> _budgetRepository;
        private readonly IRepository<BudgetRequest> _budgetRequestRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<BudgetApprover> _budgetApproverRepository;
        private readonly IRepository<User, long> _user;
        private readonly UserManager _userManager;
        private readonly IRepository<GLCode, int> _glCodeRepository;

        private readonly IEmailSender _emailSender;
        private readonly IUserEmailer _userEmailer;
        private readonly RoleManager _roleManager;
        private readonly OrganizationUnitManager _organizationUnitManager;

        private readonly IBinaryObjectManager _binaryObjectManager;

        public BudgetUsesAppService(IRepository<BudgetUse> budgetUseRepository,
            IBudgetUsesExcelExporter budgetUsesExcelExporter,
            IRepository<Budget, int> budgetRepository,
            IRepository<BudgetRequest> budgetRequestRepository,
            IRepository<BudgetApprover> budgetApproverRepository, IRepository<User, long> user,
            UserManager userManager, IRepository<GLCode, int> glCodeRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IEmailSender emailSender, IUserEmailer userEmailer, RoleManager roleManager,
            OrganizationUnitManager organizationUnitManager, 
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IBinaryObjectManager binaryObjectManager)
        {
            _budgetUseRepository = budgetUseRepository;
            _budgetUsesExcelExporter = budgetUsesExcelExporter;
            _budgetRepository = budgetRepository;
            _budgetRequestRepository = budgetRequestRepository;
            _budgetApproverRepository = budgetApproverRepository;
            _user = user;
            _userManager = userManager;
            _organizationUnitRepository = organizationUnitRepository;
            _glCodeRepository = glCodeRepository;
            _emailSender = emailSender;
            _userEmailer = userEmailer;
            _roleManager = roleManager;
            _organizationUnitManager = organizationUnitManager;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _binaryObjectManager = binaryObjectManager;
        }

        public async Task<PagedResultDto<GetBudgetUseForViewDto>> GetAll(GetAllBudgetUsesInput input)
        {
            try
            {
                var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
                var orgUnit = await _userManager.GetOrganizationUnitsAsync(user);
                var orgUnitCodes = orgUnit.Select(ou => ou.Id).ToList();

                var query = _budgetUseRepository
                .GetAll()
                .Include(ou => ou.OrganizationUnit)
                .Where(ou => orgUnitCodes.Any(unitId => ou.OrganizationUnitId == unitId))
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower().Contains(input.DescriptionFilter.ToLower()))
                .WhereIf(!string.IsNullOrWhiteSpace(input.PaperStatus), e => e.PaperStatus.ToString().ToLower() == input.PaperStatus.ToLower())
                .WhereIf(!string.IsNullOrWhiteSpace(input.RequestStatus), e => e.RequestStatus.ToString().ToLower() == input.RequestStatus.ToLower())
                .WhereIf(!string.IsNullOrWhiteSpace(input.Year), e => e.CreationTime.Year.ToString() == input.Year);

                //var query2 = query.Select(br => br.BudgetRequests.Select(b => b.Budget))
                //where orgUnitCodes.Any(code => !organizationUnit.Code.StartsWith(code))

                var budgetUse = await query
                    .OrderBy(input.Sorting ?? "id desc")
                    .PageBy(input)
                    .ToListAsync();

                var total = await query.CountAsync();
                var budgetUseDto = ObjectMapper.Map<List<GetBudgetUseForViewDto>>(budgetUse);

                return new PagedResultDto<GetBudgetUseForViewDto>(
                    total,
                    budgetUseDto);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<PagedResultDto<GetBudgetUseForViewDto>> GetAllCustodian(GetAllBudgetUsesInput input)
        {
            try
            {
                var query = _budgetUseRepository
                .GetAll()
                .Include(ou => ou.OrganizationUnit)
                .Where(w => w.OrganizationUnitId != null)
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower().Contains(input.DescriptionFilter.ToLower()))
                .WhereIf(!string.IsNullOrWhiteSpace(input.PaperStatus), e => e.PaperStatus.ToString().ToLower() == input.PaperStatus.ToLower())
                .WhereIf(!string.IsNullOrWhiteSpace(input.RequestStatus), e => e.RequestStatus.ToString().ToLower() == input.RequestStatus.ToLower())
                .WhereIf(!string.IsNullOrWhiteSpace(input.Year), e => e.CreationTime.Year.ToString() == input.Year);

                var budgetUse = await query
                    .OrderBy(input.Sorting ?? "id desc")
                    .PageBy(input)
                    .ToListAsync();

                var total = await query.CountAsync();
                var budgetUseDto = ObjectMapper.Map<List<GetBudgetUseForViewDto>>(budgetUse);

                return new PagedResultDto<GetBudgetUseForViewDto>(
                    total,
                    budgetUseDto);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        public async Task<GetBudgetUseDetailForViewDto> GetBudgetUseDetailForView(int id)
        {
            var budgetUse = await _budgetUseRepository.GetAll()
                .Include(e => e.BinaryObjects)
                .Include(br => br.BudgetRequests)
                    .ThenInclude(b => b.Budget)
                        .ThenInclude(gl => gl.GLCode)
                .Include(br => br.BudgetRequests)
                    .ThenInclude(b => b.Budget)
                        .ThenInclude(ou => ou.OrganizationUnit)
                .Include(ou => ou.OrganizationUnit)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();

            var budgetUseDto = budgetUse.MapTo<GetBudgetUseForViewDto>();
            var CreatorUser = _userManager.GetUserByIdAsync(budgetUseDto.CreatorUserId).Result;

            var budgetUseDetailDto = new GetBudgetUseDetailForViewDto
            {
                BudgetUse = budgetUse.MapTo<GetBudgetUseForViewDto>(),
                CreatorUser = CreatorUser.MapTo<UserListDto>()
            };

            return budgetUseDetailDto;
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses_Edit)]
        public async Task<GetBudgetUseForEditOutput> GetBudgetUseForEdit(EntityDto input)
        {
            var budgetUse = await _budgetUseRepository.GetAll()
                .Include(e => e.BinaryObjects)
                 .Include(br => br.BudgetRequests)
                    .ThenInclude(b => b.Budget)
                        .ThenInclude(gl => gl.GLCode)
                .Include(br => br.BudgetRequests)
                    .ThenInclude(b => b.Budget)
                        .ThenInclude(ou => ou.OrganizationUnit)
                .Include(ou => ou.OrganizationUnit)
                 .Where(e => e.Id == input.Id)
                 .FirstOrDefaultAsync();
            var output = new GetBudgetUseForEditOutput { BudgetUse = ObjectMapper.Map<CreateOrEditBudgetUseDto>(budgetUse) };
            return output;
        }

        public async Task CreateOrEdit(GetBudgetUseForEditOutput input)
        {
            if (input.BudgetUse.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses_Create)]
        private async Task Create(GetBudgetUseForEditOutput input)
        {
            
            var budgets = input.BudgetUse.BudgetRequests.Select(b => b.Budget);
            var budgetUses = input.BudgetUse;
            var budgetRequests = input.BudgetUse.BudgetRequests;
            //budgetUse
            var newBudgetUse = new BudgetUse
            {
                Description = input.BudgetUse.Description,
                RequestAmount = input.BudgetUse.RequestAmount,
                PaperStatus = (PaperStatus)Enum.Parse(typeof(PaperStatus), input.BudgetUse.PaperStatus),
                RequestStatus = RequestStatus.Submitted,
                OrganizationUnitId = input.BudgetUse.OrganizationUnitId
            };
            //var budgetUse = input.MapTo<BudgetUse>();
            var budgetUse = await _budgetUseRepository.InsertAsync(newBudgetUse);
            var budgetuseid = budgetUse.Id;

            foreach (var item in input.BudgetUse.BudgetRequests)
            {
                //budgetRequest
                var budgetRequest = new BudgetRequest
                {
                    BudgetId = item.BudgetId,
                    BudgetUseId = budgetuseid,

                    RequestAmount = item.RequestAmount,
                    OwnBudget = item.OwnBudget,
                    RequestStatus = RequestStatus.Submitted
                };

                await _budgetRequestRepository.InsertAsync(budgetRequest);

                

                //updateBudget
                var budget = item.Budget;

                if ((budget.EstimatedBalance - item.RequestAmount) < 0 )
                    throw new UserFriendlyException("Budget Insufficient");

                budget.EstimatedBalance = budget.EstimatedBalance - item.RequestAmount;
                budget.UtilizedBudget = budget.UtilizedBudget + item.RequestAmount;
                budget.DescriptionType = item.Budget.DescriptionType.Dehumanize();

                var budgetDto = ObjectMapper.Map<CreateOrEditBudgetDto>(budget);
                var b = await _budgetRepository.FirstOrDefaultAsync((int)budget.Id);
                ObjectMapper.Map(budgetDto, b);
            }

            // Attachment : Update detail
            foreach (var att in input.BudgetUse.BinaryObjects)
            {
                var attachment = await _binaryObjectManager.GetOrNullAsync(att.Id);
                attachment.BudgetUseId = budgetuseid;
                attachment.ContentType = att.ContentType;
                attachment.Name = att.Name;
                attachment.Remark = att.Remark;
            }

            // get users as budget owner
            foreach (var item in budgetRequests)
            {
                if (item.OwnBudget)
                {
                    // send email to custodian
                    // get email budgetcustodian
                    var users = await _userManager.GetUsersInRoleAsync("Custodian");

                    foreach (var user in users)
                    {
                        await _userEmailer.SendTaskBudgetedUseCustodian(ObjectMapper.Map<User>(user), "");
                    }
                }
                else
                {
                    // send email to budget owner first
                    // get email budget owner
                    var users = from uou in _userOrganizationUnitRepository.GetAll()
                                join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
                                join user in UserManager.Users on uou.UserId equals user.Id
                                where uou.OrganizationUnitId == item.Budget.OrganizationUnitId
                                select new { user };

                    foreach (var user in users)
                    {
                        await _userEmailer.SendTaskBudgetedUseBudgetOwner(ObjectMapper.Map<User>(user.user), "");
                    }
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();

        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses_Edit)]
        private async Task Update(GetBudgetUseForEditOutput input)
        {
            //Update BudgetUse
            using (UnitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                // get budgetUse to edit
                var budgetUseDetail = await _budgetUseRepository.GetAll()
                 .Include(br => br.BudgetRequests)
                 .ThenInclude(b => b.Budget)
                 .Where(e => e.Id == input.BudgetUse.Id)
                 .FirstOrDefaultAsync();

                // update PaperStatus
                budgetUseDetail.PaperStatus = (PaperStatus)Enum.Parse(typeof(PaperStatus), input.BudgetUse.PaperStatus);

                // Check actual amount update or not
                if (input.IsActualAmountUpdate)
                {
                    //if LOA Issued Status
                    if (input.BudgetUse.PaperStatus == PaperStatus.LOAIssued.ToString())
                    {
                        //if Budget Request only one and ownBudget is true then update budget utilize amount
                        if (input.BudgetUse.BudgetRequests.Count() == 1 && input.BudgetUse.BudgetRequests.FirstOrDefault().OwnBudget)
                        {
                            var budget = budgetUseDetail.BudgetRequests.FirstOrDefault().Budget;
                            budget.EstimatedBalance += budgetUseDetail.ActualAmount == 0 ? budgetUseDetail.RequestAmount : budgetUseDetail.ActualAmount;
                            budget.UtilizedBudget -= budgetUseDetail.ActualAmount == 0 ? budgetUseDetail.RequestAmount : budgetUseDetail.ActualAmount;

                            budget.EstimatedBalance -= input.BudgetUse.ActualAmount;
                            budget.UtilizedBudget += input.BudgetUse.ActualAmount;
                        }
                        budgetUseDetail.ActualAmount = input.BudgetUse.ActualAmount;
                    }
                    else
                    {
                        budgetUseDetail.PaperStatus = (PaperStatus)Enum.Parse(typeof(PaperStatus), input.BudgetUse.PaperStatus);

                        var budget = budgetUseDetail.BudgetRequests.FirstOrDefault().Budget;
                        budget.EstimatedBalance += budgetUseDetail.ActualAmount;
                        budget.UtilizedBudget -= budgetUseDetail.ActualAmount;

                        budget.EstimatedBalance -= budgetUseDetail.RequestAmount;
                        budget.UtilizedBudget += budgetUseDetail.RequestAmount;

                        budgetUseDetail.ActualAmount = 0;
                    }
                }
                else
                {
                    //Update budgetUseDetail
                    budgetUseDetail.OrganizationUnitId = input.BudgetUse.OrganizationUnitId;
                    budgetUseDetail.Description = input.BudgetUse.Description;
                    budgetUseDetail.RequestAmount = input.BudgetUse.RequestAmount;

                    var existingRequest = budgetUseDetail.BudgetRequests.Select(r => new { r.BudgetId, r.BudgetUseId }).ToArray();
                    var updatedRequest = input.BudgetUse.BudgetRequests.Select(r => new { r.BudgetId, r.BudgetUseId }).ToArray();

                    // find the new one
                    foreach (var request in updatedRequest)
                    {
                        var availableInExisting = existingRequest.Any(r => r.BudgetId == request.BudgetId && r.BudgetUseId == request.BudgetUseId);

                        if (!availableInExisting)
                        {
                            // perform insert
                            Console.WriteLine(request);
                            var addedRequest = input.BudgetUse.BudgetRequests.Where(r => r.BudgetUseId == request.BudgetUseId && r.BudgetId == request.BudgetId).Single();

                            budgetUseDetail.BudgetRequests.Add(new BudgetRequest
                            {
                                BudgetId = request.BudgetId,
                                BudgetUseId = input.BudgetUse.Id,
                                RequestAmount = addedRequest.RequestAmount,
                                OwnBudget = addedRequest.OwnBudget,
                                RequestStatus = RequestStatus.Submitted,
                            });

                            // update budget EstimatedBalance and UtilizeBudget
                            var budget = await _budgetRepository.FirstOrDefaultAsync(request.BudgetId.Value);
                            budget.EstimatedBalance -= addedRequest.RequestAmount;
                            budget.UtilizedBudget += addedRequest.RequestAmount;
                        }
                        else
                        {
                            // perform update
                            Console.WriteLine(request);
                            var updatedItem = input.BudgetUse.BudgetRequests.Where(r => r.BudgetUseId == request.BudgetUseId && r.BudgetId == request.BudgetId).Single();

                            var requestToUpdate = budgetUseDetail.BudgetRequests.Single(r => r.BudgetUseId == request.BudgetUseId && r.BudgetId == request.BudgetId);

                            // update return budget EstimatedBalance and UtilizeBudget
                            var budget = await _budgetRepository.FirstOrDefaultAsync(request.BudgetId.Value);
                            var isRequestBudgetActive = !requestToUpdate.IsDeleted;
                            // if budget in use; user change amount need to return an first
                            if (isRequestBudgetActive)
                            {
                                budget.EstimatedBalance += requestToUpdate.RequestAmount;
                                budget.UtilizedBudget -= requestToUpdate.RequestAmount;
                            }

                            // Check budget sufficient or not
                            if ((budget.EstimatedBalance -= updatedItem.RequestAmount) < 0)
                                throw new UserFriendlyException("Budget Insufficient");

                            // update deduct budget EstimatedBalance and UtilizeBudget
                            budget.EstimatedBalance -= updatedItem.RequestAmount;
                            budget.UtilizedBudget += updatedItem.RequestAmount;

                            requestToUpdate.ActualAmount = updatedItem.ActualAmount;
                            requestToUpdate.RequestAmount = updatedItem.RequestAmount;
                            requestToUpdate.RequestStatus = RequestStatus.Submitted;
                            requestToUpdate.OwnBudget = updatedItem.OwnBudget;
                            requestToUpdate.IsDeleted = false;
                            requestToUpdate.DeleterUserId = null;
                            requestToUpdate.DeletionTime = null;
                        }
                    }

                    // exclude new record
                    var requestToCompareForDelete = updatedRequest.Where(r => r.BudgetUseId != null);

                    foreach (var request in existingRequest)
                    {
                        var requestExistInUpdatedRequest = requestToCompareForDelete.Any(r => r.BudgetId == request.BudgetId && r.BudgetUseId == request.BudgetUseId);
                        var isBudgetRequestActive = budgetUseDetail.BudgetRequests.Any(r => r.BudgetUseId == request.BudgetUseId && r.BudgetId == request.BudgetId && r.IsDeleted == false);

                        if (!requestExistInUpdatedRequest && isBudgetRequestActive)
                        {
                            var requestToDelete = budgetUseDetail.BudgetRequests.Single(r => r.BudgetUseId == request.BudgetUseId && r.BudgetId == request.BudgetId);

                            // update budget value
                            var budget = await _budgetRepository.FirstOrDefaultAsync(request.BudgetId.Value);
                            budget.EstimatedBalance += requestToDelete.RequestAmount;
                            budget.UtilizedBudget -= requestToDelete.RequestAmount;

                            // perform delete
                            await _budgetRequestRepository.DeleteAsync(requestToDelete);
                        }
                    }
                }

                // Attachment : Update detail
                foreach (var att in input.BudgetUse.BinaryObjects)
                {
                    var attachment = await _binaryObjectManager.GetOrNullAsync(att.Id);
                    attachment.BudgetUseId = budgetUseDetail.Id;
                    attachment.ContentType = att.ContentType;
                    attachment.Name = att.Name;
                    attachment.Remark = att.Remark;
                }

                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses_Delete)]
        public async Task Delete(EntityDto input)
        {
            // get budgetUse to edit
            var budgetUseDetail = await _budgetUseRepository.GetAll()
             .Include(br => br.BudgetRequests)
                .ThenInclude(b => b.Budget)
             .Where(e => e.Id == input.Id)
             .FirstOrDefaultAsync();

            if (budgetUseDetail.RequestStatus != RequestStatus.Declined || budgetUseDetail.RequestStatus != RequestStatus.Rejected)
            {
                foreach (var budgetItem in budgetUseDetail.BudgetRequests)
                {
                    budgetItem.Budget.EstimatedBalance = budgetItem.Budget.EstimatedBalance + budgetItem.RequestAmount;
                    budgetItem.Budget.UtilizedBudget = budgetItem.Budget.UtilizedBudget - budgetItem.RequestAmount;
                }

                var attachments = await _budgetUseRepository.GetAll()
                    .Include(e => e.BinaryObjects)
                    .Where(e => e.Id == input.Id)
                    .FirstOrDefaultAsync();

                await _budgetUseRepository.DeleteAsync(input.Id);

                foreach (var att in attachments.BinaryObjects)
                {
                    await _binaryObjectManager.DeleteAsync(att.Id);
                }
            }
            else
                throw new UserFriendlyException("Item cannot be delete");


            await CurrentUnitOfWork.SaveChangesAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_BudgetUses)]
        public async Task<PagedResultDto<GetBudgetForViewDto>> GetAllBudgetForLookupTable(GetAllForLookupTableInput input)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
            var orgUnit = await _userManager.GetOrganizationUnitsAsync(user);
            var orgUnitCodes = orgUnit.Select(ou => ou.Code).ToList();

            var query = from budget in _budgetRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter) || e.Remark.Contains(input.Filter))
                        where budget.BudgetCategory.ToString() == BudgetCategory.Budgeted.ToString()
                        join glCode in _glCodeRepository.GetAll() on budget.GLCodeId equals glCode.Id
                        join organizationUnit in _organizationUnitRepository.GetAll() on budget.OrganizationUnitId equals organizationUnit.Id
                        where orgUnitCodes.Any(code => organizationUnit.Code.StartsWith(code))
                        select new GetBudgetForViewDto
                        {
                            //Budget = ObjectMapper.Map<BudgetDto>(budget),
                            //GLCode = ObjectMapper.Map<GLCodeDto>(glCode),
                            //OrganizationUnit = ObjectMapper.Map<BudgetOrganizationUnitLookupTableDto>(organizationUnit),
                            //BudgetUses = ObjectMapper.Map<List<BudgetUseDto>>(budget.BudgetRequests.Select(b => b.BudgetUse))
                        };

            var totalCount = await query.CountAsync();

            var budgets = await query
                .OrderBy(input.Sorting ?? "budget.id desc")
                .PageBy(input)
               .ToListAsync();
            return new PagedResultDto<GetBudgetForViewDto>(
                totalCount,
                budgets
            );
        }

        public async Task<PagedResultDto<GetBudgetForViewDto>> GetAllOtherBudgetForLookupTable(GetAllForLookupTableInput input)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
            var orgUnit = await _userManager.GetOrganizationUnitsAsync(user);
            var orgUnitCodes = orgUnit.Select(ou => ou.Code).ToList();

            var query = from budget in _budgetRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter) || e.Remark.Contains(input.Filter))
                        where budget.BudgetCategory.ToString() == BudgetCategory.Budgeted.ToString()
                        join glCode in _glCodeRepository.GetAll() on budget.GLCodeId equals glCode.Id
                        join organizationUnit in _organizationUnitRepository.GetAll() on budget.OrganizationUnitId equals organizationUnit.Id
                        where orgUnitCodes.Any(code => !organizationUnit.Code.StartsWith(code))
                        select new GetBudgetForViewDto
                        {
                            //Budget = ObjectMapper.Map<BudgetDto>(budget),
                            //GLCode = ObjectMapper.Map<GLCodeDto>(glCode),
                            //OrganizationUnit = ObjectMapper.Map<BudgetOrganizationUnitLookupTableDto>(organizationUnit),
                            //BudgetUses = ObjectMapper.Map<List<BudgetUseDto>>(budget.BudgetRequests.Select(b => b.BudgetUse))
                        };

            var totalCount = await query.CountAsync();

            var budgets = await query
                .OrderBy(input.Sorting ?? "budget.id desc")
                .PageBy(input)
               .ToListAsync();
            return new PagedResultDto<GetBudgetForViewDto>(
                totalCount,
                budgets
            );
        }

        public List<string> GetPaperStatus()
        {
            return Enum.GetNames(typeof(UseStatus)).ToList();
        }

        public async Task<FileDto> getBudgetUsesToExcel(GetAllBudgetUsesForExcelInput input)
        {
            var query = _budgetUseRepository
                .GetAll()
                .Include(o => o.OrganizationUnit)
                .Include(br => br.BudgetRequests)
                    .ThenInclude(b => b.Budget)
                        .ThenInclude(gl => gl.GLCode)
                .Include(br => br.BudgetRequests)
                    .ThenInclude(b => b.Budget)
                        .ThenInclude(ou => ou.OrganizationUnit)
                .Include(ou => ou.OrganizationUnit)
                .Where(w => w.OrganizationUnitId != null)
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower().Contains(input.DescriptionFilter.ToLower()))
                .WhereIf(!string.IsNullOrWhiteSpace(input.PaperStatus), e => e.PaperStatus.ToString().ToLower() == input.PaperStatus.ToLower())
                .WhereIf(!string.IsNullOrWhiteSpace(input.RequestStatus), e => e.RequestStatus.ToString().ToLower() == input.RequestStatus.ToLower());

            //var query2 = query.Select(br => br.BudgetRequests.Select(b => b.Budget))
            //where orgUnitCodes.Any(code => !organizationUnit.Code.StartsWith(code))

            var budgetUse = await query
                .ToListAsync();

            var budgetUseDto = ObjectMapper.Map<List<GetBudgetUseForViewDto>>(budgetUse);

            return _budgetUsesExcelExporter.ExportToFile(budgetUseDto);
        }

        public async Task<FileDto> getBudgetUsesMyDepartmentToExcel(GetAllBudgetUsesForExcelInput input)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
            var orgUnit = await _userManager.GetOrganizationUnitsAsync(user);
            var orgUnitCodes = orgUnit.Select(ou => ou.Id).ToList();

            var query = _budgetUseRepository
            .GetAll()
            .Include(o => o.OrganizationUnit)
            .Include(br => br.BudgetRequests)
                .ThenInclude(b => b.Budget)
                    .ThenInclude(gl => gl.GLCode)
            .Include(br => br.BudgetRequests)
                .ThenInclude(b => b.Budget)
                    .ThenInclude(ou => ou.OrganizationUnit)
            .Include(ou => ou.OrganizationUnit)
            .Where(ou => orgUnitCodes.Any(unitId => ou.OrganizationUnitId == unitId))
            .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.ToLower().Contains(input.DescriptionFilter.ToLower()))
            .WhereIf(!string.IsNullOrWhiteSpace(input.PaperStatus), e => e.PaperStatus.ToString().ToLower() == input.PaperStatus.ToLower())
            .WhereIf(!string.IsNullOrWhiteSpace(input.RequestStatus), e => e.RequestStatus.ToString().ToLower() == input.RequestStatus.ToLower());

            var budgetUse = await query
                .ToListAsync();

            var budgetUseDto = ObjectMapper.Map<List<GetBudgetUseForViewDto>>(budgetUse);

            return _budgetUsesExcelExporter.ExportToFile(budgetUseDto);
        }

        public async Task<List<OrganizationUnit>> GetAllOrganizationUnit()
        {
            var query = from uou in _userOrganizationUnitRepository.GetAll()
                        join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
                        join user in UserManager.Users on uou.UserId equals user.Id
                        where user.Id == AbpSession.UserId.Value
                        select new { ou };

            var items = await query.ToListAsync();

            var orgUnitDto = items.Select(item =>
            {
                var dto = ObjectMapper.Map<OrganizationUnit>(item.ou);
                return dto;
            }).ToList();

            return orgUnitDto;
        }

        public async Task<List<BudgetOrganizationUnitLookupTableDto>> GetListOrganizationUnit()
        {
            var query = _organizationUnitRepository.GetAll();

            var items = await query.ToListAsync();

            var orgDto = items.Select(item =>
            {
                var dto = ObjectMapper.Map<BudgetOrganizationUnitLookupTableDto>(item);
                return dto;
            }).ToList();

            return orgDto;
        }

        // Update BudgetUse Detail
        public async Task UpdateAttachment(BinaryObjectInput input)
        {
            var attachment = await _binaryObjectManager.GetOrNullAsync(input.Id);

            attachment.BudgetUseId = input.BudgetUseId;
            attachment.ContentType = input.ContentType;
            attachment.Name = input.Name;
            attachment.Remark = input.Remark;
        }

        public async Task DeleteAttachment(string input)
        {
            await _binaryObjectManager.DeleteAsync(Guid.Parse(input));
        }

        public async Task<GetUserForEditOutput> GetUserForEdit(NullableIdDto<long> input)
        {
            //Getting all available roles
            var userRoleDtos = await _roleManager.Roles
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .ToArrayAsync();

            var allOrganizationUnits = await _organizationUnitRepository.GetAllListAsync();

            var output = new GetUserForEditOutput
            {
                Roles = userRoleDtos,
                AllOrganizationUnits = ObjectMapper.Map<List<OrganizationUnitDto>>(allOrganizationUnits),
                MemberedOrganizationUnits = new List<string>()
            };
        
            //Editing an existing user
            var user = await UserManager.GetUserByIdAsync(input.Id.Value);

            output.User = ObjectMapper.Map<UserEditDto>(user);
            output.ProfilePictureId = user.ProfilePictureId;

            foreach (var userRoleDto in userRoleDtos)
            {
                userRoleDto.IsAssigned = await UserManager.IsInRoleAsync(user, userRoleDto.RoleName);
            }

            var organizationUnits = await UserManager.GetOrganizationUnitsAsync(user);
            output.MemberedOrganizationUnits = organizationUnits.Select(ou => ou.Code).ToList();
            

            return output;
        }
    }
}