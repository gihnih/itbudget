namespace Base2.Tracker.Dtos
{
    public class GetBudgetYearForEditOutput
    {
        public CreateOrEditBudgetYearDto BudgetYear { get; set; }
    }
}