using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public class DescriptionTypesExcelExporter : EpPlusExcelExporterBase, IDescriptionTypesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DescriptionTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) : base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDescriptionTypeForViewDto> descriptionTypes)
        {
            return CreateExcelPackage(
                "DescriptionTypes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DescriptionTypes"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, 2, descriptionTypes,
                        _ => _.DescriptionType.Name
                        );
                });
        }
    }
}