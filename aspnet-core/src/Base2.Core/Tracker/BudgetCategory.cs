﻿namespace Base2.Tracker
{
    public enum BudgetCategory
    {
        Budgeted = 1,
        Unbudgeted = 2
    }
}