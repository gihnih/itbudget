﻿using Abp.Dependency;
using System.Threading.Tasks;

namespace Base2.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}