namespace Base2.Tracker.Dtos
{
    public class GetBudgetUseForEditOutput
    {
        public CreateOrEditBudgetUseDto BudgetUse { get; set; }

        public bool IsActualAmountUpdate { get; set; }
    }
}