namespace Base2.Tracker.Dtos
{
    public class GetGLCodeForEditOutput
    {
        public CreateOrEditGLCodeDto GLCode { get; set; }
    }
}