namespace Base2.Tracker.Dtos
{
    public class GetBudgetForEditOutput
    {
        public CreateOrEditBudgetDto Budget { get; set; }

        public string BudgetTypeName { get; set; }

        public string GLCodeCode { get; set; }

        public string DescriptionTypeName { get; set; }

        public string BudgetYearYear { get; set; }

        public string OrganizationUnitDisplayName { get; set; }
    }
}