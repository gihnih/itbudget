﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class UpdatetableBudgetUses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "BudgetRequests");

            migrationBuilder.AddColumn<long>(
                name: "UserApprovalId",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "UserRequestId",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserApprovalId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "UserRequestId",
                table: "BudgetUses");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "BudgetRequests",
                nullable: true);
        }
    }
}