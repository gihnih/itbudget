namespace Base2.MultiTenancy.Payments
{
    public abstract class ExecutePaymentResponse
    {
        public abstract string GetId();
    }
}