﻿using System.ComponentModel.DataAnnotations;

namespace Base2.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}