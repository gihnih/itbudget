using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class BudgetRequestDto : EntityDto
    {
        public int? BudgetId { get; set; }
        public BudgetDetailDto Budget { get; set; }

        public int? BudgetUseId { get; set; }
        //public virtual BudgetUseDto BudgetUse { get; set; }

        public decimal RequestAmount { get; set; }
        public decimal ActualAmount { get; set; }

        public bool OwnBudget { get; set; }

        public string RequestStatus { get; set; }
    }
}