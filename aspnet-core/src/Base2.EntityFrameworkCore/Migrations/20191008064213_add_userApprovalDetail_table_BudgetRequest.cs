﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Base2.Migrations
{
    public partial class add_userApprovalDetail_table_BudgetRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustodianApprovalId",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CustodianApprovalId1",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OwnerApprovalId",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "OwnerApprovalId1",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetRequests_CustodianApprovalId1",
                table: "BudgetRequests",
                column: "CustodianApprovalId1");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetRequests_OwnerApprovalId1",
                table: "BudgetRequests",
                column: "OwnerApprovalId1");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetRequests_AbpUsers_CustodianApprovalId1",
                table: "BudgetRequests",
                column: "CustodianApprovalId1",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetRequests_AbpUsers_OwnerApprovalId1",
                table: "BudgetRequests",
                column: "OwnerApprovalId1",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetRequests_AbpUsers_CustodianApprovalId1",
                table: "BudgetRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetRequests_AbpUsers_OwnerApprovalId1",
                table: "BudgetRequests");

            migrationBuilder.DropIndex(
                name: "IX_BudgetRequests_CustodianApprovalId1",
                table: "BudgetRequests");

            migrationBuilder.DropIndex(
                name: "IX_BudgetRequests_OwnerApprovalId1",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "CustodianApprovalId",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "CustodianApprovalId1",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "OwnerApprovalId",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "OwnerApprovalId1",
                table: "BudgetRequests");
        }
    }
}
