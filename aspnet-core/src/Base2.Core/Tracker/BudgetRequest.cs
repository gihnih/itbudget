using Abp.Domain.Entities.Auditing;
using Base2.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base2.Tracker
{
    [Table("BudgetRequests")]
    public class BudgetRequest : FullAuditedEntity
    {
        public virtual int? BudgetId { get; set; }
        public virtual Budget Budget { get; set; }

        public virtual int? BudgetUseId { get; set; }
        public virtual BudgetUse BudgetUse { get; set; }

        public virtual decimal RequestAmount { get; set; }

        public virtual decimal ActualAmount { get; set; }

        public virtual bool OwnBudget { get; set; }

        public RequestStatus RequestStatus { get; set; }

        public virtual DateTime? BudgetOwnerApproval { get; set; }

        public virtual RequestStatus? BudgetOwnerRequestStatus { get; set; }

        public virtual int? OwnerApprovalId { get; set; }

        public virtual User OwnerApproval { get; set; }

        public virtual DateTime? BudgetCustodianApproval { get; set; }

        public virtual RequestStatus? BudgetCustodianRequestStatus { get; set; }

        public virtual int? CustodianApprovalId { get; set; }
        public virtual User CustodianApproval { get; set; }

        //public string Status { get; set; }
    }
}