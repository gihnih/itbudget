﻿using Base2.EntityFrameworkCore;

namespace Base2.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly Base2DbContext _context;
        private readonly int _tenantId;

        public TestDataBuilder(Base2DbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            new TestOrganizationUnitsBuilder(_context, _tenantId).Create();
            new TestSubscriptionPaymentBuilder(_context, _tenantId).Create();

            _context.SaveChanges();
        }
    }
}