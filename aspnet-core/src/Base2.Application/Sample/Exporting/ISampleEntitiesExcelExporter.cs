﻿using System.Collections.Generic;
using Base2.Sample.Dtos;
using Base2.Dto;

namespace Base2.Sample.Exporting
{
    public interface ISampleEntitiesExcelExporter
    {
        FileDto ExportToFile(List<GetSampleEntityForViewDto> sampleEntities);
    }
}