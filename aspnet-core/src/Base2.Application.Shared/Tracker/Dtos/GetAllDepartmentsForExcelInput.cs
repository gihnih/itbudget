namespace Base2.Tracker.Dtos
{
    public class GetAllDepartmentsForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }
    }
}