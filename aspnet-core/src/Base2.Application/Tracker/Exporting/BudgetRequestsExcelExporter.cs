using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public class BudgetRequestsExcelExporter : EpPlusExcelExporterBase, IBudgetRequestsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BudgetRequestsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) :
    base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetBudgetRequestForViewDto> budgetRequests)
        {
            return CreateExcelPackage(
                "BudgetRequests.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("BudgetRequests"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        (L("Budget")) + L("Description"),
                        (L("BudgetUse")) + L("Description")
                        );

                    AddObjects(
                        sheet, 2, budgetRequests,
                        _ => _.BudgetDescription,
                        _ => _.BudgetUseDescription
                        );
                });
        }
    }
}