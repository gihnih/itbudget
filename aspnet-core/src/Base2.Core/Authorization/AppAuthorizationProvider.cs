﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Base2.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var sampleEntities = pages.CreateChildPermission(AppPermissions.Pages_SampleEntities, L("SampleEntities"));
            sampleEntities.CreateChildPermission(AppPermissions.Pages_SampleEntities_Create, L("CreateNewSampleEntity"));
            sampleEntities.CreateChildPermission(AppPermissions.Pages_SampleEntities_Edit, L("EditSampleEntity"));
            sampleEntities.CreateChildPermission(AppPermissions.Pages_SampleEntities_Delete, L("DeleteSampleEntity"));



            var dashboards = pages.CreateChildPermission(AppPermissions.Pages_Dashboards, L("Dashboard"));
            dashboards.CreateChildPermission(AppPermissions.Pages_Dashboards_ItDashboard, L("ITDashboard"));
            dashboards.CreateChildPermission(AppPermissions.Pages_Dashboards_MyDepartment, L("DepartmentDashboard"));

            var pendingActivity = pages.CreateChildPermission(AppPermissions.Pages_PendingActivity, L("PendingActivity"));
            pendingActivity.CreateChildPermission(AppPermissions.Pages_PendingActivity_ItCustodian, L("ITCustodian"));
            pendingActivity.CreateChildPermission(AppPermissions.Pages_PendingActivity_MyDepartment, L("MyDepartment"));

            var budgets = pages.CreateChildPermission(AppPermissions.Pages_Budgets, L("Budgets"));
            budgets.CreateChildPermission(AppPermissions.Pages_Budgets_Create, L("CreateNewBudget"));
            budgets.CreateChildPermission(AppPermissions.Pages_Budgets_Edit, L("EditBudget"));
            budgets.CreateChildPermission(AppPermissions.Pages_Budgets_Delete, L("DeleteBudget"));

            var myBudgets = pages.CreateChildPermission(AppPermissions.Pages_MyBudgets, L("MyBudgets"));
            myBudgets.CreateChildPermission(AppPermissions.Pages_MyBudgets_Cancel, L("CancelMyBudgets"));
            myBudgets.CreateChildPermission(AppPermissions.Pages_MyBudgets_View, L("ViewMyBudgets"));
            myBudgets.CreateChildPermission(AppPermissions.Pages_MyBudgets_Unbudgeted, L("Unbudgeted"));

            var budgetUses = pages.CreateChildPermission(AppPermissions.Pages_BudgetUses, L("BudgetUses"));
            budgetUses.CreateChildPermission(AppPermissions.Pages_BudgetUses_Create, L("CreateNewBudgetUtilize"));
            budgetUses.CreateChildPermission(AppPermissions.Pages_BudgetUses_Edit, L("EditBudgetUtilize"));
            budgetUses.CreateChildPermission(AppPermissions.Pages_BudgetUses_Delete, L("DeleteBudgetUtilize"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));
            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));
            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            var glCodes = administration.CreateChildPermission(AppPermissions.Pages_Administration_GLCodes, L("GLCodes"));
            glCodes.CreateChildPermission(AppPermissions.Pages_Administration_GLCodes_Create, L("CreateNewGLCode"));
            glCodes.CreateChildPermission(AppPermissions.Pages_Administration_GLCodes_Edit, L("EditGLCode"));
            glCodes.CreateChildPermission(AppPermissions.Pages_Administration_GLCodes_Delete, L("DeleteGLCode"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            //var budgetApprovers = pages.CreateChildPermission(AppPermissions.Pages_BudgetApprovers, L("BudgetApprovers"));
            //budgetApprovers.CreateChildPermission(AppPermissions.Pages_BudgetApprovers_Create, L("CreateNewBudgetApprover"));
            //budgetApprovers.CreateChildPermission(AppPermissions.Pages_BudgetApprovers_Edit, L("EditBudgetApprover"));
            //budgetApprovers.CreateChildPermission(AppPermissions.Pages_BudgetApprovers_Delete, L("DeleteBudgetApprover"));

            //var budgetRequests = pages.CreateChildPermission(AppPermissions.Pages_BudgetRequests, L("BudgetRequests"));
            //budgetRequests.CreateChildPermission(AppPermissions.Pages_BudgetRequests_Create, L("CreateNewBudgetRequest"));
            //budgetRequests.CreateChildPermission(AppPermissions.Pages_BudgetRequests_Edit, L("EditBudgetRequest"));
            //budgetRequests.CreateChildPermission(AppPermissions.Pages_BudgetRequests_Delete, L("DeleteBudgetRequest"));

            //var useStatuses = pages.CreateChildPermission(AppPermissions.Pages_UseStatuses, L("UseStatuses"));
            //useStatuses.CreateChildPermission(AppPermissions.Pages_UseStatuses_Create, L("CreateNewUseStatus"));
            //useStatuses.CreateChildPermission(AppPermissions.Pages_UseStatuses_Edit, L("EditUseStatus"));
            //useStatuses.CreateChildPermission(AppPermissions.Pages_UseStatuses_Delete, L("DeleteUseStatus"));

            //var budgetYears = pages.CreateChildPermission(AppPermissions.Pages_BudgetYears, L("BudgetYears"));
            //budgetYears.CreateChildPermission(AppPermissions.Pages_BudgetYears_Create, L("CreateNewBudgetYear"));
            //budgetYears.CreateChildPermission(AppPermissions.Pages_BudgetYears_Edit, L("EditBudgetYear"));
            //budgetYears.CreateChildPermission(AppPermissions.Pages_BudgetYears_Delete, L("DeleteBudgetYear"));

            //var descriptionTypes = pages.CreateChildPermission(AppPermissions.Pages_DescriptionTypes, L("DescriptionTypes"));
            //descriptionTypes.CreateChildPermission(AppPermissions.Pages_DescriptionTypes_Create, L("CreateNewDescriptionType"));
            //descriptionTypes.CreateChildPermission(AppPermissions.Pages_DescriptionTypes_Edit, L("EditDescriptionType"));
            //descriptionTypes.CreateChildPermission(AppPermissions.Pages_DescriptionTypes_Delete, L("DeleteDescriptionType"));

            //pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            //TENANT-SPECIFIC PERMISSIONS

            //pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, Base2Consts.LocalizationSourceName);
        }
    }
}