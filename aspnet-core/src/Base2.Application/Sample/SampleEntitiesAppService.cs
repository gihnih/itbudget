﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Base2.Sample.Exporting;
using Base2.Sample.Dtos;
using Base2.Dto;
using Abp.Application.Services.Dto;
using Base2.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Base2.Sample
{
	[AbpAuthorize(AppPermissions.Pages_SampleEntities)]
    public class SampleEntitiesAppService : Base2AppServiceBase, ISampleEntitiesAppService
    {
		 private readonly IRepository<SampleEntity> _sampleEntityRepository;
		 private readonly ISampleEntitiesExcelExporter _sampleEntitiesExcelExporter;
		 

		  public SampleEntitiesAppService(IRepository<SampleEntity> sampleEntityRepository, ISampleEntitiesExcelExporter sampleEntitiesExcelExporter ) 
		  {
			_sampleEntityRepository = sampleEntityRepository;
			_sampleEntitiesExcelExporter = sampleEntitiesExcelExporter;
			
		  }

		 public async Task<PagedResultDto<GetSampleEntityForViewDto>> GetAll(GetAllSampleEntitiesInput input)
         {
			
			var filteredSampleEntities = _sampleEntityRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.FieldRequired.Contains(input.Filter) || e.Field.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title == input.TitleFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description == input.DescriptionFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.FieldRequiredFilter),  e => e.FieldRequired == input.FieldRequiredFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.FieldFilter),  e => e.Field == input.FieldFilter)
						.WhereIf(input.MinFieldDecimalNullFilter != null, e => e.FieldDecimalNull >= input.MinFieldDecimalNullFilter)
						.WhereIf(input.MaxFieldDecimalNullFilter != null, e => e.FieldDecimalNull <= input.MaxFieldDecimalNullFilter)
						.WhereIf(input.MinFieldDoubleNullFilter != null, e => e.FieldDoubleNull >= input.MinFieldDoubleNullFilter)
						.WhereIf(input.MaxFieldDoubleNullFilter != null, e => e.FieldDoubleNull <= input.MaxFieldDoubleNullFilter);

			var pagedAndFilteredSampleEntities = filteredSampleEntities
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var sampleEntities = from o in pagedAndFilteredSampleEntities
                         select new GetSampleEntityForViewDto() {
							SampleEntity = new SampleEntityDto
							{
                                Title = o.Title,
                                Description = o.Description,
                                FieldRequired = o.FieldRequired,
                                Field = o.Field,
                                FieldDecimalNull = o.FieldDecimalNull,
                                FieldDoubleNull = o.FieldDoubleNull,
                                Id = o.Id
							}
						};

            var totalCount = await filteredSampleEntities.CountAsync();

            return new PagedResultDto<GetSampleEntityForViewDto>(
                totalCount,
                await sampleEntities.ToListAsync()
            );
         }
		 
		 public async Task<GetSampleEntityForViewDto> GetSampleEntityForView(int id)
         {
            var sampleEntity = await _sampleEntityRepository.GetAsync(id);

            var output = new GetSampleEntityForViewDto { SampleEntity = ObjectMapper.Map<SampleEntityDto>(sampleEntity) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_SampleEntities_Edit)]
		 public async Task<GetSampleEntityForEditOutput> GetSampleEntityForEdit(EntityDto input)
         {
            var sampleEntity = await _sampleEntityRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetSampleEntityForEditOutput {SampleEntity = ObjectMapper.Map<CreateOrEditSampleEntityDto>(sampleEntity)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditSampleEntityDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_SampleEntities_Create)]
		 protected virtual async Task Create(CreateOrEditSampleEntityDto input)
         {
            var sampleEntity = ObjectMapper.Map<SampleEntity>(input);

			
			if (AbpSession.TenantId != null)
			{
				sampleEntity.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _sampleEntityRepository.InsertAsync(sampleEntity);
         }

		 [AbpAuthorize(AppPermissions.Pages_SampleEntities_Edit)]
		 protected virtual async Task Update(CreateOrEditSampleEntityDto input)
         {
            var sampleEntity = await _sampleEntityRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, sampleEntity);
         }

		 [AbpAuthorize(AppPermissions.Pages_SampleEntities_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _sampleEntityRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetSampleEntitiesToExcel(GetAllSampleEntitiesForExcelInput input)
         {
			
			var filteredSampleEntities = _sampleEntityRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Title.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.FieldRequired.Contains(input.Filter) || e.Field.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.TitleFilter),  e => e.Title == input.TitleFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter),  e => e.Description == input.DescriptionFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.FieldRequiredFilter),  e => e.FieldRequired == input.FieldRequiredFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.FieldFilter),  e => e.Field == input.FieldFilter)
						.WhereIf(input.MinFieldDecimalNullFilter != null, e => e.FieldDecimalNull >= input.MinFieldDecimalNullFilter)
						.WhereIf(input.MaxFieldDecimalNullFilter != null, e => e.FieldDecimalNull <= input.MaxFieldDecimalNullFilter)
						.WhereIf(input.MinFieldDoubleNullFilter != null, e => e.FieldDoubleNull >= input.MinFieldDoubleNullFilter)
						.WhereIf(input.MaxFieldDoubleNullFilter != null, e => e.FieldDoubleNull <= input.MaxFieldDoubleNullFilter);

			var query = (from o in filteredSampleEntities
                         select new GetSampleEntityForViewDto() { 
							SampleEntity = new SampleEntityDto
							{
                                Title = o.Title,
                                Description = o.Description,
                                FieldRequired = o.FieldRequired,
                                Field = o.Field,
                                FieldDecimalNull = o.FieldDecimalNull,
                                FieldDoubleNull = o.FieldDoubleNull,
                                Id = o.Id
							}
						 });


            var sampleEntityListDtos = await query.ToListAsync();

            return _sampleEntitiesExcelExporter.ExportToFile(sampleEntityListDtos);
         }


    }
}