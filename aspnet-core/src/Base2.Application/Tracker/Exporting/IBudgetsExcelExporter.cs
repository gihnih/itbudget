using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public interface IBudgetsExcelExporter
    {
        FileDto ExportToFile(List<GetBudgetForViewDto> budgets);

        FileDto ExportUnbudgetedToFile(List<GetBudgetForViewDto> budgets);
    }
}