namespace Base2.MultiTenancy
{
    public enum EndSubscriptionResult
    {
        TenantSetInActive,
        AssignedToAnotherEdition
    }
}