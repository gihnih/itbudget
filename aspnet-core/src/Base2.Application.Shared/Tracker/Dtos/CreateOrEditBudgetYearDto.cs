using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class CreateOrEditBudgetYearDto : EntityDto<int?>
    {
        public int Year { get; set; }
    }
}