﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class someentitychangetoenum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Budgets_BudgetTypes_BudgetTypeId",
                table: "Budgets");

            migrationBuilder.DropForeignKey(
                name: "FK_Budgets_BudgetYears_BudgetYearId",
                table: "Budgets");

            migrationBuilder.DropForeignKey(
                name: "FK_Budgets_DescriptionTypes_DescriptionTypeId",
                table: "Budgets");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetUses_UseStatuses_UseStatusId",
                table: "BudgetUses");

            migrationBuilder.DropTable(
                name: "BudgetTypes");

            migrationBuilder.DropTable(
                name: "BudgetYears");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "DescriptionTypes");

            migrationBuilder.DropTable(
                name: "UseStatuses");

            migrationBuilder.DropIndex(
                name: "IX_BudgetUses_UseStatusId",
                table: "BudgetUses");

            migrationBuilder.DropIndex(
                name: "IX_Budgets_BudgetTypeId",
                table: "Budgets");

            migrationBuilder.DropIndex(
                name: "IX_Budgets_BudgetYearId",
                table: "Budgets");

            migrationBuilder.DropIndex(
                name: "IX_Budgets_DescriptionTypeId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "BudgetYearId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "Category",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "DescriptionTypeId",
                table: "Budgets");

            migrationBuilder.RenameColumn(
                name: "BudgetTypeId",
                table: "Budgets",
                newName: "BudgetYear");

            migrationBuilder.AddColumn<int>(
                name: "UseStatus",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BudgetCategory",
                table: "Budgets",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BudgetType",
                table: "Budgets",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DescriptionType",
                table: "Budgets",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UseStatus",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "BudgetCategory",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "BudgetType",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "DescriptionType",
                table: "Budgets");

            migrationBuilder.RenameColumn(
                name: "BudgetYear",
                table: "Budgets",
                newName: "BudgetTypeId");

            migrationBuilder.AddColumn<int>(
                name: "BudgetYearId",
                table: "Budgets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Category",
                table: "Budgets",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DescriptionTypeId",
                table: "Budgets",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BudgetTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BudgetYears",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetYears", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DescriptionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DescriptionTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UseStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UseStatuses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BudgetUses_UseStatusId",
                table: "BudgetUses",
                column: "UseStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_BudgetTypeId",
                table: "Budgets",
                column: "BudgetTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_BudgetYearId",
                table: "Budgets",
                column: "BudgetYearId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_DescriptionTypeId",
                table: "Budgets",
                column: "DescriptionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Budgets_BudgetTypes_BudgetTypeId",
                table: "Budgets",
                column: "BudgetTypeId",
                principalTable: "BudgetTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Budgets_BudgetYears_BudgetYearId",
                table: "Budgets",
                column: "BudgetYearId",
                principalTable: "BudgetYears",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Budgets_DescriptionTypes_DescriptionTypeId",
                table: "Budgets",
                column: "DescriptionTypeId",
                principalTable: "DescriptionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetUses_UseStatuses_UseStatusId",
                table: "BudgetUses",
                column: "UseStatusId",
                principalTable: "UseStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}