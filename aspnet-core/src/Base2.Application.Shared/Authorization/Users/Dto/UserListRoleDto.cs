﻿namespace Base2.Authorization.Users.Dto
{
    public class UserListRoleDto
    {
        public int RoleId { get; set; }

        public string RoleName { get; set; }
    }
}