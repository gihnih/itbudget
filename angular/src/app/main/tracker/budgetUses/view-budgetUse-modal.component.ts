import {Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {
    GetBudgetUseForViewDto,
    BudgetUseDto,
    GetBudgetForViewDto,
    BudgetsServiceProxy, BudgetOrganizationUnitLookupTableDto, BudgetUsesServiceProxy, UserListDto
} from '@shared/service-proxies/service-proxies';
import {AppComponentBase} from '@shared/common/app-component-base';
import {AppConsts} from '@shared/AppConsts';

@Component({
    selector: 'viewBudgetUseModal',
    templateUrl: './view-budgetUse-modal.component.html'
})
export class ViewBudgetUseModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetBudgetUseForViewDto = new GetBudgetUseForViewDto();
    creatorUser: UserListDto = new UserListDto;
    budget: GetBudgetForViewDto;
    budgetUse: GetBudgetUseForViewDto;

// attachment
    getUrl: string;

    constructor(
        injector: Injector,
        private _budgetServiceProxy: BudgetsServiceProxy,
        private _budgetUseServiceProxy: BudgetUsesServiceProxy
    ) {
        super(injector);
        this.item = new GetBudgetUseForViewDto();
        this.item.organizationUnit = new BudgetOrganizationUnitLookupTableDto();
        this.item.binaryObjects = [];
        this.item.budgetRequests = [];
        this.getUrl = AppConsts.remoteServiceBaseUrl + '/Upload/GetUploadedObject';
    }

    show(id: number): void {
        console.log(id);

        this._budgetUseServiceProxy.getBudgetUseDetailForView(id).subscribe(result => {
            this.item = result.budgetUse;
            this.creatorUser = result.creatorUser;
            this.active = true;
            this.modal.show();
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.budget = null;
    }
}
