﻿import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetSampleEntityForViewDto, SampleEntityDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewSampleEntityModal',
    templateUrl: './view-sampleEntity-modal.component.html'
})
export class ViewSampleEntityModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetSampleEntityForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetSampleEntityForViewDto();
        this.item.sampleEntity = new SampleEntityDto();
    }

    show(item: GetSampleEntityForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
