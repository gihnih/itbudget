using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace Base2.EntityFrameworkCore
{
    public static class Base2DbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<Base2DbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<Base2DbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}