using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class GLCodeDto : EntityDto
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }
}