﻿namespace Base2.Tracker.Dtos
{
    public class GetAllDashboardInput
    {
        public int Year { get; set; }
    }
}