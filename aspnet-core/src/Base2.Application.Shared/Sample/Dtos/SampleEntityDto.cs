﻿
using System;
using Abp.Application.Services.Dto;

namespace Base2.Sample.Dtos
{
    public class SampleEntityDto : EntityDto
    {
		public string Title { get; set; }

		public string Description { get; set; }

		public string FieldRequired { get; set; }

		public string Field { get; set; }

		public decimal? FieldDecimalNull { get; set; }

		public double? FieldDoubleNull { get; set; }

    }
}