﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Base2.Migrations
{
    public partial class Regenerated_SampleEntity8769 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BudgetCategoryEnum",
                table: "SampleEntities");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BudgetCategoryEnum",
                table: "SampleEntities",
                nullable: false,
                defaultValue: 0);
        }
    }
}
