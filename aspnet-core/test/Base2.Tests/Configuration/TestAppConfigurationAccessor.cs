﻿using Abp.Dependency;
using Abp.Reflection.Extensions;
using Base2.Configuration;
using Microsoft.Extensions.Configuration;

namespace Base2.Tests.Configuration
{
    public class TestAppConfigurationAccessor : IAppConfigurationAccessor, ISingletonDependency
    {
        public IConfigurationRoot Configuration { get; }

        public TestAppConfigurationAccessor()
        {
            Configuration = AppConfigurations.Get(
                typeof(Base2TestModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }
    }
}