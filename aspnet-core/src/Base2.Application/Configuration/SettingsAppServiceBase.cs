﻿using Abp.Net.Mail;
using Base2.Configuration.Host.Dto;
using System.Threading.Tasks;

namespace Base2.Configuration
{
    public abstract class SettingsAppServiceBase : Base2AppServiceBase
    {
        private readonly IEmailSender _emailSender;

        protected SettingsAppServiceBase(
            IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        #region Send Test Email

        public async Task SendTestEmail(SendTestEmailInput input)
        {
            await _emailSender.SendAsync(
                input.EmailAddress,
                L("TestEmail_Subject"),
                L("TestEmail_Body")
            );
        }

        #endregion Send Test Email
    }
}