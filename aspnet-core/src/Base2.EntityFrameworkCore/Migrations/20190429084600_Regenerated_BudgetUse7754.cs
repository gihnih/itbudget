﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class Regenerated_BudgetUse7754 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BudgetId",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetUses_BudgetId",
                table: "BudgetUses",
                column: "BudgetId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetUses_Budgets_BudgetId",
                table: "BudgetUses",
                column: "BudgetId",
                principalTable: "Budgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetUses_Budgets_BudgetId",
                table: "BudgetUses");

            migrationBuilder.DropIndex(
                name: "IX_BudgetUses_BudgetId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "BudgetId",
                table: "BudgetUses");
        }
    }
}