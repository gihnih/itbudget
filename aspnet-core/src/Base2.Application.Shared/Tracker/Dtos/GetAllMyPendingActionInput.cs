using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class GetAllMyPendingActionInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}