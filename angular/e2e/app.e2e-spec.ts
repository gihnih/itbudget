﻿import { Base2Page } from './app.po';

describe('abp-zero-template App', function () {
    let page: Base2Page;

    beforeEach(() => {
        page = new Base2Page();
    });

    it('should display message saying app works', () => {
        page.navigateTo();
        page.getCopyright().then(value => {
            expect(value).toEqual(new Date().getFullYear() + ' © Base2.');
        });
    });
});
