using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Auditing.Dto;
using Base2.Dto;
using System.Threading.Tasks;

namespace Base2.Auditing
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<PagedResultDto<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input);

        Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input);
    }
}