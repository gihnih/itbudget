﻿using Abp.Domain.Services;

namespace Base2
{
    public abstract class Base2DomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected Base2DomainServiceBase()
        {
            LocalizationSourceName = Base2Consts.LocalizationSourceName;
        }
    }
}