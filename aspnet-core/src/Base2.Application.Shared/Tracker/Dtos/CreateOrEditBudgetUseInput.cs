using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Base2.Tracker.Dtos
{
    public class CreateOrEditBudgetUseInput : FullAuditedEntityDto<int?>
    {
        public string Description { get; set; }

        public decimal RequestAmount { get; set; }

        public decimal ActualAmount { get; set; }

        public string PaperStatus { get; set; }

        public string RequestStatus { get; set; }

        public long OrganizationUnitId { get; set; }

        public bool IsActualAmountUpdate { get; set; }

        public ICollection<BudgetRequestDto> BudgetRequests { get; set; }

        public BudgetOrganizationUnitLookupTableDto OrganizationUnit { get; set; }
    }
}