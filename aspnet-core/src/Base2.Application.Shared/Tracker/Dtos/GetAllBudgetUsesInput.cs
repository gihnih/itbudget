using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetUsesInput : PagedAndSortedResultRequestDto
    {
        public string DescriptionFilter { get; set; }

        public string PaperStatus { get; set; }

        public string RequestStatus { get; set; }

        public string Year { get; set; }
    }
}