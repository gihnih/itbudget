namespace Base2.Tracker.Dtos
{
    public class GetAllUseStatusesForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }
    }
}