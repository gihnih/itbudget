using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public class GLCodesExcelExporter : EpPlusExcelExporterBase, IGLCodesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public GLCodesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) : base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetGLCodeForViewDto> glCodes)
        {
            return CreateExcelPackage(
                "GLCodes.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("GLCodes"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Code"),
                        L("Description")
                        );

                    AddObjects(
                        sheet, 2, glCodes,
                        _ => _.GLCode.Code,
                        _ => _.GLCode.Description
                        );
                });
        }
    }
}