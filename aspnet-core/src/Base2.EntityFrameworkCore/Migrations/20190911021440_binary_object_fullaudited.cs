﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Base2.Migrations
{
    public partial class binary_object_fullaudited : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "AppBinaryObjects",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "AppBinaryObjects",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "AppBinaryObjects",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "AppBinaryObjects",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "AppBinaryObjects",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "AppBinaryObjects",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "AppBinaryObjects",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "AppBinaryObjects");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "AppBinaryObjects");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "AppBinaryObjects");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "AppBinaryObjects");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "AppBinaryObjects");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "AppBinaryObjects");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "AppBinaryObjects");
        }
    }
}
