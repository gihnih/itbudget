import {Component, Injector, ViewEncapsulation, ViewChild} from '@angular/core';
import {BudgetUsesServiceProxy, BudgetUseDto, BudgetsServiceProxy} from '@shared/service-proxies/service-proxies';
import {AppComponentBase} from '@shared/common/app-component-base';
import {CreateOrEditBudgetUseModalComponent} from './create-or-edit-budgetUse-modal.component';
import {ViewBudgetUseModalComponent} from './view-budgetUse-modal.component';
import {appModuleAnimation} from '@shared/animations/routerTransition';
import {DataTable} from 'primeng/components/datatable/datatable';
import {Paginator} from 'primeng/components/paginator/paginator';
import {LazyLoadEvent} from 'primeng/components/common/lazyloadevent';
import {FileDownloadService} from '@shared/utils/file-download.service';

@Component({
    templateUrl: './budgetUses.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class BudgetUsesComponent extends AppComponentBase {

    @ViewChild('createOrEditBudgetUseModal') createOrEditBudgetUseModal: CreateOrEditBudgetUseModalComponent;
    @ViewChild('viewBudgetUseModalComponent') viewBudgetUseModal: ViewBudgetUseModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    // Filter
    descriptionFilter = '';
    paperStatusFilter = '';
    requestStatusFilter = '';
    yearFilter = '';

    // data init
    paperStatus;
    requestStatus;

    advancedFiltersAreShown = false;
    status: string;

    // html flag
    department = true;

    ssss: any = '';


    constructor(
        injector: Injector,
        private _budgetUsesServiceProxy: BudgetUsesServiceProxy,
        private _budgetServiceProxy: BudgetsServiceProxy,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);
    }
    getInitData() {
        this._budgetServiceProxy.getPaperStatus().subscribe(result => {
            this.paperStatus = result;
        });
        this._budgetServiceProxy.getRequestStatus().subscribe(result => {
            this.requestStatus = result;
        });
    }

    getBudgetUses(event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        this._budgetUsesServiceProxy.getAll(
            this.descriptionFilter,
            this.paperStatusFilter,
            this.requestStatusFilter,
            this.yearFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
            console.log("get budget use");
            console.log(result);
            this.ssss = result;
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createBudgetUse(): void {
        this.createOrEditBudgetUseModal.show();
    }

    deleteBudgetUse(id: number): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._budgetUsesServiceProxy.delete(id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._budgetUsesServiceProxy.getBudgetUsesMyDepartmentToExcel(
            this.descriptionFilter,
            this.paperStatusFilter,
            this.requestStatusFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
