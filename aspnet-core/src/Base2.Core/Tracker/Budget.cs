using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base2.Tracker
{
    [Table("Budgets")]
    public class Budget : FullAuditedEntity
    {
        public virtual string Description { get; set; }

        public virtual decimal BudgetAmount { get; set; }

        public virtual decimal UtilizedBudget { get; set; }

        public virtual decimal EstimatedBalance { get; set; }

        public virtual string Remark { get; set; }

        public virtual int BudgetYear { get; set; }

        public virtual long? OrganizationUnitId { get; set; }

        public virtual bool IsBudgeted { get; set; }

        public BudgetCategory BudgetCategory { get; set; }

        public BudgetType BudgetType { get; set; }

        public DescriptionType DescriptionType { get; set; }

        [ForeignKey("OrganizationUnitId")]
        public OrganizationUnit OrganizationUnit { get; set; }

        public ICollection<BudgetRequest> BudgetRequests { get; set; }

        public virtual int? GLCodeId { get; set; }

        [ForeignKey("GLCodeId")]
        public GLCode GLCode { get; set; }
    }
}