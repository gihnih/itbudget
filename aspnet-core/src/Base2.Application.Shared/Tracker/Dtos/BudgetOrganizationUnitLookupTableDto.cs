namespace Base2.Tracker.Dtos
{
    public class BudgetOrganizationUnitLookupTableDto
    {
        public long Id { get; set; }

        public string DisplayName { get; set; }
    }
}