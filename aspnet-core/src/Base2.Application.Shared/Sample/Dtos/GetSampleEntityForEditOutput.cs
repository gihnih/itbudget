﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Base2.Sample.Dtos
{
    public class GetSampleEntityForEditOutput
    {
		public CreateOrEditSampleEntityDto SampleEntity { get; set; }


    }
}