﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Base2.Authorization;
using Base2.Authorization.Users;
using Base2.MultiTenancy;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace Base2.Tests.Authorization.Users
{
    public class UserAppService_Unlock_Tests : UserAppServiceTestBase
    {
        private readonly UserManager _userManager;
        private readonly LogInManager _loginManager;

        public UserAppService_Unlock_Tests()
        {
            _userManager = Resolve<UserManager>();
            _loginManager = Resolve<LogInManager>();

            CreateTestUsers();
        }

        [Fact]
        public async Task Should_Unlock_User()
        {
            //Arrange

            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);
            var user = await GetUserByUserNameAsync("jnash");

            //Pre conditions
            (await _userManager.IsLockedOutAsync(user)).ShouldBeFalse();
            user.IsLockoutEnabled.ShouldBeTrue();

            //Try wrong password until lockout
            AbpLoginResultType loginResultType;
            do
            {
                loginResultType = (await _loginManager.LoginAsync(user.UserName, "wrong-password", Tenant.DefaultTenantName)).Result;
            } while (loginResultType != AbpLoginResultType.LockedOut);

            (await _userManager.IsLockedOutAsync(await GetUserByUserNameAsync("jnash"))).ShouldBeTrue();

            //Act

            await UserAppService.UnlockUser(new EntityDto<long>(user.Id));

            //Assert

            (await _loginManager.LoginAsync(user.UserName, "wrong-password", Tenant.DefaultTenantName)).Result.ShouldBe(AbpLoginResultType.InvalidPassword);
        }
    }
}