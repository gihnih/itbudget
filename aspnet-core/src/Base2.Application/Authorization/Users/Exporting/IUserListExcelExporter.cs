using Base2.Authorization.Users.Dto;
using Base2.Dto;
using System.Collections.Generic;

namespace Base2.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}