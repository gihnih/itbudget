namespace Base2.Tracker.Dtos
{
    public class GetDescriptionTypeForEditOutput
    {
        public CreateOrEditDescriptionTypeDto DescriptionType { get; set; }
    }
}