﻿using Abp.Organizations;
using Base2.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Base2.Migrations.Seed.Host
{
    public class DefaultOrganizationUnitCreator
    {
        private long id;
        public static List<OrganizationUnit> InitialOrganizationUnit => GetInitialOrganizationUnit();

        private readonly Base2DbContext _context;

        private static List<OrganizationUnit> GetInitialOrganizationUnit()
        {
            return new List<OrganizationUnit>
            {
                
            };
        }

        public DefaultOrganizationUnitCreator(Base2DbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateOrganizationUnit();
        }

        private void CreateOrganizationUnit()
        {
            foreach (var unit in InitialOrganizationUnit)
            {
                AddOrganizationUnitIfNotExists(unit);
            }
        }

        private void AddOrganizationUnitIfNotExists(OrganizationUnit organizationUnit)
        {
            if (_context.OrganizationUnits.IgnoreQueryFilters().Any(l => l.DisplayName == organizationUnit.DisplayName))
            {
                return;
            }

            _context.OrganizationUnits.Add(organizationUnit);
            _context.SaveChanges();
        }
    }
}