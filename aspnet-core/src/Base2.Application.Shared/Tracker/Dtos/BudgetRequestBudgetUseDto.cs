using Abp.Application.Services.Dto;
using Base2.Authorization.Users.Dto;
using System;

namespace Base2.Tracker.Dtos
{
    public class BudgetRequestBudgetUseDto : EntityDto
    {
        public int? BudgetId { get; set; }

        public int? BudgetUseId { get; set; }
        //public virtual BudgetUseDto BudgetUse { get; set; }

        public decimal RequestAmount { get; set; }

        public decimal ActualAmount { get; set; }

        public int CreatorUserId { get; set; }

        public UserListDto User { get; set; }

        public BudgetUse2Dto BudgetUse { get; set; }

        public string RequestStatus { get; set; }

        public DateTime BudgetOwnerApproval { get; set; }

        public string BudgetOwnerRequestStatus { get; set; }

        public DateTime BudgetCustodianApproval { get; set; }

        public string BudgetCustodianRequestStatus { get; set; }

        public int? CustodianApprovalId { get; set; }
        public UserListDto CustodianApproval { get; set; }

        public int? OwnerApprovalId { get; set; }
        public UserListDto OwnerApproval { get; set; }
    }
}