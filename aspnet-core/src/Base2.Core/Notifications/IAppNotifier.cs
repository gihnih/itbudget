﻿using Abp;
using Abp.Notifications;
using Base2.Authorization.Users;
using Base2.MultiTenancy;
using System.Threading.Tasks;

namespace Base2.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task NewTenantRegisteredAsync(Tenant tenant);

        Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info);
    }
}