namespace Base2.MultiTenancy.Payments.Dto
{
    public class GetSubscriptionPaymentInput
    {
        public long Id { get; set; }
    }
}