﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.MultiTenancy.Dto;
using Base2.MultiTenancy.Payments.Dto;
using System.Threading.Tasks;

namespace Base2.MultiTenancy.Payments
{
    public interface IPaymentAppService : IApplicationService
    {
        Task<PaymentInfoDto> GetPaymentInfo(PaymentInfoInput input);

        Task<CreatePaymentResponse> CreatePayment(CreatePaymentDto input);

        Task<ExecutePaymentResponse> ExecutePayment(ExecutePaymentDto input);

        Task<PagedResultDto<SubscriptionPaymentListDto>> GetPaymentHistory(GetPaymentHistoryInput input);
    }
}