import {Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {finalize} from 'rxjs/operators';
import {BudgetRequestsServiceProxy, CreateOrEditBudgetRequestDto} from '@shared/service-proxies/service-proxies';
import {AppComponentBase} from '@shared/common/app-component-base';
import * as moment from 'moment';
import {BudgetUseLookupTableModalComponent} from './budgetUse-lookup-table-modal.component';
import {BudgetLookupTableModalComponent} from '@app/main/tracker/budgetUses/budget-lookup-table-modal.component';


@Component({
    selector: 'createOrEditBudgetRequestModal',
    templateUrl: './create-or-edit-budgetRequest-modal.component.html'
})
export class CreateOrEditBudgetRequestModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('budgetLookupTableModal') budgetLookupTableModal: BudgetLookupTableModalComponent;
    @ViewChild('budgetUseLookupTableModal') budgetUseLookupTableModal: BudgetUseLookupTableModalComponent;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    budgetRequest: CreateOrEditBudgetRequestDto = new CreateOrEditBudgetRequestDto();

    budgetDescription = '';
    budgetUseDescription = '';


    constructor(
        injector: Injector,
        private _budgetRequestsServiceProxy: BudgetRequestsServiceProxy
    ) {
        super(injector);
    }

    show(budgetRequestId?: number): void {

        if (!budgetRequestId) {
            this.budgetRequest = new CreateOrEditBudgetRequestDto();
            this.budgetRequest.id = budgetRequestId;
            this.budgetDescription = '';
            this.budgetUseDescription = '';

            this.active = true;
            this.modal.show();
        } else {
            this._budgetRequestsServiceProxy.getBudgetRequestForEdit(budgetRequestId).subscribe(result => {
                this.budgetRequest = result.budgetRequest;

                this.budgetDescription = result.budgetDescription;
                this.budgetUseDescription = result.budgetUseDescription;

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        this.saving = true;


        this._budgetRequestsServiceProxy.createOrEdit(this.budgetRequest)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    openSelectBudgetModal() {
        this.budgetLookupTableModal.id = this.budgetRequest.budgetId;
        this.budgetLookupTableModal.displayName = this.budgetDescription;
        this.budgetLookupTableModal.show();
    }

    openSelectBudgetUseModal() {
        this.budgetUseLookupTableModal.id = this.budgetRequest.budgetUseId;
        this.budgetUseLookupTableModal.displayName = this.budgetUseDescription;
        this.budgetUseLookupTableModal.show();
    }


    setBudgetIdNull() {
        this.budgetRequest.budgetId = null;
        this.budgetDescription = '';
    }

    setBudgetUseIdNull() {
        this.budgetRequest.budgetUseId = null;
        this.budgetUseDescription = '';
    }


    getNewBudgetId() {
        this.budgetRequest.budgetId = this.budgetLookupTableModal.id;
        this.budgetDescription = this.budgetLookupTableModal.displayName;
    }

    getNewBudgetUseId() {
        this.budgetRequest.budgetUseId = this.budgetUseLookupTableModal.id;
        this.budgetUseDescription = this.budgetUseLookupTableModal.displayName;
    }


    close(): void {

        this.active = false;
        this.modal.hide();
    }
}
