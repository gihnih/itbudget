using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base2.Tracker
{
    [Table("GLCodes")]
    public class GLCode : Entity
    {
        public virtual string Code { get; set; }

        public virtual string Description { get; set; }
    }
}