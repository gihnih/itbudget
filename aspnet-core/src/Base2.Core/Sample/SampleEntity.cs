﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;

namespace Base2.Sample
{
	[Table("SampleEntities")]
    public class SampleEntity : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		[Required]
		[StringLength(SampleEntityConsts.MaxTitleLength, MinimumLength = SampleEntityConsts.MinTitleLength)]
		public virtual string Title { get; set; }
		
		public virtual string Description { get; set; }
		
		[Required]
		public virtual string FieldRequired { get; set; }
		
		public virtual string Field { get; set; }
		
		public virtual decimal? FieldDecimalNull { get; set; }
		
		public virtual double? FieldDoubleNull { get; set; }
		

    }
}