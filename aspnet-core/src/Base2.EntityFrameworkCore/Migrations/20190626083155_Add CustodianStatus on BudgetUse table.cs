﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class AddCustodianStatusonBudgetUsetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustodianStatus",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "BudgetRequests",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustodianStatus",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "BudgetRequests");
        }
    }
}