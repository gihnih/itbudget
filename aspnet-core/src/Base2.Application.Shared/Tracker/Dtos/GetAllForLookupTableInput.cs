using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Base2.Tracker.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string Category { get; set; }

        public List<long> OrganizationUnitId { get; set; }
    }
}