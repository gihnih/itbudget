namespace Base2.Tracker.Dtos
{
    public class BudgetDepartmentLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}