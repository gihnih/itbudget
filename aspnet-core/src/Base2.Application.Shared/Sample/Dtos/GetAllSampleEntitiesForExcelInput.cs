﻿using Abp.Application.Services.Dto;
using System;

namespace Base2.Sample.Dtos
{
    public class GetAllSampleEntitiesForExcelInput
    {
		public string Filter { get; set; }

		public string TitleFilter { get; set; }

		public string DescriptionFilter { get; set; }

		public string FieldRequiredFilter { get; set; }

		public string FieldFilter { get; set; }

		public decimal? MaxFieldDecimalNullFilter { get; set; }
		public decimal? MinFieldDecimalNullFilter { get; set; }

		public double? MaxFieldDoubleNullFilter { get; set; }
		public double? MinFieldDoubleNullFilter { get; set; }



    }
}