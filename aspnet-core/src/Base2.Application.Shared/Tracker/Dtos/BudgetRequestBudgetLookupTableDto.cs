namespace Base2.Tracker.Dtos
{
    public class BudgetRequestBudgetLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}