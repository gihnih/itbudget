﻿using Base2.Sample;
using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Base2.Authorization.Roles;
using Base2.Authorization.Users;
using Base2.Editions;
using Base2.MultiTenancy;
using Base2.MultiTenancy.Accounting;
using Base2.MultiTenancy.Payments;
using Base2.Storage;
using Base2.Tracker;
using Microsoft.EntityFrameworkCore;

namespace Base2.EntityFrameworkCore
{
    public class Base2DbContext : AbpZeroDbContext<Tenant, Role, User, Base2DbContext>, IAbpPersistedGrantDbContext
    {
        public virtual DbSet<SampleEntity> SampleEntities { get; set; }

        public virtual DbSet<BudgetApprover> BudgetApprovers { get; set; }

        public virtual DbSet<BudgetRequest> BudgetRequests { get; set; }

        public virtual DbSet<BudgetUse> BudgetUses { get; set; }

        public virtual DbSet<Budget> Budgets { get; set; }

        public virtual DbSet<GLCode> GLCodes { get; set; }

        //public virtual DbSet<BudgetType> BudgetTypes { get; set; }

        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public Base2DbContext(DbContextOptions<Base2DbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

           
            modelBuilder.Entity<SampleEntity>(s =>
            {
                s.HasIndex(e => new { e.TenantId });
            });
 modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { e.PaymentId, e.Gateway });
            });

            modelBuilder.Entity<BudgetRequest>()
                .HasKey(r => new
                {
                    r.BudgetId,
                    r.BudgetUseId
                });

            modelBuilder.Entity<BudgetRequest>()
               .HasOne(pt => pt.Budget)
               .WithMany(p => p.BudgetRequests)
               .HasForeignKey(pt => pt.BudgetId);

            modelBuilder.Entity<BudgetRequest>()
               .HasOne(pt => pt.BudgetUse)
               .WithMany(p => p.BudgetRequests)
               .HasForeignKey(pt => pt.BudgetUseId);

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}