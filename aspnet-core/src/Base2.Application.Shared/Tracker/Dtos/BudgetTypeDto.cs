using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class BudgetTypeDto : EntityDto
    {
        public string Name { get; set; }
    }
}