using Abp.Application.Services;
using Base2.Editions.Dto;
using Base2.MultiTenancy.Dto;
using System.Threading.Tasks;

namespace Base2.MultiTenancy
{
    public interface ITenantRegistrationAppService : IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}