﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class Add_OrganizationUnit_to_table_budgetUse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "OrganizationUnitId",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetUses_OrganizationUnitId",
                table: "BudgetUses",
                column: "OrganizationUnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetUses_AbpOrganizationUnits_OrganizationUnitId",
                table: "BudgetUses",
                column: "OrganizationUnitId",
                principalTable: "AbpOrganizationUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetUses_AbpOrganizationUnits_OrganizationUnitId",
                table: "BudgetUses");

            migrationBuilder.DropIndex(
                name: "IX_BudgetUses_OrganizationUnitId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "OrganizationUnitId",
                table: "BudgetUses");
        }
    }
}