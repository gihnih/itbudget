﻿import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import { SampleEntitiesComponent } from './sample/sampleEntities/sampleEntities.component';
import {BudgetApproversComponent} from './tracker/budgetApprovers/budgetApprovers.component';
import {BudgetRequestsComponent} from './tracker/budgetRequests/budgetRequests.component';
import {BudgetUsesComponent} from './tracker/budgetUses/budgetUses.component';
import {BudgetsComponent} from './tracker/budgets/budgets.component';

import {DashboardComponent} from './dashboard/dashboard.component';
import {UnbudgetsComponent} from '@app/main/tracker/unbudgets/unbudgets.component';
import {AllBudgetsComponent} from '@app/main/tracker/budgets/all-budgets.component';
import {PendingActivityComponent} from '@app/main/tracker/pending-activity/pending-activity.component';
import {MyBudgetsComponent} from '@app/main/tracker/my-budgets/my-budgets.component';
import {MyPendingActivityComponent} from '@app/main/tracker/pending-activity/my-pending-activity.component';
import {DepartmentDashboardComponent} from '@app/main/department-dashboard/department-dashboard.component';
import {BudgetUseCustodianComponent} from '@app/main/tracker/budgetUses/budget-use-custodian.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'sample/sampleEntities', component: SampleEntitiesComponent, data: { permission: 'Pages.SampleEntities' }  },
                    {path: 'dashboard', component: DashboardComponent, data: {permission: 'Pages.Dashboard.It'}},
                    {
                        path: 'department-dashboard',
                        component: DepartmentDashboardComponent,
                        data: {permission: 'Pages.Dashboard.MyDepartment'}
                    },
                    {
                        path: 'pendingActivity',
                        component: PendingActivityComponent,
                        data: {permission: 'Pages.PendingActivity'}
                    },
                    {
                        path: 'my-pending-activity',
                        component: MyPendingActivityComponent,
                        data: {permission: 'Pages.PendingActivity'}
                    },

                    {path: 'it/budget', component: BudgetsComponent, data: {permission: 'Pages.Budgets'}},
                    {path: 'it/budget-utilize', component: BudgetUseCustodianComponent, data: {permission: 'Pages.Budgets'}},
                    {path: 'it/unbudgets', component: UnbudgetsComponent, data: {permission: 'Pages.Budgets'}},

                    {path: 'mydepartment/budget', component: MyBudgetsComponent, data: {permission: 'Pages.MyBudgets'}},
                    {
                        path: 'mydepartment/budget-utilize',
                        component: BudgetUsesComponent,
                        data: {permission: 'Pages.MyBudgets'}
                    },
                    {
                        path: 'mydepartment/unbudgets',
                        component: UnbudgetsComponent,
                        data: {permission: 'Pages.MyBudgets'}
                    },

                    {
                        path: 'tracker/budgetRequests',
                        component: BudgetRequestsComponent,
                        data: {permission: 'Pages.BudgetRequests'}
                    },
                    {
                        path: 'tracker/budgetUses',
                        component: BudgetUsesComponent,
                        data: {permission: 'Pages.BudgetUses'}
                    },
                    {path: 'tracker/allbudgets', component: AllBudgetsComponent, data: {permission: 'Pages.Budgets'}},
                    {path: 'tracker/budgets', component: BudgetsComponent, data: {permission: 'Pages.Budgets'}},
                    {path: 'tracker/unbudgets', component: UnbudgetsComponent, data: {permission: 'Pages.Budgets'}},

                    {
                        path: '**', redirectTo: 'department-dashboard'
                    }

                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule {
}
