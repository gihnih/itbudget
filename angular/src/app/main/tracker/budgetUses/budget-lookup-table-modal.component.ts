import {Component, ViewChild, Injector, Output, EventEmitter, ViewEncapsulation} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {
    BudgetUsesServiceProxy,
    BudgetDto,
    OrganizationUnitServiceProxy, GetBudgetForViewDto, BudgetsServiceProxy, BudgetOrganizationUnitLookupTableDto
} from '@shared/service-proxies/service-proxies';
import {AppComponentBase} from '@shared/common/app-component-base';
import {DataTable} from 'primeng/components/datatable/datatable';
import {Paginator} from 'primeng/components/paginator/paginator';
import {LazyLoadEvent} from 'primeng/components/common/lazyloadevent';
import { Globals } from '@app/global';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'budgetLookupTableModal',
    styleUrls: ['./budget-lookup-table-modal.component.less'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './budget-lookup-table-modal.component.html'
})
export class BudgetLookupTableModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    filterText = '';
    id: number;
    displayName: string;
    budget: GetBudgetForViewDto;
    organizationUnit: any[] = [];
    category = 'Budgeted';
    budgetOwn;
    remarkFilter = '';

    budgetCategories = [];
    departments: BudgetOrganizationUnitLookupTableDto[] = [];
    budgetTypes = this._globals.budgetType;
    descriptionTypes = this._globals.budgetDescriptionType;

    budgetCategoryFilter = '';
    categoryFilter = '';
    budgetTypeNameFilter = '';
    glCodeCodeFilter = '';
    descriptionTypeNameFilter = '';
    budgetYearYearFilter = '';
    organizationUnitDisplayNameFilter = '';
    advancedFiltersAreShown = false;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;

    constructor(
        injector: Injector,
        private _budgetUsesServiceProxy: BudgetUsesServiceProxy,
        private _organizationUnitServiceProxy: OrganizationUnitServiceProxy,
        private _budgetServiceProxy: BudgetsServiceProxy,
        private _globals: Globals,
    ) {
        super(injector);
        this._budgetUsesServiceProxy.getListOrganizationUnit().subscribe(result => {
            this.departments = result;
            console.log(this.departments);
        });
    }

    show(type: string = 'own'): void {
        this.budget = null;
        this.active = true;
        this.paginator.rows = 5;
        this.budgetOwn = type;

        this.getAll();
        this.modal.show();
    }

    getAll(event?: LazyLoadEvent) {
        if (!this.active) {
            return;
        }

        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        if (this.budgetOwn === 'own') {
            this.getOwnBudget(event);
        } else {
            this.getOtherBudget(event);
        }


    }

    getOwnBudget(event?: LazyLoadEvent) {
        this._budgetServiceProxy.getAllMyBudgets(
            this.filterText,
            this.budgetCategoryFilter, // Budgeted or Unbudgeted
            this.budgetTypeNameFilter,
            this.glCodeCodeFilter,
            this.descriptionTypeNameFilter,
            this.budgetYearYearFilter,
            this.organizationUnitDisplayNameFilter,
            this.codeOrganizationUnit,
            this.remarkFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });

    }

    getOtherBudget(event?: LazyLoadEvent) {
        this._budgetServiceProxy.getAllOtherBudgets(
            this.filterText,
            this.budgetCategoryFilter,
            this.budgetTypeNameFilter,
            this.glCodeCodeFilter,
            this.descriptionTypeNameFilter,
            this.budgetYearYearFilter,
            this.organizationUnitDisplayNameFilter,
            this.codeOrganizationUnit,
            this.remarkFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    setAndSave(budget: GetBudgetForViewDto) {
        this.budget = budget;
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);

    }

    close(form?: NgForm): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
        this.advancedFiltersAreShown = false;

        // reset value in advance search
        form.reset();
        this.filterText = '';
        this.descriptionTypeNameFilter = '';
        this.organizationUnitDisplayNameFilter = '';
        this.budgetTypeNameFilter = '';
        this.budgetYearYearFilter = '';
    }
}
