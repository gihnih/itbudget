import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { GLCodesServiceProxy, CreateOrEditGLCodeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';


@Component({
    selector: 'createOrEditGLCodeModal',
    templateUrl: './create-or-edit-glCode-modal.component.html',
    styleUrls: ['./create-or-edit-glCode-modal.component.css'],
})
export class CreateOrEditGLCodeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    glCodeUnique = true;

    glCode: CreateOrEditGLCodeDto = new CreateOrEditGLCodeDto();



    constructor(
        injector: Injector,
        private _glCodesServiceProxy: GLCodesServiceProxy
    ) {
        super(injector);
    }

    show(glCodeId?: number): void {

        if (!glCodeId) {
            this.glCode = new CreateOrEditGLCodeDto();
            this.glCode.id = glCodeId;

            this.active = true;
            this.modal.show();
        } else {
            this._glCodesServiceProxy.getGLCodeForEdit(glCodeId).subscribe(result => {
                this.glCode = result.glCode;


                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
        if(this.glCodeUnique){
            this.saving = true;

            this._glCodesServiceProxy.createOrEdit(this.glCode)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
        }
    }

    close(): void {

        this.active = false;
        this.modal.hide();
    }

    checkGLCode(code: string){
        this._glCodesServiceProxy.getByCode(code).subscribe(result => {
            if(result.glCode){
                this.glCodeUnique = false;
            }
            else{
                this.glCodeUnique = true;
            }

            console.log(this.glCodeUnique);
        });
    }
}
