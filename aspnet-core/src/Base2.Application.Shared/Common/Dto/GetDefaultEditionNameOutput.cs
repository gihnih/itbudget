﻿namespace Base2.Common.Dto
{
    public class GetDefaultEditionNameOutput
    {
        public string Name { get; set; }
    }
}