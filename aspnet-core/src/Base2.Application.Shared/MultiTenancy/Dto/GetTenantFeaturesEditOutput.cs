using Abp.Application.Services.Dto;
using Base2.Editions.Dto;
using System.Collections.Generic;

namespace Base2.MultiTenancy.Dto
{
    public class GetTenantFeaturesEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}