using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetRequestsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string BudgetDescriptionFilter { get; set; }

        public string BudgetUseDescriptionFilter { get; set; }
    }
}