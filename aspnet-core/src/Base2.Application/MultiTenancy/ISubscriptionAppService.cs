﻿using Abp.Application.Services;
using System.Threading.Tasks;

namespace Base2.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task UpgradeTenantToEquivalentEdition(int upgradeEditionId);
    }
}