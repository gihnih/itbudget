﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Timing.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Base2.Timing
{
    public interface ITimingAppService : IApplicationService
    {
        Task<ListResultDto<NameValueDto>> GetTimezones(GetTimezonesInput input);

        Task<List<ComboboxItemDto>> GetTimezoneComboboxItems(GetTimezoneComboboxItemsInput input);
    }
}