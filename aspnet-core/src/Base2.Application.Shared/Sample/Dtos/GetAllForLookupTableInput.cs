﻿using Abp.Application.Services.Dto;

namespace Base2.Sample.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}