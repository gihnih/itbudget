import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { BudgetApproversServiceProxy, CreateOrEditBudgetApproverDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';


@Component({
    selector: 'createOrEditBudgetApproverModal',
    templateUrl: './create-or-edit-budgetApprover-modal.component.html'
})
export class CreateOrEditBudgetApproverModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    budgetApprover: CreateOrEditBudgetApproverDto = new CreateOrEditBudgetApproverDto();



    constructor(
        injector: Injector,
        private _budgetApproversServiceProxy: BudgetApproversServiceProxy
    ) {
        super(injector);
    }

    show(budgetApproverId?: number): void {

        if (!budgetApproverId) {
            this.budgetApprover = new CreateOrEditBudgetApproverDto();
            this.budgetApprover.id = budgetApproverId;

            this.active = true;
            this.modal.show();
        } else {
            this._budgetApproversServiceProxy.getBudgetApproverForEdit(budgetApproverId).subscribe(result => {
                this.budgetApprover = result.budgetApprover;


                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
            this._budgetApproversServiceProxy.createOrEdit(this.budgetApprover)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {

        this.active = false;
        this.modal.hide();
    }
}
