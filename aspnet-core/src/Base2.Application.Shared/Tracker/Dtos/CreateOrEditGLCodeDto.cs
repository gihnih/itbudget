using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class CreateOrEditGLCodeDto : EntityDto<int?>
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }
}