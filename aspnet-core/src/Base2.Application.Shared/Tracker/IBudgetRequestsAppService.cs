using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IBudgetRequestsAppService : IApplicationService
    {
        Task<PagedResultDto<GetBudgetRequestForViewDto>> GetAll(GetAllBudgetRequestsInput input);

        Task<GetBudgetRequestForViewDto> GetBudgetRequestForView(int id);

        Task<GetBudgetRequestForEditOutput> GetBudgetRequestForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBudgetRequestDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetBudgetRequestsToExcel(GetAllBudgetRequestsForExcelInput input);

        Task<PagedResultDto<BudgetRequestBudgetLookupTableDto>> GetAllBudgetForLookupTable(GetAllForLookupTableInput input);

        Task<PagedResultDto<BudgetRequestBudgetUseLookupTableDto>> GetAllBudgetUseForLookupTable(GetAllForLookupTableInput input);
    }
}