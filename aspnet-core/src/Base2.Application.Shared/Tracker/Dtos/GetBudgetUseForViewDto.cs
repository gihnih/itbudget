using Abp.Application.Services.Dto;
using Base2.Authorization.Users.Dto;
using Humanizer;
using System;
using System.Collections.Generic;

namespace Base2.Tracker.Dtos
{
    public class GetBudgetUseForViewDto : EntityDto
    {
        private string _paperStatus;
        private string _requestStatus;

        public string Description { get; set; }

        public decimal RequestAmount { get; set; }

        public decimal ActualAmount { get; set; }

        public string PaperStatus
        {
            get { return _paperStatus.Humanize(LetterCasing.Title); }
            set { _paperStatus = value; }
        }

        public string RequestStatus
        {
            get { return _requestStatus.Humanize(LetterCasing.Title); }
            set { _requestStatus = value; }
        }

        public long OrganizationUnitId { get; set; }

        public BudgetOrganizationUnitLookupTableDto OrganizationUnit { get; set; }

        public ICollection<BudgetRequestDto> BudgetRequests { get; set; }

        public ICollection<BinaryObjectDto> BinaryObjects { get; set; }

        public int CreatorUserId { get; set; }

        public DateTime CreationTime { get; set; }
    }
}