using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class BudgetCategoryDto : EntityDto
    {
        public string Name { get; set; }
    }
}