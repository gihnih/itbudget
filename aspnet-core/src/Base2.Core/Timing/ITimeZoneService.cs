﻿using Abp.Application.Services.Dto;
using Abp.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Base2.Timing
{
    public interface ITimeZoneService
    {
        Task<string> GetDefaultTimezoneAsync(SettingScopes scope, int? tenantId);

        List<NameValueDto> GetWindowsTimezones();
    }
}