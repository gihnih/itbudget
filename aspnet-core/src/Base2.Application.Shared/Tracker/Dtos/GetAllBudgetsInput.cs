using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string BudgetCategoryFilter { get; set; }

        public string BudgetTypeFilter { get; set; }

        public string GLCodeFilter { get; set; }

        public string DescriptionTypeFilter { get; set; }

        public string BudgetYearFilter { get; set; }

        public string OrganizationUnitFilter { get; set; }

        public List<string> CodeOrganizationUnit { get; set; }

        public string RemarkFilter { get; set; }
    }
}