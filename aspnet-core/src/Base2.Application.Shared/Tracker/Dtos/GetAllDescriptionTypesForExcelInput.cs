namespace Base2.Tracker.Dtos
{
    public class GetAllDescriptionTypesForExcelInput
    {
        public string Filter { get; set; }

        public string NameFilter { get; set; }
    }
}