using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using Base2.Storage;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base2.Tracker
{
    [Table("BudgetUses")]
    public class BudgetUse : FullAuditedEntity
    {
        public virtual string Description { get; set; }

        public virtual decimal RequestAmount { get; set; }

        public virtual decimal ActualAmount { get; set; }

        public virtual long? OrganizationUnitId { get; set; }

        public PaperStatus PaperStatus { get; set; }

        public RequestStatus RequestStatus { get; set; }

        public virtual ICollection<BudgetRequest> BudgetRequests { get; set; }

        public virtual ICollection<BinaryObject> BinaryObjects { get; set; }

        [ForeignKey("OrganizationUnitId")]
        public OrganizationUnit OrganizationUnit { get; set; }
    }
}