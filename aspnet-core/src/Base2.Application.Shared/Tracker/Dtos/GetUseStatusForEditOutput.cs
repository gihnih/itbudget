namespace Base2.Tracker.Dtos
{
    public class GetUseStatusForEditOutput
    {
        public CreateOrEditUseStatusDto UseStatus { get; set; }
    }
}