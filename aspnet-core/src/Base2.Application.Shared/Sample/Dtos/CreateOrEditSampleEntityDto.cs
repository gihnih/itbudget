﻿
using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Base2.Sample.Dtos
{
    public class CreateOrEditSampleEntityDto : EntityDto<int?>
    {

		[Required]
		[StringLength(SampleEntityConsts.MaxTitleLength, MinimumLength = SampleEntityConsts.MinTitleLength)]
		public string Title { get; set; }
		
		
		public string Description { get; set; }
		
		
		[Required]
		public string FieldRequired { get; set; }
		
		
		public string Field { get; set; }
		
		
		public decimal? FieldDecimalNull { get; set; }
		
		
		public double? FieldDoubleNull { get; set; }
		
		

    }
}