import {Component, Injector, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
    BudgetsServiceProxy,
    BudgetDto, OrganizationUnitServiceProxy, UserServiceProxy, GLCodesServiceProxy, GetGLCodeForViewDto
} from '@shared/service-proxies/service-proxies';
import {NotifyService} from '@abp/notify/notify.service';
import {AppComponentBase} from '@shared/common/app-component-base';
import {TokenAuthServiceProxy} from '@shared/service-proxies/service-proxies';
import {CreateOrEditBudgetModalComponent} from './create-or-edit-budget-modal.component';
import {ViewBudgetModalComponent} from './view-budget-modal.component';
import {appModuleAnimation} from '@shared/animations/routerTransition';
import {DataTable} from 'primeng/components/datatable/datatable';
import {Paginator} from 'primeng/components/paginator/paginator';
import {LazyLoadEvent} from 'primeng/components/common/lazyloadevent';
import {FileDownloadService} from '@shared/utils/file-download.service';
import {EnumElement, Globals} from '@app/global';
import {GLCodeLookupTableModalComponent} from '@app/main/tracker/budgets/glCode-lookup-table-modal.component';

@Component({
    templateUrl: './budgets.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class BudgetsComponent extends AppComponentBase {

    @ViewChild('createOrEditBudgetModal') createOrEditBudgetModal: CreateOrEditBudgetModalComponent;
    @ViewChild('viewBudgetModalComponent') viewBudgetModal: ViewBudgetModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;


    // filter
    filter = '';
    budgetTypeFilter = '';
    budgetCategory = 'Budgeted';
    descriptionTypeFilter = '';

    advancedFiltersAreShown = false;
    descriptionFilter = '';
    remarkFilter = '';
    categoryFilter = 'Budgeted';
    budgetTypeNameFilter = '';
    glCodeFilter = '';
    budgetYearFilter = '';
    organizationUnitFilter = '';
    organizationUnitId: any[];
    codeOrganizationUnit: any[] = [];

    budgetTypes = [];
    glCode: GetGLCodeForViewDto[];
    descriptionType = [];

    shownLoginName = '';
    tenancyName = '';
    userName = '';
    budget = [];

    userId = this.appSession.userId;
    userRole = null;


    constructor(
        injector: Injector,
        private _budgetsServiceProxy: BudgetsServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private _glCodeServiceProxy: GLCodesServiceProxy,
        private _userOrganizationUnit: OrganizationUnitServiceProxy,
        private _userService: UserServiceProxy,
        private _global: Globals
    ) {
        super(injector);
        this.initialData();
    }


    initialData() {
        this._budgetsServiceProxy.getBudgetType().subscribe(result => {
            this.budgetTypes = result;
            console.log(this.budgetTypes);
        });

        this._glCodeServiceProxy.getAll('', '', '', '', 0, 9999)
            .subscribe(result => {
                this.glCode = result.items;
            });

        this._budgetsServiceProxy.getDescriptionType().subscribe(result => this.descriptionType = result);
    }

    getBudgets(event?: LazyLoadEvent) {
        // get Budgets
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengDatatableHelper.showLoadingIndicator();


        this._budgetsServiceProxy.getAll(
            this.filter,
            this.budgetCategory,
            this.budgetTypeFilter,
            this.glCodeFilter,
            this.descriptionTypeFilter,
            this.budgetYearFilter,
            this.organizationUnitFilter,
            this.codeOrganizationUnit,
            this.remarkFilter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
            console.log('Budgets');
            console.log(result.items);
            // this.organizationUnitFilter = '';
            // console.log(result.items);

        });
    }


    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createBudget(): void {
        this.createOrEditBudgetModal.budgetCategory = this.budgetCategory;
        console.log(this.budgetCategory);
        this.createOrEditBudgetModal.show();
    }

    deleteBudget(budgetId: number): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._budgetsServiceProxy.delete(budgetId)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }


    exportToExcel(): void {
        console.log(this.budgetCategory);
        this._budgetsServiceProxy.getBudgetsToExcel(
            this.filter,
            this.descriptionFilter,
            this.remarkFilter,
            this.categoryFilter,
            this.budgetTypeFilter,
            this.glCodeFilter,
            this.descriptionTypeFilter,
            this.budgetYearFilter,
            this.organizationUnitFilter,
            this.budgetCategory
        )
            .subscribe(result => {
                // console.log(result);
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    // budgetDetail(data) {
    //     console.log(data);
    //     this.viewBudgetModal.show(data);
    // }
}
