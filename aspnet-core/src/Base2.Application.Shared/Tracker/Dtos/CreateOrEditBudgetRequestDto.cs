using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class CreateOrEditBudgetRequestDto : EntityDto<int?>
    {
        public int? BudgetId { get; set; }

        public int? BudgetUseId { get; set; }

        public decimal ActualAmount { get; set; }

        public string RequestStatus { get; set; }

        public bool OwnBudget { get; set; }
    }
}