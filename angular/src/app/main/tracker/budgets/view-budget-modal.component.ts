import {Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {
    GetBudgetForViewDto,
    BudgetDto,
    GLCodeDto,
    BudgetOrganizationUnitLookupTableDto,
    BudgetUseDto,
    GetBudgetRequestForViewDto,
    BudgetRequestBudgetUseDto,
    GetBudgetUseForViewDto, BudgetsServiceProxy
} from '@shared/service-proxies/service-proxies';
import {AppComponentBase} from '@shared/common/app-component-base';

@Component({
    selector: 'viewBudgetModal',
    templateUrl: './view-budget-modal.component.html'
})
export class ViewBudgetModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    isUnbudgeted = false;
    item: GetBudgetForViewDto = new GetBudgetForViewDto();


    constructor(
        injector: Injector,
        private _budgetsServiceProxy: BudgetsServiceProxy,
    ) {
        super(injector);
    }

    show(id: number): void {
        this._budgetsServiceProxy.getBudgetForView(id).subscribe(result => {
            console.log(result);
            this.item = result;
            this.active = true;
            if (this.item.budgetCategory === 'Unbudgeted') {
                this.isUnbudgeted = true;
            }
            this.modal.show();
        });

        // this.item = data;
        //


    }

    close(): void {
        this.active = false;
        this.isUnbudgeted = false;
        this.modal.hide();
    }
}
