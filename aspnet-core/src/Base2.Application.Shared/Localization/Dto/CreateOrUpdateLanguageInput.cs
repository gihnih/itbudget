﻿using System.ComponentModel.DataAnnotations;

namespace Base2.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}