﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class Added_SharedMessageId_To_ChatMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SharedMessageId",
                table: "AppChatMessages",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SharedMessageId",
                table: "AppChatMessages");
        }
    }
}