using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IDescriptionTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetDescriptionTypeForViewDto>> GetAll(GetAllDescriptionTypesInput input);

        Task<GetDescriptionTypeForViewDto> GetDescriptionTypeForView(int id);

        Task<GetDescriptionTypeForEditOutput> GetDescriptionTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDescriptionTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDescriptionTypesToExcel(GetAllDescriptionTypesForExcelInput input);
    }
}