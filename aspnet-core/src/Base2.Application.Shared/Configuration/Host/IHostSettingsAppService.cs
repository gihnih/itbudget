﻿using Abp.Application.Services;
using Base2.Configuration.Host.Dto;
using System.Threading.Tasks;

namespace Base2.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}