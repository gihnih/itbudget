namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetsForExcelInput
    {
        public string Filter { get; set; }

        public string DescriptionFilter { get; set; }

        public string RemarkFilter { get; set; }

        public string CategoryFilter { get; set; }

        public string BudgetTypeFilter { get; set; }

        public string GLCodeFilter { get; set; }

        public string DescriptionTypeFilter { get; set; }

        public string BudgetYearFilter { get; set; }

        public string OrganizationUnitFilter { get; set; }

        public string BudgetCategory { get; set; }
    }
}