﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Common.Dto;
using Base2.Editions.Dto;
using System.Threading.Tasks;

namespace Base2.Common
{
    public interface ICommonLookupAppService : IApplicationService
    {
        Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false);

        Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input);

        GetDefaultEditionNameOutput GetDefaultEditionName();
    }
}