﻿using Abp.Application.Services;
using Base2.Configuration.Tenants.Dto;
using System.Threading.Tasks;

namespace Base2.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}