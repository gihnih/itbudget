﻿namespace Base2.Tracker.Dtos
{
    public class DashboardItDto
    {
        public decimal TotalBudgeted { get; set; }

        public decimal BalanceBudgetd { get; set; }

        public decimal TotalBudgetUtilized { get; set; }

        public decimal TotalUnbudgeted { get; set; }

        public int PendingAction { get; set; }

        public decimal TotalCapexBudgeted { get; set; }

        public decimal TotalCapexUtilized { get; set; }

        public decimal TotalOpexBudgeted { get; set; }

        public decimal TotalOpexUtilized { get; set; }
    }
}