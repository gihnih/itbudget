﻿namespace Base2.DemoUiComponents.Dto
{
    public class DateToStringOutput
    {
        public string DateString { get; set; }
    }
}