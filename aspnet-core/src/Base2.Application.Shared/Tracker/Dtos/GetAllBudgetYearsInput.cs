using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetYearsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? MaxYearFilter { get; set; }
        public int? MinYearFilter { get; set; }
    }
}