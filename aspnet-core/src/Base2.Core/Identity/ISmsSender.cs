using System.Threading.Tasks;

namespace Base2.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}