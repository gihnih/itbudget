﻿#if FEATURE_LDAP

using Abp.Zero.Ldap.Authentication;
using Abp.Zero.Ldap.Configuration;
using Base2.Authorization.Users;
using Base2.MultiTenancy;

namespace Base2.Authorization.Ldap
{
    public class AppLdapAuthenticationSource : LdapAuthenticationSource<Tenant, User>
    {
        public AppLdapAuthenticationSource(ILdapSettings settings, IAbpZeroLdapModuleConfig ldapModuleConfig)
            : base(settings, ldapModuleConfig)
        {
        }
    }
}

#endif