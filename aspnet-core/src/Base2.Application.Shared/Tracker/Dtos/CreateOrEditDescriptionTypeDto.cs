using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Base2.Tracker.Dtos
{
    public class CreateOrEditDescriptionTypeDto : EntityDto<int?>
    {
        [Required]
        public string Name { get; set; }
    }
}