﻿namespace Base2.Tracker
{
    public enum RequestStatus
    {
        Submitted = 1,
        VerifiedAndReserved = 2,
        Declined = 3,
        Approved = 4,
        Rejected = 5,
        PendingApproval = 6,
        Cancel = 7
    }
}