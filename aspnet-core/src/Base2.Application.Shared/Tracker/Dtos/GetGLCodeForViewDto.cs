namespace Base2.Tracker.Dtos
{
    public class GetGLCodeForViewDto
    {
        public GLCodeDto GLCode { get; set; }
    }
}