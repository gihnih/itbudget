import {Component, Injector, ViewEncapsulation, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Http} from '@angular/http';
import {BudgetApproversServiceProxy, BudgetApproverDto} from '@shared/service-proxies/service-proxies';
import {NotifyService} from '@abp/notify/notify.service';
import {AppComponentBase} from '@shared/common/app-component-base';
import {TokenAuthServiceProxy} from '@shared/service-proxies/service-proxies';
import {CreateOrEditBudgetApproverModalComponent} from './create-or-edit-budgetApprover-modal.component';
import {ViewBudgetApproverModalComponent} from './view-budgetApprover-modal.component';
import {appModuleAnimation} from '@shared/animations/routerTransition';
import {DataTable} from 'primeng/components/datatable/datatable';
import {Paginator} from 'primeng/components/paginator/paginator';
import {LazyLoadEvent} from 'primeng/components/common/lazyloadevent';
import {FileDownloadService} from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './budgetApprovers.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class BudgetApproversComponent extends AppComponentBase {

    @ViewChild('createOrEditBudgetApproverModal') createOrEditBudgetApproverModal: CreateOrEditBudgetApproverModalComponent;
    @ViewChild('viewBudgetApproverModalComponent') viewBudgetApproverModal: ViewBudgetApproverModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    maxUserIdFilter: number;
    maxUserIdFilterEmpty: number;
    minUserIdFilter: number;
    minUserIdFilterEmpty: number;
    maxBudgetUseIdFilter: number;
    maxBudgetUseIdFilterEmpty: number;
    minBudgetUseIdFilter: number;
    minBudgetUseIdFilterEmpty: number;
    maxBudgetIdFilter: number;
    maxBudgetIdFilterEmpty: number;
    minBudgetIdFilter: number;
    minBudgetIdFilterEmpty: number;
    maxUseStatusIdFilter: number;
    maxUseStatusIdFilterEmpty: number;
    minUseStatusIdFilter: number;
    minUseStatusIdFilterEmpty: number;
    maxRequestAmountFilter: number;
    maxRequestAmountFilterEmpty: number;
    minRequestAmountFilter: number;
    minRequestAmountFilterEmpty: number;
    maxActualAmountFilter: number;
    maxActualAmountFilterEmpty: number;
    minActualAmountFilter: number;
    minActualAmountFilterEmpty: number;
    status: string;


    constructor(
        injector: Injector,
        private _budgetApproversServiceProxy: BudgetApproversServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    getBudgetApprovers(event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        this._budgetApproversServiceProxy.getAll(
            this.filterText,
            this.status,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createBudgetApprover(): void {
        this.createOrEditBudgetApproverModal.show();
    }

    deleteBudgetApprover(budgetApprover: BudgetApproverDto): void {
        this.message.confirm(
            '',
            (isConfirmed) => {
                if (isConfirmed) {
                    this._budgetApproversServiceProxy.delete(budgetApprover.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            }
        );
    }

    exportToExcel(): void {
        this._budgetApproversServiceProxy.getBudgetApproversToExcel(
            this.filterText,
            this.maxUserIdFilter == null ? this.maxUserIdFilterEmpty : this.maxUserIdFilter,
            this.minUserIdFilter == null ? this.minUserIdFilterEmpty : this.minUserIdFilter,
            this.maxBudgetUseIdFilter == null ? this.maxBudgetUseIdFilterEmpty : this.maxBudgetUseIdFilter,
            this.minBudgetUseIdFilter == null ? this.minBudgetUseIdFilterEmpty : this.minBudgetUseIdFilter,
            this.maxBudgetIdFilter == null ? this.maxBudgetIdFilterEmpty : this.maxBudgetIdFilter,
            this.minBudgetIdFilter == null ? this.minBudgetIdFilterEmpty : this.minBudgetIdFilter,
            this.maxUseStatusIdFilter == null ? this.maxUseStatusIdFilterEmpty : this.maxUseStatusIdFilter,
            this.minUseStatusIdFilter == null ? this.minUseStatusIdFilterEmpty : this.minUseStatusIdFilter,
            this.maxRequestAmountFilter == null ? this.maxRequestAmountFilterEmpty : this.maxRequestAmountFilter,
            this.minRequestAmountFilter == null ? this.minRequestAmountFilterEmpty : this.minRequestAmountFilter,
            this.maxActualAmountFilter == null ? this.maxActualAmountFilterEmpty : this.maxActualAmountFilter,
            this.minActualAmountFilter == null ? this.minActualAmountFilterEmpty : this.minActualAmountFilter,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
