namespace Base2.Tracker.Dtos
{
    public class GetBudgetTypeForEditOutput
    {
        public CreateOrEditBudgetTypeDto BudgetType { get; set; }
    }
}