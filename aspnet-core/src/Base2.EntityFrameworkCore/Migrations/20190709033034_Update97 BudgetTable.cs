﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Base2.Migrations
{
    public partial class Update97BudgetTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CurrentAmount",
                table: "Budgets",
                newName: "EstimatedBalance");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Budgets",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Budgets",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Budgets",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Budgets",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Budgets",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Budgets",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Budgets",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Budgets");

            migrationBuilder.RenameColumn(
                name: "EstimatedBalance",
                table: "Budgets",
                newName: "CurrentAmount");
        }
    }
}