namespace Base2.Tracker.Dtos
{
    public class BudgetGLCodeLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}