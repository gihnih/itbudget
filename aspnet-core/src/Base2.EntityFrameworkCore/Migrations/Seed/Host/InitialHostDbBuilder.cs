﻿using Base2.EntityFrameworkCore;

namespace Base2.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly Base2DbContext _context;

        public InitialHostDbBuilder(Base2DbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new DefaultGLCodeCreator(_context).Create();
            new DefaultOrganizationUnitCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}