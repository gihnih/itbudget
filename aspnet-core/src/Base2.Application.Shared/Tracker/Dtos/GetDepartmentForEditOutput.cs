namespace Base2.Tracker.Dtos
{
    public class GetDepartmentForEditOutput
    {
        public CreateOrEditDepartmentDto Department { get; set; }
    }
}