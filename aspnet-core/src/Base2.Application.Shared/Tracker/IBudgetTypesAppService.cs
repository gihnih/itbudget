using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IBudgetTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetBudgetTypeForViewDto>> GetAll(GetAllBudgetTypesInput input);

        Task<GetBudgetTypeForViewDto> GetBudgetTypeForView(int id);

        Task<GetBudgetTypeForEditOutput> GetBudgetTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBudgetTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetBudgetTypesToExcel(GetAllBudgetTypesForExcelInput input);
    }
}