﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Caching.Dto;
using System.Threading.Tasks;

namespace Base2.Caching
{
    public interface ICachingAppService : IApplicationService
    {
        ListResultDto<CacheDto> GetAllCaches();

        Task ClearCache(EntityDto<string> input);

        Task ClearAllCaches();
    }
}