﻿namespace Base2.Chat
{
    public enum ChatMessageReadState
    {
        Unread = 1,

        Read = 2
    }
}