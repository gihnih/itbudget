﻿import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { SampleEntitiesServiceProxy, CreateOrEditSampleEntityDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';

@Component({
    selector: 'createOrEditSampleEntityModal',
    templateUrl: './create-or-edit-sampleEntity-modal.component.html'
})
export class CreateOrEditSampleEntityModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    sampleEntity: CreateOrEditSampleEntityDto = new CreateOrEditSampleEntityDto();



    constructor(
        injector: Injector,
        private _sampleEntitiesServiceProxy: SampleEntitiesServiceProxy
    ) {
        super(injector);
    }

    show(sampleEntityId?: number): void {

        if (!sampleEntityId) {
            this.sampleEntity = new CreateOrEditSampleEntityDto();
            this.sampleEntity.id = sampleEntityId;

            this.active = true;
            this.modal.show();
        } else {
            this._sampleEntitiesServiceProxy.getSampleEntityForEdit(sampleEntityId).subscribe(result => {
                this.sampleEntity = result.sampleEntity;


                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
            this._sampleEntitiesServiceProxy.createOrEdit(this.sampleEntity)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
