namespace Base2.Tracker
{
    public enum UseStatus
    {
        PendingConceptPaperApproval = 1,
        ConceptPaperApproved = 2,
        ConceptPaperReject = 3,
        LOAIssued = 4
    }
}