﻿using Base2.Auditing.Dto;
using Base2.Dto;
using System.Collections.Generic;

namespace Base2.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}