using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IGLCodesAppService : IApplicationService
    {
        Task<PagedResultDto<GetGLCodeForViewDto>> GetAll(GetAllGLCodesInput input);

        Task<GetGLCodeForViewDto> GetGLCodeForView(int id);

        Task<GetGLCodeForEditOutput> GetGLCodeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditGLCodeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetGLCodesToExcel(GetAllGLCodesForExcelInput input);
    }
}