using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class CreateOrEditUseStatusDto : EntityDto<int?>
    {
        public string Name { get; set; }
    }
}