import {Component, EventEmitter, Injector, OnInit, Output, ViewChild} from '@angular/core';
import {AppComponentBase} from '@shared/common/app-component-base';
import {
    BinaryObjectDto,
    BudgetApproversServiceProxy,
    BudgetDto,
    BudgetOrganizationUnitLookupTableDto,
    BudgetRequestsServiceProxy,
    BudgetUse2Dto,
    BudgetUsesServiceProxy,
    CreateOrEditBudgetApproverDto,
    CreateOrEditBudgetRequestDto,
    CreateOrEditBudgetUseDto,
    EditBudgetApproveDto,
    GetAllMyPendingAction,
    GetBudgetUseForViewDto,
    GetGLCodeForViewDto,
    GetUserForEditOutput,
    GLCodesServiceProxy,
    OrganizationUnitDto,
    OrganizationUnitServiceProxy,
    UserEditDto,
    UserServiceProxy,
} from '@shared/service-proxies/service-proxies';
import {ModalDirective} from '@node_modules/ngx-bootstrap';
import {AppConsts} from '@shared/AppConsts';

@Component({
    selector: 'app-detail-modal',
    templateUrl: './detail-modal.component.html',
    styleUrls: ['./detail-modal.component.css']
})
export class DetailModalComponent extends AppComponentBase {

    @ViewChild('detailModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    glCode: GetGLCodeForViewDto[] = [];
    descriptionType: any[];
    budgetYear: any[];

    input: EditBudgetApproveDto;
    data: GetAllMyPendingAction = new GetAllMyPendingAction();

    user: GetUserForEditOutput;

    filter = '';
    skip = 0;
    sorting = '';

    budgetApprover: CreateOrEditBudgetApproverDto;
    getUrl: string;

    constructor(injector: Injector,
                private glCodeProxy: GLCodesServiceProxy,
                private organizationUnitProxy: OrganizationUnitServiceProxy,
                private budgetApproverProxy: BudgetApproversServiceProxy,
                private _budgetRequestProxy: BudgetRequestsServiceProxy,
                private  _userProxy: UserServiceProxy,
                private _budgetUseProxy: BudgetUsesServiceProxy) {
        super(injector);
        this.getUrl = AppConsts.remoteServiceBaseUrl + '/Upload/GetUploadedObject';
    }


    show(budgetUse: GetAllMyPendingAction): void {
        this._budgetUseProxy.getUserForEdit(budgetUse.creatorUserId).subscribe(result => {
            this.user = result;
            this.data = budgetUse;
            this.active = true;
            this.modal.show();
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.modalSave.emit(null);
    }


    approved(status: string, pendingAction: GetAllMyPendingAction) {
        let data: CreateOrEditBudgetRequestDto = new CreateOrEditBudgetRequestDto();
        data.budgetId = pendingAction.budgetId;
        data.budgetUseId = pendingAction.budgetUseId;
        data.requestStatus = status;
        this._budgetRequestProxy.update(data).subscribe(result => {
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
        });
    }

}
