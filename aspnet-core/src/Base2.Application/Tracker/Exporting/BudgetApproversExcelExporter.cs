using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public class BudgetApproversExcelExporter : EpPlusExcelExporterBase, IBudgetApproversExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BudgetApproversExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) :
    base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetBudgetApproverForViewDto> budgetApprovers)
        {
            return CreateExcelPackage(
                "BudgetApprovers.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("BudgetApprovers"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("UserId"),
                        L("BudgetUseId"),
                        L("BudgetId"),
                        L("UseStatusId"),
                        L("RequestAmount"),
                        L("ActualAmount")
                        );

                    AddObjects(
                        sheet, 2, budgetApprovers,
                        _ => _.BudgetApprover.UserId,
                        _ => _.BudgetApprover.BudgetUseId,
                        _ => _.BudgetApprover.BudgetId,
                        _ => _.BudgetApprover.UseStatusId,
                        _ => _.BudgetApprover.RequestAmount,
                        _ => _.BudgetApprover.ActualAmount
                        );
                });
        }
    }
}