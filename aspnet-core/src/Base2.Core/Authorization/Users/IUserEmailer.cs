﻿using Base2.Tracker;
using System.Threading.Tasks;

namespace Base2.Authorization.Users
{
    public interface IUserEmailer
    {
        /// <summary>
        /// Send email activation link to user's email address.
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="link">Email activation link</param>
        /// <param name="plainPassword">
        /// Can be set to user's plain password to include it in the email.
        /// </param>
        Task SendEmailActivationLinkAsync(User user, string link, string plainPassword = null);

        /// <summary>
        /// Sends a password reset link to user's email.
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="link">Password reset link (optional)</param>
        Task SendPasswordResetLinkAsync(User user, string link = null);

        // Add new budget, Notify user base Organization Unit
        Task SendBudgetedAddedAsync(User user, Budget budget);

        // Request own budgeted item, Notify Budget Custodian
        Task SendTaskBudgetedUseCustodian(User user, string message);

        // Request Other Department Budget; Notify Budget Owner
        Task SendTaskBudgetedUseBudgetOwner(User user, string message);

        // Custodian Approval; Notify BudgetUse Requested. (Verified or Decline)
        Task SendRespondApprovalToRequesterCustodian(User user, string message);

        // Budget Owner Approval; Notify BudgetUse requested & Custodian if approved (Approval or rejected)
        Task SendRespondApprovalToRequesterBudgetOwner(User user, string message);

        // Second approval; Notify Custodian as second approval
        Task SendTaskSecondApprovalToRequesterBudgetOwner(User user, string message);

        // Update current amount; Notify Budget Owner & Custodian.
        //Task SendBudgetedUtilizeApprovalAsync(User user, string status);
        //Task SendBudgetedOwnerApprovalUtilizeAsync(User user, string status);
    }
}