﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class UpdateBudgetTable2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Budgeted",
                table: "Budgets",
                newName: "Unbudgeted");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Unbudgeted",
                table: "Budgets",
                newName: "Budgeted");
        }
    }
}