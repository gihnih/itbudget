﻿using Base2.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;

namespace Base2.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */

    public class Base2DbContextFactory : IDesignTimeDbContextFactory<Base2DbContext>
    {
        public Base2DbContext CreateDbContext(string[] args)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new DbContextOptionsBuilder<Base2DbContext>();

            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(WebContentDirectoryFinder.CalculateContentRootFolder())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{envName}.json", optional: true)
                .Build();

            Base2DbContextConfigurer.Configure(builder, configuration.GetConnectionString(Base2Consts.ConnectionStringName));

            return new Base2DbContext(builder.Options);
        }
    }
}