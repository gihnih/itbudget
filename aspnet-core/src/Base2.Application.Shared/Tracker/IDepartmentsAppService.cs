using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IDepartmentsAppService : IApplicationService
    {
        Task<PagedResultDto<GetDepartmentForViewDto>> GetAll(GetAllDepartmentsInput input);

        Task<GetDepartmentForViewDto> GetDepartmentForView(int id);

        Task<GetDepartmentForEditOutput> GetDepartmentForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDepartmentDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDepartmentsToExcel(GetAllDepartmentsForExcelInput input);
    }
}