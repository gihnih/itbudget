﻿namespace Base2.Install.Dto
{
    public class CheckDatabaseOutput
    {
        public bool IsDatabaseExist { get; set; }
    }
}