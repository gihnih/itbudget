﻿namespace Base2.Tracker.Dtos
{
    public class BudgetDescriptionTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}