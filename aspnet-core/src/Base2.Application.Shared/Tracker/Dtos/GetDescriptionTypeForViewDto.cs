namespace Base2.Tracker.Dtos
{
    public class GetDescriptionTypeForViewDto
    {
        public DescriptionTypeDto DescriptionType { get; set; }
    }
}