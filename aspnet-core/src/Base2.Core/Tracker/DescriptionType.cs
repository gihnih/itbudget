namespace Base2.Tracker
{
    public enum DescriptionType
    {
        NewProject = 1,
        Improvement = 2,
        License = 3,
        Others = 4
    }
}