﻿using Abp.Authorization.Users;
using System.ComponentModel.DataAnnotations;

namespace Base2.Configuration.Host.Dto
{
    public class SendTestEmailInput
    {
        [Required]
        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}