using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IBudgetsAppService : IApplicationService
    {
        Task<PagedResultDto<GetBudgetForViewDto>> GetAll(GetAllBudgetsInput input);

        Task<GetBudgetForViewDto> GetBudgetForView(int id);

        Task<GetBudgetForEditOutput> GetBudgetForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBudgetDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetBudgetsToExcel(GetAllBudgetsForExcelInput input);

        Task<PagedResultDto<BudgetGLCodeLookupTableDto>> GetAllGLCodeForLookupTable(GetAllForLookupTableInput input);

        Task<PagedResultDto<BudgetOrganizationUnitLookupTableDto>> GetAllOrganizationUnitForLookupTable(GetAllForLookupTableInput input);
    }
}