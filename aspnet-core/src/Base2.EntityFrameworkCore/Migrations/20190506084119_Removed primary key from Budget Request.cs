﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class RemovedprimarykeyfromBudgetRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetRequests_Budgets_BudgetId",
                table: "BudgetRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetRequests_BudgetUses_BudgetUseId",
                table: "BudgetRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetUses_Budgets_BudgetId",
                table: "BudgetUses");

            migrationBuilder.DropIndex(
                name: "IX_BudgetUses_BudgetId",
                table: "BudgetUses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BudgetRequests",
                table: "BudgetRequests");

            migrationBuilder.DropIndex(
                name: "IX_BudgetRequests_BudgetId",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "BudgetRequests");

            migrationBuilder.AlterColumn<int>(
                name: "BudgetUseId",
                table: "BudgetRequests",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BudgetId",
                table: "BudgetRequests",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_BudgetRequests",
                table: "BudgetRequests",
                columns: new[] { "BudgetId", "BudgetUseId" });

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetRequests_Budgets_BudgetId",
                table: "BudgetRequests",
                column: "BudgetId",
                principalTable: "Budgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetRequests_BudgetUses_BudgetUseId",
                table: "BudgetRequests",
                column: "BudgetUseId",
                principalTable: "BudgetUses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetRequests_Budgets_BudgetId",
                table: "BudgetRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetRequests_BudgetUses_BudgetUseId",
                table: "BudgetRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BudgetRequests",
                table: "BudgetRequests");

            migrationBuilder.AlterColumn<int>(
                name: "BudgetUseId",
                table: "BudgetRequests",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "BudgetId",
                table: "BudgetRequests",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "BudgetRequests",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_BudgetRequests",
                table: "BudgetRequests",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetUses_BudgetId",
                table: "BudgetUses",
                column: "BudgetId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetRequests_BudgetId",
                table: "BudgetRequests",
                column: "BudgetId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetRequests_Budgets_BudgetId",
                table: "BudgetRequests",
                column: "BudgetId",
                principalTable: "Budgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetRequests_BudgetUses_BudgetUseId",
                table: "BudgetRequests",
                column: "BudgetUseId",
                principalTable: "BudgetUses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetUses_Budgets_BudgetId",
                table: "BudgetUses",
                column: "BudgetId",
                principalTable: "Budgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}