﻿namespace Base2
{
    public class Base2Consts
    {
        public const string LocalizationSourceName = "Base2";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const int PaymentCacheDurationInMinutes = 30;
    }
}