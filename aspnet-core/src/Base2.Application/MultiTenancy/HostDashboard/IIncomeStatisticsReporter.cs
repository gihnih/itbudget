﻿using Base2.MultiTenancy.HostDashboard.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Base2.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}