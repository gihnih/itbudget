﻿using Base2.Security.Recaptcha;
using System.Threading.Tasks;

namespace Base2.Tests.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}