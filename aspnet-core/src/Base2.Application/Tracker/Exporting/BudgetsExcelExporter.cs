using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public class BudgetsExcelExporter : EpPlusExcelExporterBase, IBudgetsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BudgetsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) :
    base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetBudgetForViewDto> budgets)
        {
            return CreateExcelPackage(
                "Budgets.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Budgets"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Description"),
                        L("Year"),
                        L("BudgetType"),
                        L("GLCode"),
                        L("DescriptionType"),
                        L("OrganizationUnit"),
                        L("BudgetAmount"),
                        L("UtilizedBudget"),
                        L("CurrentAmount"),
                        L("Remark")
                        );

                    AddObjects(
                        sheet, 2, budgets,
                        _ => _.Description,
                        _ => _.BudgetYear,
                        _ => _.BudgetType,
                        _ => _.GLCode.Code,
                        _ => _.DescriptionType,
                        _ => _.OrganizationUnit.DisplayName,
                        _ => _.BudgetAmount,
                        _ => _.UtilizedBudget,
                        _ => _.EstimatedBalance,
                        _ => _.Remark
                        );
                });
        }

        public FileDto ExportUnbudgetedToFile(List<GetBudgetForViewDto> budgets)
        {
            return CreateExcelPackage(
                "Unbudgets.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Unbudgets"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("BudgetType"),
                        L("Year"),
                        L("OrganizationUnit"),
                        L("BudgetAmount"),
                        L("Remark")
                        );

                    AddObjects(
                        sheet, 2, budgets,
                        _ => _.BudgetType,
                        _ => _.BudgetYear,
                        _ => _.OrganizationUnit.DisplayName,
                        _ => _.BudgetAmount,
                        _ => _.Remark
                        );
                });
        }
    }
}