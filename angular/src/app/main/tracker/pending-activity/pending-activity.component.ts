import {Component, Injector, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AppComponentBase} from '@shared/common/app-component-base';
import {DetailModalComponent} from '@app/main/tracker/pending-activity/detail-modal/detail-modal.component';
import {LazyLoadEvent} from '@node_modules/primeng/components/common/lazyloadevent';
import {Paginator} from '@node_modules/primeng/components/paginator/paginator';
import {
    BudgetApproversServiceProxy,
    BudgetRequestsServiceProxy,
    BudgetUsesServiceProxy, UserServiceProxy
} from '@shared/service-proxies/service-proxies';
import {DataTable} from '@node_modules/primeng/components/datatable/datatable';
import {appModuleAnimation} from '@shared/animations/routerTransition';

@Component({
    selector: 'app-pending-activity',
    templateUrl: './pending-activity.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PendingActivityComponent extends AppComponentBase {

    @ViewChild('app-detail-modal') detailModal: DetailModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    filter: string;
    description: string;
    useStatusName: string;
    budgetDescription: string;
    custodianStatus = 'Pending custodian approval';

    status: string;

    constructor(injector: Injector,
                private _budgetRequestService: BudgetRequestsServiceProxy) {
        super(injector);
    }



    getCustodianPendingAction(event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();


        this._budgetRequestService.getCustodianPendingAction(
            this.filter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
            console.log(result.items);
        });
    }
}
