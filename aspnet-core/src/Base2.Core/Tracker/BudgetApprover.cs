using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base2.Tracker
{
    [Table("BudgetApprovers")]
    public class BudgetApprover : AuditedEntity
    {
        public virtual long? UserId { get; set; }
        public virtual long? UserRequestId { get; set; }

        public virtual int? BudgetUseId { get; set; }

        public virtual int? BudgetId { get; set; }

        public virtual PaperStatus PaperStatus { get; set; }

        public virtual RequestStatus RequestStatus { get; set; }

        public virtual decimal? RequestAmount { get; set; }

        public virtual decimal? ActualAmount { get; set; }

        public virtual string Status { get; set; }
    }
}