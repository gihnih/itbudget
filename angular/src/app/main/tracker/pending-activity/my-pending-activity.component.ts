import {Component, Injector, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {LazyLoadEvent} from '@node_modules/primeng/components/common/lazyloadevent';
import {AppComponentBase} from '@shared/common/app-component-base';
import {appModuleAnimation} from '@shared/animations/routerTransition';
import {BudgetRequestsServiceProxy, UserServiceProxy} from '@shared/service-proxies/service-proxies';
import {DetailModalComponent} from '@app/main/tracker/pending-activity/detail-modal/detail-modal.component';
import {DataTable} from '@node_modules/primeng/components/datatable/datatable';
import {Paginator} from '@node_modules/primeng/components/paginator/paginator';

@Component({
    selector: 'app-my-pending-activity',
    templateUrl: './my-pending-activity.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MyPendingActivityComponent extends AppComponentBase {
    @ViewChild('app-detail-modal') detailModal: DetailModalComponent;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    filter: string;

    constructor(injector: Injector,
                private _budgetRequestService: BudgetRequestsServiceProxy) {
        super(injector);
    }

    getMyPendingAction($event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging($event)) {
            this.paginator.changePage(0);
            return;
        }
        this.primengDatatableHelper.showLoadingIndicator();
        this._budgetRequestService.getMyPendingAction(
            this.filter,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getSkipCount(this.paginator, $event),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, $event)
        ).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
    }

}
