using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IBudgetUsesAppService : IApplicationService
    {
        Task<PagedResultDto<GetBudgetUseForViewDto>> GetAll(GetAllBudgetUsesInput input);

        Task<GetBudgetUseDetailForViewDto> GetBudgetUseDetailForView(int id);

        Task<GetBudgetUseForEditOutput> GetBudgetUseForEdit(EntityDto input);

        Task CreateOrEdit(GetBudgetUseForEditOutput input);

        Task Delete(EntityDto input);

        //Task<FileDto> GetBudgetUsesToExcel(GetAllBudgetUsesForExcelInput input);

        Task<PagedResultDto<GetBudgetForViewDto>> GetAllBudgetForLookupTable(GetAllForLookupTableInput input);

        Task<PagedResultDto<GetBudgetForViewDto>> GetAllOtherBudgetForLookupTable(GetAllForLookupTableInput input);
    }
}