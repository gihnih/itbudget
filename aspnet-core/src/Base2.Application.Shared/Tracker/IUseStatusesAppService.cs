using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IUseStatusesAppService : IApplicationService
    {
        Task<PagedResultDto<GetUseStatusForViewDto>> GetAll(GetAllUseStatusesInput input);

        Task<GetUseStatusForViewDto> GetUseStatusForView(int id);

        Task<GetUseStatusForEditOutput> GetUseStatusForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditUseStatusDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetUseStatusesToExcel(GetAllUseStatusesForExcelInput input);
    }
}