﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Base2.Migrations
{
    public partial class UpdateDatabase24074 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "BudgetRequests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "BudgetRequests",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "BudgetRequests");
        }
    }
}