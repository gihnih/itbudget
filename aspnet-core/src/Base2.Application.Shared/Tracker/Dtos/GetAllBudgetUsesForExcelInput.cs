namespace Base2.Tracker.Dtos
{
    public class GetAllBudgetUsesForExcelInput
    {
        public string DescriptionFilter { get; set; }

        public string PaperStatus { get; set; }

        public string RequestStatus { get; set; }
    }
}