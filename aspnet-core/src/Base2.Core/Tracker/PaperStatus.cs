namespace Base2.Tracker
{
    public enum PaperStatus
    {
        PendingConceptPaperApproval = 1,
        ConceptPaperApproved = 2,
        ConceptPaperRejected = 3,
        LOAIssued = 4
    }
}