﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Sample.Dtos;
using Base2.Dto;
using Base2.Storage;

namespace Base2.Sample.Exporting
{
    public class SampleEntitiesExcelExporter : EpPlusExcelExporterBase, ISampleEntitiesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SampleEntitiesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) :  base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetSampleEntityForViewDto> sampleEntities)
        {
            return CreateExcelPackage(
                "SampleEntities.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SampleEntities"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Title"),
                        L("Description"),
                        L("FieldRequired"),
                        L("Field"),
                        L("FieldDecimalNull"),
                        L("FieldDoubleNull")
                        );

                    AddObjects(
                        sheet, 2, sampleEntities,
                        _ => _.SampleEntity.Title,
                        _ => _.SampleEntity.Description,
                        _ => _.SampleEntity.FieldRequired,
                        _ => _.SampleEntity.Field,
                        _ => _.SampleEntity.FieldDecimalNull,
                        _ => _.SampleEntity.FieldDoubleNull
                        );

					

                });
        }
    }
}
