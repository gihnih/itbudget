using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class BudgetDetailDto : EntityDto
    {
        public string Description { get; set; }

        public decimal BudgetAmount { get; set; }

        public decimal UtilizedBudget { get; set; }

        public decimal EstimatedBalance { get; set; }

        public string Remark { get; set; }

        public int? BudgetYear { get; set; }

        public long? OrganizationUnitId { get; set; }

        public bool IsBudgeted { get; set; }

        public string BudgetCategory { get; set; }

        public string BudgetType { get; set; }

        public string DescriptionType { get; set; }

        public int? GLCodeId { get; set; }

        public BudgetOrganizationUnitLookupTableDto OrganizationUnit { get; set; }

        public GLCodeDto GLCode { get; set; }
    }
}