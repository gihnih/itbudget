﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Authorization.Permissions.Dto;

namespace Base2.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}