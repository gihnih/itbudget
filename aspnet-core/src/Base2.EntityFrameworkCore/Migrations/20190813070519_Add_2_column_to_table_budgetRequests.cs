﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Base2.Migrations
{
    public partial class Add_2_column_to_table_budgetRequests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BudgetCustodianApproval",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BudgetCustodianRequestStatus",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "BudgetOwnerApproval",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BudgetOwnerRequestStatus",
                table: "BudgetRequests",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BudgetCustodianApproval",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "BudgetCustodianRequestStatus",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "BudgetOwnerApproval",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "BudgetOwnerRequestStatus",
                table: "BudgetRequests");
        }
    }
}