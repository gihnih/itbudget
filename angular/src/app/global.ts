import {Injectable} from '@angular/core';

@Injectable()
export class Globals {
    budgetType: EnumElement[] = [
        {id: 1, text: 'OPEX'},
        {id: 2, text: 'CAPEX'},
    ];

    budgetCategory: EnumElement[] = [
        {id: 1, text: 'Budgeted'},
        {id: 2, text: 'Unbudgeted'},
    ];

    budgetDescriptionType: EnumElement[] = [
        {id: 1, text: 'NewProject'},
        {id: 2, text: 'Improvement'},
        {id: 3, text: 'License'},
        {id: 4, text: 'Others'}
    ];
}

export interface EnumElement {
    id: number;
    text: string;
}
