import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetBudgetRequestForViewDto, BudgetRequestDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewBudgetRequestModal',
    templateUrl: './view-budgetRequest-modal.component.html'
})
export class ViewBudgetRequestModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetBudgetRequestForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetBudgetRequestForViewDto();
        this.item.budgetRequest = new BudgetRequestDto();
    }

    show(item: GetBudgetRequestForViewDto): void {
        console.log(item);
        this.item = item;
        this.active = true;
        this.modal.config.class = 'modal-full-width';
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
