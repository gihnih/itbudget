﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Base2
{
    public class Base2ClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(Base2ClientModule).GetAssembly());
        }
    }
}
