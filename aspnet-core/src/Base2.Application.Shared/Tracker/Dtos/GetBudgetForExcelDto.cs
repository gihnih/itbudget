using Humanizer;
using System.Collections.Generic;

namespace Base2.Tracker.Dtos
{
    public class GetBudgetForExcelDto
    {
        private string _descriptionType;

        public string Description { get; set; }

        public decimal BudgetAmount { get; set; }

        public decimal UtilizedBudget { get; set; }

        public decimal EstimatedBalance { get; set; }

        public string Remark { get; set; }

        public int? BudgetYear { get; set; }

        public long? OrganizationUnitId { get; set; }

        public bool IsBudgeted { get; set; }

        public string BudgetCategory { get; set; }

        public string BudgetType { get; set; }

        public string DescriptionType
        {
            get { return _descriptionType.Humanize(LetterCasing.Title); }
            set { _descriptionType = value; }
        }

        public int? GLCodeId { get; set; }

        public ICollection<BudgetRequestBudgetUseDto> BudgetRequests { get; set; }

        public GLCodeDto GLCode { get; set; }

        public BudgetOrganizationUnitLookupTableDto OrganizationUnit { get; set; }
    }
}