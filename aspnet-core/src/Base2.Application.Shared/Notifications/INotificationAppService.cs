﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Notifications.Dto;
using System;
using System.Threading.Tasks;

namespace Base2.Notifications
{
    public interface INotificationAppService : IApplicationService
    {
        Task<GetNotificationsOutput> GetUserNotifications(GetUserNotificationsInput input);

        Task SetAllNotificationsAsRead();

        Task SetNotificationAsRead(EntityDto<Guid> input);

        Task<GetNotificationSettingsOutput> GetNotificationSettings();

        Task UpdateNotificationSettings(UpdateNotificationSettingsInput input);
    }
}