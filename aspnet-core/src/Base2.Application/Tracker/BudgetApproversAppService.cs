using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Base2.Authorization;
using Base2.Authorization.Users;
using Base2.Dto;
using Base2.Tracker.Dtos;
using Base2.Tracker.Exporting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    [AbpAuthorize(AppPermissions.Pages_PendingActivity)]
    public class BudgetApproversAppService : Base2AppServiceBase, IBudgetApproversAppService
    {
        private readonly IRepository<BudgetApprover> _budgetApproverRepository;
        private readonly IBudgetApproversExcelExporter _budgetApproversExcelExporter;

        private readonly IRepository<Budget> _budgetRepository;
        private readonly IRepository<BudgetUse> _budgetUseRepository;
        private readonly IRepository<User, long> _user;

        private readonly IRepository<BudgetRequest> _budgetRequestRepository;

        public BudgetApproversAppService(IRepository<BudgetApprover> budgetApproverRepository, IBudgetApproversExcelExporter budgetApproversExcelExporter,
         IRepository<Budget> budgetRepository, IRepository<BudgetUse> budgetUseRepository, IRepository<User, long> user,
         IRepository<BudgetRequest> budgetRequestRepository)
        {
            _budgetApproverRepository = budgetApproverRepository;
            _budgetApproversExcelExporter = budgetApproversExcelExporter;
            _budgetRepository = budgetRepository;
            _budgetUseRepository = budgetUseRepository;
            _budgetRequestRepository = budgetRequestRepository;
            _user = user;
        }

        public async Task<PagedResultDto<GetBudgetApproverForViewDto>> GetAll(GetAllBudgetApproversInput input)
        {
            var filteredBudgetApprovers = _budgetApproverRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Status), e => e.Status.Contains(input.Status))
                        .Where(e => e.UserId == null);

            var query = (from o in filteredBudgetApprovers
                         join o1 in _budgetRepository.GetAll() on o.BudgetId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _budgetUseRepository.GetAll() on o.BudgetUseId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         join o4 in _user.GetAll() on o.UserRequestId equals o4.Id into j4
                         from s4 in j4.DefaultIfEmpty()

                         select new GetBudgetApproverForViewDto()
                         {
                             BudgetApprover = ObjectMapper.Map<BudgetApproverDto>(o),
                             Budget = ObjectMapper.Map<BudgetDto>(s1),
                             BudgetUse = ObjectMapper.Map<BudgetUseDto>(s2),
                             requesterId = s4.Id,
                             requesterName = s4.Name
                         });

            var totalCount = await query.CountAsync();

            var budgetApprovers = await query
                .OrderBy(input.Sorting ?? "budgetApprover.id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<GetBudgetApproverForViewDto>(
                totalCount,
                budgetApprovers
            );
        }

        public async Task<GetBudgetApproverForViewDto> GetBudgetApproverForView(int id)
        {
            var budgetApprover = await _budgetApproverRepository.GetAsync(id);

            var output = new GetBudgetApproverForViewDto { BudgetApprover = ObjectMapper.Map<BudgetApproverDto>(budgetApprover) };

            return output;
        }

        //[AbpAuthorize(AppPermissions.Pages_BudgetApprovers_Edit)]
        public async Task<GetBudgetApproverForEditOutput> GetBudgetApproverForEdit(EntityDto input)
        {
            var budgetApprover = await _budgetApproverRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetBudgetApproverForEditOutput { BudgetApprover = ObjectMapper.Map<CreateOrEditBudgetApproverDto>(budgetApprover) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditBudgetApproverDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
        }

        public async Task UpdateStatus(EditBudgetApproveDto input)
        {
            if (input.Status == "Approved")
            {
                await Update(input);
            }
            if (input.Status == "Rejected")
            {
                await UpdateReject(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_BudgetApprovers_Create)]
        private async Task Create(CreateOrEditBudgetApproverDto input)
        {
            var budgetApprover = ObjectMapper.Map<BudgetApprover>(input);

            await _budgetApproverRepository.InsertAsync(budgetApprover);
        }

        //[AbpAuthorize(AppPermissions.Pages_BudgetApprovers_Edit)]
        private async Task Update(EditBudgetApproveDto input)
        {
            var budgetApprover = await _budgetApproverRepository.FirstOrDefaultAsync((int)input.Id);
            budgetApprover.Status = input.Status;
            budgetApprover.UserId = input.UserId;

            await CurrentUnitOfWork.SaveChangesAsync();
        }

        //[AbpAuthorize(AppPermissions.Pages_BudgetApprovers_Edit)]
        private async Task UpdateReject(EditBudgetApproveDto input)
        {
            var budgetApprover = await _budgetApproverRepository.FirstOrDefaultAsync((int)input.Id);
            var budget = await _budgetRepository.FirstOrDefaultAsync((int)budgetApprover.BudgetId);
            var budgetUse = await _budgetUseRepository.FirstOrDefaultAsync((int)budgetApprover.BudgetUseId);
            var budgetRequest = await _budgetRequestRepository.GetAll().Where(b => b.BudgetUseId == budgetUse.Id).ToListAsync();

            budgetApprover.Status = input.Status;
            budgetApprover.UserId = input.UserId;

            budget.EstimatedBalance = budget.EstimatedBalance + (budgetUse.ActualAmount > 0 ? budgetUse.ActualAmount : budgetUse.RequestAmount);
            budget.UtilizedBudget = budget.UtilizedBudget - (budgetUse.ActualAmount > 0 ? budgetUse.ActualAmount : budgetUse.RequestAmount);

            _budgetUseRepository.Delete(budgetUse);
            foreach (var item in budgetRequest)
            {
                _budgetRequestRepository.Delete(item);
            }

            await CurrentUnitOfWork.SaveChangesAsync();
        }

        //[AbpAuthorize(AppPermissions.Pages_BudgetApprovers_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _budgetApproverRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetBudgetApproversToExcel(GetAllBudgetApproversForExcelInput input)
        {
            var filteredBudgetApprovers = _budgetApproverRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinUserIdFilter != null, e => e.UserId >= input.MinUserIdFilter)
                        .WhereIf(input.MaxUserIdFilter != null, e => e.UserId <= input.MaxUserIdFilter)
                        .WhereIf(input.MinBudgetUseIdFilter != null, e => e.BudgetUseId >= input.MinBudgetUseIdFilter)
                        .WhereIf(input.MaxBudgetUseIdFilter != null, e => e.BudgetUseId <= input.MaxBudgetUseIdFilter)
                        .WhereIf(input.MinBudgetIdFilter != null, e => e.BudgetId >= input.MinBudgetIdFilter)
                        .WhereIf(input.MaxBudgetIdFilter != null, e => e.BudgetId <= input.MaxBudgetIdFilter)
                        .WhereIf(input.MinRequestAmountFilter != null, e => e.RequestAmount >= input.MinRequestAmountFilter)
                        .WhereIf(input.MaxRequestAmountFilter != null, e => e.RequestAmount <= input.MaxRequestAmountFilter)
                        .WhereIf(input.MinActualAmountFilter != null, e => e.ActualAmount >= input.MinActualAmountFilter)
                        .WhereIf(input.MaxActualAmountFilter != null, e => e.ActualAmount <= input.MaxActualAmountFilter);

            var query = (from o in filteredBudgetApprovers
                         select new GetBudgetApproverForViewDto()
                         {
                             BudgetApprover = ObjectMapper.Map<BudgetApproverDto>(o)
                         });

            var budgetApproverListDtos = await query.ToListAsync();

            return _budgetApproversExcelExporter.ExportToFile(budgetApproverListDtos);
        }
    }
}