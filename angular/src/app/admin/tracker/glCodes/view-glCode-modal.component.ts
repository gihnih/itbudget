import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { GetGLCodeForViewDto, GLCodeDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'viewGLCodeModal',
    templateUrl: './view-glCode-modal.component.html'
})
export class ViewGLCodeModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    item: GetGLCodeForViewDto;


    constructor(
        injector: Injector
    ) {
        super(injector);
        this.item = new GetGLCodeForViewDto();
        this.item.glCode = new GLCodeDto();
    }

    show(item: GetGLCodeForViewDto): void {
        this.item = item;
        this.active = true;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
