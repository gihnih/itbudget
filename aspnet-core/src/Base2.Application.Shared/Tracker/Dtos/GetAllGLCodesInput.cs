using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class GetAllGLCodesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string CodeFilter { get; set; }

        public string DescriptionFilter { get; set; }
    }
}