﻿import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DashboardComponent} from './dashboard/dashboard.component';
import { SampleEntitiesComponent } from './sample/sampleEntities/sampleEntities.component';
import { ViewSampleEntityModalComponent } from './sample/sampleEntities/view-sampleEntity-modal.component';
import { CreateOrEditSampleEntityModalComponent } from './sample/sampleEntities/create-or-edit-sampleEntity-modal.component';

import {BudgetApproversComponent} from './tracker/budgetApprovers/budgetApprovers.component';
import {ViewBudgetApproverModalComponent} from './tracker/budgetApprovers/view-budgetApprover-modal.component';
import {CreateOrEditBudgetApproverModalComponent} from './tracker/budgetApprovers/create-or-edit-budgetApprover-modal.component';

import {BudgetsComponent} from './tracker/budgets/budgets.component';
import {ViewBudgetModalComponent} from './tracker/budgets/view-budget-modal.component';
import {CreateOrEditBudgetModalComponent} from './tracker/budgets/create-or-edit-budget-modal.component';
import {OrganizationUnitLookupTableModalComponent} from './tracker/budgets/organizationUnit-lookup-table-modal.component';

import {BudgetRequestsComponent} from './tracker/budgetRequests/budgetRequests.component';
import {ViewBudgetRequestModalComponent} from './tracker/budgetRequests/view-budgetRequest-modal.component';
import {CreateOrEditBudgetRequestModalComponent} from './tracker/budgetRequests/create-or-edit-budgetRequest-modal.component';
import {BudgetUseLookupTableModalComponent} from './tracker/budgetRequests/budgetUse-lookup-table-modal.component';

import {BudgetUsesComponent} from './tracker/budgetUses/budgetUses.component';
import {ViewBudgetUseModalComponent} from './tracker/budgetUses/view-budgetUse-modal.component';
import {CreateOrEditBudgetUseModalComponent} from './tracker/budgetUses/create-or-edit-budgetUse-modal.component';
import {BudgetLookupTableModalComponent} from './tracker/budgetUses/budget-lookup-table-modal.component';

import {AutoCompleteModule} from 'primeng/primeng';
import {PaginatorModule} from 'primeng/primeng';
import {EditorModule} from 'primeng/primeng';
import {InputMaskModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import {DataTableModule} from 'primeng/primeng';

import {ModalModule, TabsModule, TooltipModule} from 'ngx-bootstrap';
import {AppCommonModule} from '@app/shared/common/app-common.module';
import {UtilsModule} from '@shared/utils/utils.module';
import {MainRoutingModule} from './main-routing.module';
import {CountoModule} from '@node_modules/angular2-counto';
import {EasyPieChartModule} from 'ng2modules-easypiechart';
import {UnbudgetsComponent} from './tracker/unbudgets/unbudgets.component';
import {AllBudgetsComponent} from '@app/main/tracker/budgets/all-budgets.component';
import {NgxMaskModule} from 'ngx-mask';
import { PendingActivityComponent } from './tracker/pending-activity/pending-activity.component';
import { DetailModalComponent } from './tracker/pending-activity/detail-modal/detail-modal.component';
import {MatPaginatorModule, MatSelectModule, MatTableModule} from '@angular/material';
import {NgxChartsModule} from '@node_modules/@swimlane/ngx-charts';
import { DepartmentDashboardComponent } from './department-dashboard/department-dashboard.component';
import { MyBudgetsComponent } from './tracker/my-budgets/my-budgets.component';
import {GLCodeLookupTableModalComponent} from '@app/main/tracker/budgets/glCode-lookup-table-modal.component';
import { MyPendingActivityComponent } from './tracker/pending-activity/my-pending-activity.component';
import {BudgetUseCustodianComponent} from 'app/main/tracker/budgetUses/budget-use-custodian.component';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';

@NgModule({
    imports: [
        FileUploadModule,
        AutoCompleteModule,
        PaginatorModule,
        EditorModule,
        InputMaskModule, DataTableModule,

        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        MainRoutingModule,
        CountoModule,
        EasyPieChartModule, NgxMaskModule,
        MatTableModule,
        MatPaginatorModule,
        NgxChartsModule,
        MatSelectModule,
        HttpClientModule,
        HttpClientJsonpModule
    ],
    declarations: [
		SampleEntitiesComponent,
		ViewSampleEntityModalComponent,		CreateOrEditSampleEntityModalComponent,
        BudgetApproversComponent,
        ViewBudgetApproverModalComponent, CreateOrEditBudgetApproverModalComponent,
        BudgetsComponent,
        ViewBudgetModalComponent, CreateOrEditBudgetModalComponent,
        OrganizationUnitLookupTableModalComponent,
        BudgetRequestsComponent,
        ViewBudgetRequestModalComponent, CreateOrEditBudgetRequestModalComponent,
        BudgetUseLookupTableModalComponent,
        BudgetUsesComponent,
        ViewBudgetUseModalComponent, CreateOrEditBudgetUseModalComponent,
        BudgetLookupTableModalComponent,
        DashboardComponent,
        UnbudgetsComponent,
        AllBudgetsComponent,
        PendingActivityComponent,
        DetailModalComponent,
        DepartmentDashboardComponent,
        MyBudgetsComponent,
        GLCodeLookupTableModalComponent,
        MyPendingActivityComponent,
        BudgetUseCustodianComponent
    ]
})
export class MainModule {
}
