﻿using Abp.Auditing;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Base2.Web.Controllers
{
    public class HomeController : Base2ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public HomeController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [DisableAuditing]
        public IActionResult Index()
        {
            if (_hostingEnvironment.IsDevelopment())
                return Redirect("/swagger");
            else
                return Redirect("/index.html");
        }
    }
}