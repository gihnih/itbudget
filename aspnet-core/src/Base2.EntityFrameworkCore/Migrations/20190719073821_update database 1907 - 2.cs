﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Base2.Migrations
{
    public partial class updatedatabase19072 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ActualAmount",
                table: "BudgetRequests",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "RequestAmount",
                table: "BudgetRequests",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActualAmount",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "RequestAmount",
                table: "BudgetRequests");
        }
    }
}