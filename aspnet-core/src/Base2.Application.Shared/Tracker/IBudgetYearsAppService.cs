using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Threading.Tasks;

namespace Base2.Tracker
{
    public interface IBudgetYearsAppService : IApplicationService
    {
        Task<PagedResultDto<GetBudgetYearForViewDto>> GetAll(GetAllBudgetYearsInput input);

        Task<GetBudgetYearForViewDto> GetBudgetYearForView(int id);

        Task<GetBudgetYearForEditOutput> GetBudgetYearForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBudgetYearDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetBudgetYearsToExcel(GetAllBudgetYearsForExcelInput input);
    }
}