﻿namespace Base2.Sessions.Dto
{
    public class SubscriptionPaymentInfoDto
    {
        public decimal Amount { get; set; }
    }
}