﻿using Abp.Domain.Policies;
using System.Threading.Tasks;

namespace Base2.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}