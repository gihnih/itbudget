using Abp.Application.Services.Dto;

namespace Base2.Tracker.Dtos
{
    public class CreateOrEditBudgetTypeDto : EntityDto<int?>
    {
        public string Name { get; set; }
    }
}