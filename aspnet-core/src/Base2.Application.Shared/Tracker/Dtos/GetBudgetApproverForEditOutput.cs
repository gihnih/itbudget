namespace Base2.Tracker.Dtos
{
    public class GetBudgetApproverForEditOutput
    {
        public CreateOrEditBudgetApproverDto BudgetApprover { get; set; }
    }
}