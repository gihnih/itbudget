using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Base2.DataExporting.Excel.EpPlus;
using Base2.Dto;
using Base2.Tracker.Dtos;
using System.Collections.Generic;

namespace Base2.Tracker.Exporting
{
    public class BudgetYearsExcelExporter : EpPlusExcelExporterBase, IBudgetYearsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BudgetYearsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession) : base()
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetBudgetYearForViewDto> budgetYears)
        {
            return CreateExcelPackage(
                "BudgetYears.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("BudgetYears"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Year")
                        );

                    AddObjects(
                        sheet, 2, budgetYears,
                        _ => _.BudgetYear.Year
                        );
                });
        }
    }
}