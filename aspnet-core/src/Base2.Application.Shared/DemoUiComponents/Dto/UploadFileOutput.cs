﻿using System;

namespace Base2.DemoUiComponents.Dto
{
    public class UploadFileOutput
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
    }
}