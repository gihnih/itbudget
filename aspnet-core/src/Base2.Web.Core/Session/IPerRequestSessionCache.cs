﻿using Base2.Sessions.Dto;
using System.Threading.Tasks;

namespace Base2.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}