﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Base2.Migrations
{
    public partial class updatedatabase1907 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnerStatus",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "UseStatus",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "UseStatusId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "UseStatusId",
                table: "BudgetApprovers");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "BudgetUses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "BudgetUses",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OwnBudget",
                table: "BudgetUses",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PaperStatus",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RequestStatus",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsBudgeted",
                table: "Budgets",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "BudgetRequests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "BudgetRequests",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "BudgetRequests",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "BudgetApprovers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "BudgetApprovers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "BudgetApprovers",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "BudgetApprovers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaperStatus",
                table: "BudgetApprovers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RequestStatus",
                table: "BudgetApprovers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AbpUsers",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 32);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "OwnBudget",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "PaperStatus",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "RequestStatus",
                table: "BudgetUses");

            migrationBuilder.DropColumn(
                name: "IsBudgeted",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "BudgetRequests");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "BudgetApprovers");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "BudgetApprovers");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "BudgetApprovers");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "BudgetApprovers");

            migrationBuilder.DropColumn(
                name: "PaperStatus",
                table: "BudgetApprovers");

            migrationBuilder.DropColumn(
                name: "RequestStatus",
                table: "BudgetApprovers");

            migrationBuilder.AddColumn<string>(
                name: "OwnerStatus",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UseStatus",
                table: "BudgetUses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UseStatusId",
                table: "BudgetUses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UseStatusId",
                table: "BudgetApprovers",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AbpUsers",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 256);
        }
    }
}