namespace Base2.Tracker.Dtos
{
    public class GetDepartmentForViewDto
    {
        public DepartmentDto Department { get; set; }
    }
}